﻿#pragma once

struct FUtils
{
	template<typename T, typename G>
	static void OrderByDistance(T* Target, TArray<G*>& InArray)
	{
		InArray.Sort([Target](const G& A, const G& B)
		{
			return A.GetDistanceTo(Target) < B.GetDistanceTo(Target);
		});
	}
};
