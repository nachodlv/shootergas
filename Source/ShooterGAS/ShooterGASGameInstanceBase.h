// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ShooterGASGameInstanceBase.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERGAS_API UShooterGASGameInstanceBase : public UGameInstance
{
	GENERATED_BODY()
};
