#include "ShooterGASGun.h"

#include "DrawDebugHelpers.h"
#include "ShooterGASCharacter.h"
#include "Kismet/GameplayStatics.h"

AShooterGASGun::AShooterGASGun()
{
	PrimaryActorTick.bCanEverTick = false;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);
}

void AShooterGASGun::BeginPlay()
{
	Super::BeginPlay();

}

float AShooterGASGun::CalculateDamage(const FName& BoneName) const
{
	AShooterGASCharacter* Character = Cast<AShooterGASCharacter>(GetOwner());
	if(!Character)
	{
		return 0;
	}
	for (const FName& HeadBone : Character->GetBones().HeadBones)
	{
		if(HeadBone == BoneName)
		{
			return Damage * HeadshotDamageMultiplier;
		}
	}
	for (const FName& FeetBone : Character->GetBones().FeetBones)
	{
		if(FeetBone == BoneName)
		{
			return Damage * FeetDamageMultiplier;
		}
	}
	return Damage;
}


TSubclassOf<ULineTraceFireGameplayAbility> AShooterGASGun::GetFireAbility() const
{
	return FireAbility;
}

USkeletalMeshComponent* AShooterGASGun::GetSkeletalMesh() const
{
	return Mesh;
}

float AShooterGASGun::GetMaxRange() const
{
	return MaxRange;
}

