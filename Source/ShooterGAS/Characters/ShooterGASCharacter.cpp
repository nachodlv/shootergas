#include "ShooterGASCharacter.h"

#include "DrawDebugHelpers.h"
#include "ShooterGASGun.h"
#include "ShooterGAS/Player/ShooterGASPlayerState.h"
#include "ShooterGAS/GameAbilitySystem/ShooterGASAbilitySystemComponent.h"
#include "ShooterGAS/GameAbilitySystem/Abilities/ShooterGasGameplayAbility.h"
#include "ShooterGAS/GameAbilitySystem/Abilities/LineTraceFireGameplayAbility.h"

AShooterGASCharacter::AShooterGASCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AShooterGASCharacter::DrawSphere_Implementation(const FVector& Center, const float Radius, const FColor& Color)
{
	DrawDebugSphere(GetWorld(), Center, Radius, 32, Color);
}

void AShooterGASCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Hides the default gun from the mesh
	GetMesh()->HideBoneByName(TEXT("weapon_r"), PBO_None);

	if(!HasAuthority()) AttachInitialGun();

	if (UShooterGASAbilitySystemComponent* ASC = Cast<UShooterGASAbilitySystemComponent>(GetAbilitySystemComponent()))
	{
		ASC->OnTagUpdate.AddUObject(this, &AShooterGASCharacter::OnAbilitySystemTagUpdate);
	}
}

// Called to bind functionality to input
void AShooterGASCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Setup player movement
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterGASCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterGASCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Pressed, this, &AShooterGASCharacter::StartCrouching);
	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Released, this, &AShooterGASCharacter::StopCrouching);
	//Setup player movement for gamepad
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterGASCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterGASCharacter::LookRightRate);

	UAbilitySystemComponent* AbilitySystemComponent = GetAbilitySystemComponent();
	if(AbilitySystemComponent && InputComponent)
	{
		const FGameplayAbilityInputBinds Binds(
            "Confirm",
            "Cancel",
            "EShooterGASAbilityInputID",
            static_cast<int32>(EShooterGASAbilityInputID::Confirm),
            static_cast<int32>(EShooterGASAbilityInputID::Cancel)
            );
		AbilitySystemComponent->BindAbilityActivationToInputComponent(InputComponent, Binds);
	}
}

AShooterGASGun* AShooterGASCharacter::GetGun() const
{
	return Gun;
}

void AShooterGASCharacter::OnAbilitySystemTagUpdate(const FGameplayTag& Tag, bool TagExists)
{
	if (AShooterGASPlayerState* PS = Cast<AShooterGASPlayerState>(GetPlayerState()))
	{
		if (Tag == PS->GetCrouchTag())
		{
			if (TagExists)
			{
				Super::Crouch();
			}
			else
			{
				Super::UnCrouch();
			}
		}
	}

}

void AShooterGASCharacter::MoveForward(const float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterGASCharacter::MoveRight(const float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterGASCharacter::LookUpRate(const float AxisValue)
{
	AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterGASCharacter::LookRightRate(const float AxisValue)
{
	AddControllerYawInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterGASCharacter::AttachInitialGun()
{
	Gun = GetWorld()->SpawnActor<AShooterGASGun>(GunClass);
	Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
	Gun->SetOwner(this);
}

void AShooterGASCharacter::Crouch(bool bClientSimulation)
{
	UAbilitySystemComponent* AbilitySystem = GetAbilitySystemComponent();
	if (AbilitySystem && !bIsCrouched)
	{
		const FGameplayEffectContextHandle Handle = AbilitySystem->MakeEffectContext();
		AbilitySystem->ApplyGameplayEffectToSelf(CrouchGameplayEffect.GetDefaultObject(), 1, Handle);
	}
}

void AShooterGASCharacter::UnCrouch(bool bClientSimulation)
{
	UAbilitySystemComponent* AbilitySystem = GetAbilitySystemComponent();
	if (AbilitySystem && bIsCrouched)
	{
		const FGameplayEffectContextHandle Handle = AbilitySystem->MakeEffectContext();
		AbilitySystem->ApplyGameplayEffectToSelf(UnCrouchGameplayEffect.GetDefaultObject(), 1, Handle);
	}
}

void AShooterGASCharacter::StartCrouching()
{
	Crouch();
}

void AShooterGASCharacter::StopCrouching()
{
	UnCrouch();
}

void AShooterGASCharacter::Jump()
{
	OnJump.Broadcast(GetActorLocation());
	Super::Jump();
}

const FNavAgentProperties& AShooterGASCharacter::GetAgentProperties()
{
	AController* CurrentController = GetController();
	return CurrentController ? CurrentController->GetNavAgentPropertiesRef() : FNavAgentProperties::DefaultProperties;
}

void AShooterGASCharacter::OnWalkingOffLedge_Implementation(const FVector& PreviousFloorImpactNormal, const FVector& PreviousFloorContactNormal, const FVector& PreviousLocation, float TimeDelta)
{
	Super::OnWalkingOffLedge_Implementation(PreviousFloorImpactNormal, PreviousFloorContactNormal, PreviousLocation, TimeDelta);
	OnWalkOffLedge.Broadcast(PreviousLocation);
}

void AShooterGASCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);
	OnLand.Broadcast(Hit.Location);
}

UAbilitySystemComponent* AShooterGASCharacter::GetAbilitySystemComponent() const
{
	AShooterGASPlayerState* CurrentPlayerState = Cast<AShooterGASPlayerState>(GetPlayerState());
	if(!CurrentPlayerState) return nullptr;
	return CurrentPlayerState->GetAbilitySystemComponent();
}

void AShooterGASCharacter::InitializeAttributes()
{
	UAbilitySystemComponent* AbilitySystemComponent = GetAbilitySystemComponent();
	if (AbilitySystemComponent && DefaultAttributeSet)
	{
		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		EffectContext.AddSourceObject(this);
		const FGameplayEffectSpecHandle SpecHandle = AbilitySystemComponent->MakeOutgoingSpec(
			DefaultAttributeSet, 1, EffectContext);
		if (SpecHandle.IsValid())
		{
			AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data.Get());
		}
	}
}

void AShooterGASCharacter::GiveAbilities()
{
	UAbilitySystemComponent* AbilitySystemComponent = GetAbilitySystemComponent();
	if (HasAuthority() && AbilitySystemComponent)
	{
		AttachInitialGun();

		for (TSubclassOf<UShooterGasGameplayAbility>& DefaultAbility : DefaultAbilities)
		{
			AbilitySystemComponent->GiveAbility(
				FGameplayAbilitySpec(
					DefaultAbility,
					1,
					static_cast<int32>(DefaultAbility.GetDefaultObject()->AbilityInputID),
					this)
				);
		}
		if (Gun)
		{
			const TSubclassOf<ULineTraceFireGameplayAbility> FireAbility = Gun->GetFireAbility();
			if(FireAbility)
			{
				AbilitySystemComponent->GiveAbility(
		            FGameplayAbilitySpec(
		                FireAbility,
		                1,
		                static_cast<int32>(FireAbility.GetDefaultObject()->AbilityInputID),
		                this)
				);
			}
		}
	}
}

void AShooterGASCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();
	OnPlayerStateReplicated(GetPlayerState<AShooterGASPlayerState>());
	InitializeAttributes();

	UAbilitySystemComponent* AbilitySystemComponent = GetAbilitySystemComponent();
	if(AbilitySystemComponent && InputComponent)
	{
		const FGameplayAbilityInputBinds Binds(
			"Confirm",
			"Cancel",
			"EShooterGASAbilityInputID",
			static_cast<int32>(EShooterGASAbilityInputID::Confirm),
			static_cast<int32>(EShooterGASAbilityInputID::Cancel)
			);
		AbilitySystemComponent->BindAbilityActivationToInputComponent(InputComponent, Binds);
	}
}

void AShooterGASCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	UAbilitySystemComponent* AbilitySystemComponent = GetAbilitySystemComponent();
	AttachInitialGun();
	if(AbilitySystemComponent)
	{
		AbilitySystemComponent->InitAbilityActorInfo(this, this);
		InitializeAttributes();
		GiveAbilities();
	}
}
