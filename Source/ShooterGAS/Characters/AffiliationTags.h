﻿#pragma once

#include "CoreMinimal.h"

#include "GameplayTagContainer.h"
#include "UObject/Interface.h"
#include "AffiliationTags.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UAffiliationTags : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class SHOOTERGAS_API IAffiliationTags
{
	GENERATED_BODY()

public:
	virtual FGameplayTagContainer GetAffiliationTags() const PURE_VIRTUAL(IAffiliationTags::GetAffiliationTags, return FGameplayTagContainer(););

	virtual bool IsAlly(IAffiliationTags* Other);
	virtual bool IsEnemy(IAffiliationTags* Other);
};
