#pragma once

#include "CoreMinimal.h"

#include "ShooterGASCharacter.h"
#include "GameFramework/Actor.h"
#include "ShooterGASGun.generated.h"

class ULineTraceFireGameplayAbility;

UCLASS()
class SHOOTERGAS_API AShooterGASGun : public AActor
{
	GENERATED_BODY()

public:
	AShooterGASGun();

	TSubclassOf<ULineTraceFireGameplayAbility> GetFireAbility() const;

	UFUNCTION(BlueprintCallable)
	USkeletalMeshComponent* GetSkeletalMesh() const;

	float GetMaxRange() const;

	float GetDamage() const { return Damage; }

	float CalculateDamage(const FName& BoneName) const;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
	USceneComponent* Root;
	UPROPERTY(VisibleAnywhere)
	USkeletalMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
	UParticleSystem* MuzzleFlash;
	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactEffect;

	UPROPERTY(EditAnywhere)
	float MaxRange = 1000;

	// Damages
	UPROPERTY(EditAnywhere)
	float Damage = 10;
	UPROPERTY(EditAnywhere)
	float HeadshotDamageMultiplier = 1.5f;
	UPROPERTY(EditAnywhere)
	float BodyDamageMultiplier = 1.0f;
	UPROPERTY(EditAnywhere)
	float FeetDamageMultiplier = 0.5f;


	UPROPERTY(EditAnywhere)
	TSubclassOf<ULineTraceFireGameplayAbility> FireAbility;

};
