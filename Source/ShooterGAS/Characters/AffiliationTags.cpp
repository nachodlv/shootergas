﻿#include "AffiliationTags.h"

bool IAffiliationTags::IsAlly(IAffiliationTags* Other)
{
	if (!Other)
	{
		return false;
	}

	const FGameplayTagContainer GameplayTags = GetAffiliationTags();
	return GameplayTags.HasAll(Other->GetAffiliationTags());
}

bool IAffiliationTags::IsEnemy(IAffiliationTags* Other)
{
	return !IsAlly(Other);
}


