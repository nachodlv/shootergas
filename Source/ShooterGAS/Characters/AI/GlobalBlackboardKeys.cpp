﻿#include "GlobalBlackboardKeys.h"

namespace
{
	const FName SelfActor = FName(TEXT("Self"));
	const FName TargetLocation = FName(TEXT("TargetLocation"));
	const FName InitialLocation = FName(TEXT("InitialLocation"));
	const FName InvestigateLocation = FName(TEXT("InvestigateLocation"));
	const FName IsTargetOnSight = FName(TEXT("IsTargetOnSight"));
	const FName JustDamaged = FName(TEXT("JustDamaged"));
	const FName AttackRange = FName(TEXT("AttackRange"));
	const FName LastTimeTargetSeen = FName(TEXT("LastTimeTargetSeen"));
}


UGlobalBlackboardKeys::UGlobalBlackboardKeys()
{
}

FName UGlobalBlackboardKeys::GetSelfActor()
{
	return SelfActor;
}

FName UGlobalBlackboardKeys::GetTargetLocation()
{
	return TargetLocation;
}

FName UGlobalBlackboardKeys::GetInitialLocation()
{
	return InitialLocation;
}

FName UGlobalBlackboardKeys::GetInvestigateLocation()
{
	return InvestigateLocation;
}

FName UGlobalBlackboardKeys::GetIsTargetInSight()
{
	return IsTargetOnSight;
}

FName UGlobalBlackboardKeys::GetJustDamaged()
{
	return JustDamaged;
}

FName UGlobalBlackboardKeys::GetAttackRange()
{
	return AttackRange;
}

FName UGlobalBlackboardKeys::GetLastTimeTargetSeen()
{
	return LastTimeTargetSeen;
}

