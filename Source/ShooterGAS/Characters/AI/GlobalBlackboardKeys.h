﻿

#pragma once

#include "CoreMinimal.h"

#include "Templates/SharedPointer.h"
#include "UObject/Object.h"
#include "GlobalBlackboardKeys.generated.h"

/**
 *
 */
UCLASS(Config = Game)
class SHOOTERGAS_API UGlobalBlackboardKeys : public UObject
{
	GENERATED_BODY()
public:
	UGlobalBlackboardKeys();

	static FName GetSelfActor();
	static FName GetTargetLocation();
	static FName GetInitialLocation();
	static FName GetInvestigateLocation();
	static FName GetIsTargetInSight();
	static FName GetJustDamaged();
	static FName GetAttackRange();
	static FName GetLastTimeTargetSeen();
};
