﻿#include "Perceptor.h"

#include "AbilitySystemComponent.h"
#include "GlobalBlackboardKeys.h"
#include "PMTGlobalBlackboardKeys.h"
#include "ShooterGASAICharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"

UPerceptor::UPerceptor()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPerceptor::BeginPlay()
{
	Super::BeginPlay();
}

void UPerceptor::ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors) const
{
	UBlackboardComponent* BlackboardComponent = GetBlackboardComponent();
	UAIPerceptionComponent* PerceptionComponent = GetPerceptionComponent();
	AShooterGASAICharacter* AIPawn = Cast<AShooterGASAICharacter>(GetPawn());
	if (!BlackboardComponent || !AIPawn || !PerceptionComponent)
	{
		return;
	}
	const float TimeSeconds = GetWorld()->GetTimeSeconds();
	for (AActor* const UpdatedActor : UpdatedActors)
	{
		const FActorPerceptionInfo* ActorPerceptionInfo = PerceptionComponent->GetActorInfo(*UpdatedActor);
		if (!ActorPerceptionInfo)
		{
			return;
		}
		const FVector& StimulusLocation = ActorPerceptionInfo->GetLastStimulusLocation();
		const AActor* Target = Cast<AActor>(BlackboardComponent->GetValueAsObject(UPMTGlobalBlackboardKeys::GetTarget()));
		const bool bTargetInSight = BlackboardComponent->GetValueAsBool(UGlobalBlackboardKeys::GetIsTargetInSight());

		if (IAffiliationTags* UpdatedActorAffiliation = Cast<IAffiliationTags>(UpdatedActor))
		{
			if (AIPawn->IsAlly(UpdatedActorAffiliation)) // For now, we don't care if it's an ally
			{
				return;
			}
		}

		// We sense an actor with our primary sense so we target it and know we are threated
		if(ActorPerceptionInfo->IsSenseActive(PerceptionComponent->GetDominantSenseID()))
		{
			BlackboardComponent->SetValueAsObject(UPMTGlobalBlackboardKeys::GetTarget(), UpdatedActor);
			BlackboardComponent->SetValueAsBool(UGlobalBlackboardKeys::GetIsTargetInSight(), true);
		} else
		{
			if (UpdatedActor == Target) // If we don't sense the actor with our dominant sense anymore then we clear it
			{
				BlackboardComponent->SetValueAsBool(UGlobalBlackboardKeys::GetIsTargetInSight(), false);
				BlackboardComponent->SetValueAsFloat(UGlobalBlackboardKeys::GetLastTimeTargetSeen(), TimeSeconds);
			}
			BlackboardComponent->SetValueAsVector(UGlobalBlackboardKeys::GetInvestigateLocation(), StimulusLocation);
		}
	}
}

void UPerceptor::SetAIController(AShooterGASAIController* Controller)
{
	if (Controller)
	{
		AIController = Controller;
		AIController->OnActorPerceptionUpdate.AddUObject(this, &UPerceptor::ActorsPerceptionUpdated);
	}
}



