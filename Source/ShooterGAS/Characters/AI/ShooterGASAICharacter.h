﻿#pragma once

#include "CoreMinimal.h"


#include "GameplayTagAssetInterface.h"
#include "ShooterGAS/Characters/ShooterGASCharacter.h"

#include "ShooterGASAICharacter.generated.h"

UCLASS()
class SHOOTERGAS_API AShooterGASAICharacter : public AShooterGASCharacter, public IGameplayTagAssetInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AShooterGASAICharacter();

	UFUNCTION(BlueprintCallable, Category = GameplayTags)
	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override;
};
