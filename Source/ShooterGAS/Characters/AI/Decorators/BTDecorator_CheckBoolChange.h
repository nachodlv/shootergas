﻿

#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/BTDecorator.h"
#include "BTDecorator_CheckBoolChange.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERGAS_API UBTDecorator_CheckBoolChange : public UBTDecorator
{
	GENERATED_BODY()
public:
	UBTDecorator_CheckBoolChange();
protected:
	UPROPERTY(EditAnywhere, Category="Blackboard")
	bool DesiredValue;
	UPROPERTY(EditAnywhere, Category=Blackboard)
	FBlackboardKeySelector BlackboardKey;

	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
	EBlackboardNotificationResult OnKeyValueChanged(const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID);
};
