// Fill out your copyright notice in the Description page of Project Settings.


#include "BTDecorator_IsInLineOfSight.h"
#include "AIModule/Classes/BehaviorTree/BlackboardComponent.h"

UBTDecorator_IsInLineOfSight::UBTDecorator_IsInLineOfSight()
{
	NodeName = TEXT("Is In Line of Sight");
	ActorToObserve.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTDecorator_IsInLineOfSight, ActorToObserve), AActor::StaticClass());
	Origin.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTDecorator_IsInLineOfSight, Origin), APawn::StaticClass());
}

bool UBTDecorator_IsInLineOfSight::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if (BlackboardComponent)
	{
		APawn* Pawn = Cast<APawn>(BlackboardComponent->GetValueAsObject(Origin.SelectedKeyName));
		AController* Controller = Pawn ? Pawn->Controller : nullptr;
		if (Controller)
		{
			AActor* Actor = Cast<AActor>(BlackboardComponent->GetValueAsObject(ActorToObserve.SelectedKeyName));
			return Controller->LineOfSightTo(Actor);
		}
	}
	return false;
}



