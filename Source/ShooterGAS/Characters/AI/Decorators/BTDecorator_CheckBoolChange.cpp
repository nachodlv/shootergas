﻿#include "BTDecorator_CheckBoolChange.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Bool.h"

UBTDecorator_CheckBoolChange::UBTDecorator_CheckBoolChange()
{
	BlackboardKey.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UBTDecorator_CheckBoolChange, BlackboardKey));
}

void UBTDecorator_CheckBoolChange::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	if (BlackboardComp)
	{
		const FBlackboard::FKey KeyID = BlackboardKey.GetSelectedKeyID();
		BlackboardComp->RegisterObserver(
			KeyID,
			this,
			FOnBlackboardChangeNotification::CreateUObject(this, &UBTDecorator_CheckBoolChange::OnKeyValueChanged)
		);
	}
}

bool UBTDecorator_CheckBoolChange::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	return BlackboardComponent && BlackboardComponent->GetValueAsBool(BlackboardKey.SelectedKeyName) == DesiredValue;
}

EBlackboardNotificationResult UBTDecorator_CheckBoolChange::OnKeyValueChanged(const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID)
{
	UBehaviorTreeComponent* BehaviorComp = static_cast<UBehaviorTreeComponent*>(Blackboard.GetBrainComponent());
	if (!BehaviorComp)
	{
		return EBlackboardNotificationResult::RemoveObserver;
	}

	if (ChangedKeyID == BlackboardKey.GetSelectedKeyID())
	{
		BehaviorComp->RequestExecution(this);
	}
	return EBlackboardNotificationResult::ContinueObserving;
}
