// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/Decorators/BTDecorator_BlackboardBase.h"
#include "BTDecorator_IsInLineOfSight.generated.h"

struct FBlackboardKeySelector;
/**
 *
 */
UCLASS()
class SHOOTERGAS_API UBTDecorator_IsInLineOfSight : public UBTDecorator_BlackboardBase
{
	GENERATED_BODY()
public:
	UBTDecorator_IsInLineOfSight();
protected:
	UPROPERTY(EditAnywhere, Category=Blackboard)
	FBlackboardKeySelector ActorToObserve;
	UPROPERTY(EditAnywhere, Category=Blackboard)
	FBlackboardKeySelector Origin;

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
