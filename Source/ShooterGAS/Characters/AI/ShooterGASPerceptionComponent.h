﻿#pragma once

#include "CoreMinimal.h"

#include "Perception/AIPerceptionComponent.h"

#include "Senses/Interfaces/PMTAIPerception.h"

#include "ShooterGASPerceptionComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class SHOOTERGAS_API UShooterGASPerceptionComponent : public UAIPerceptionComponent, public IPMTAIPerception
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UShooterGASPerceptionComponent();

	virtual FOnStimulusExpired& GetOnStimulusExpiredDelegate() override { return StimulusExpiredDelegate; }

	virtual FPerceptionUpdatedDelegate& GetOnPerceptionUpdatedDelegate() override { return OnPerceptionUpdated; }

protected:
	virtual void HandleExpiredStimulus(FAIStimulus& StimulusStore) override;

	FOnStimulusExpired StimulusExpiredDelegate;
};
