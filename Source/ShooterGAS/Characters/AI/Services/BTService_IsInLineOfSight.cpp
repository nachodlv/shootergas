// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_IsInLineOfSight.h"

#include "AIModule/Classes/BehaviorTree/BlackboardComponent.h"

UBTService_IsInLineOfSight::UBTService_IsInLineOfSight()
{
	NodeName = TEXT("Is In Line of Sight");
	ActorToObserve.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_IsInLineOfSight, ActorToObserve), AActor::StaticClass());
	Origin.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_IsInLineOfSight, Origin), APawn::StaticClass());
	Result.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_IsInLineOfSight, Result));
}

void UBTService_IsInLineOfSight::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if (BlackboardComponent)
	{
		APawn* Pawn = Cast<APawn>(BlackboardComponent->GetValueAsObject(Origin.SelectedKeyName));
		AController* Controller = Pawn ? Pawn->Controller : nullptr;
		if (Controller)
		{
			AActor* Actor = Cast<AActor>(BlackboardComponent->GetValueAsObject(ActorToObserve.SelectedKeyName));
			if (Controller->LineOfSightTo(Actor))
			{
				SetResult(OwnerComp, true);
				return;
			}
		}
	}
	SetResult(OwnerComp, false);
}

void UBTService_IsInLineOfSight::SetResult(UBehaviorTreeComponent& OwnerComp, const bool ResultValue) const
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if(BlackboardComponent)
	{
		BlackboardComponent->SetValueAsBool(Result.SelectedKeyName, ResultValue);
	}
}

