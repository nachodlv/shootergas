// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/BTService.h"
#include "BehaviorTree/Decorators/BTDecorator_BlackboardBase.h"
#include "BTService_IsInLineOfSight.generated.h"

struct FBlackboardKeySelector;
/**
 *
 */
UCLASS()
class SHOOTERGAS_API UBTService_IsInLineOfSight : public UBTService
{
	GENERATED_BODY()
public:
	UBTService_IsInLineOfSight();
protected:
	UPROPERTY(EditAnywhere, Category=Blackboard)
	FBlackboardKeySelector ActorToObserve;
	UPROPERTY(EditAnywhere, Category=Blackboard)
	FBlackboardKeySelector Origin;
	UPROPERTY(EditAnywhere, Category=Blackboard)
	FBlackboardKeySelector Result;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	void SetResult(UBehaviorTreeComponent& OwnerComp, const bool ResultValue) const;
};
