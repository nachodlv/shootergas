#include "ShooterGASAIController.h"

#include "AbilitySystemComponent.h"
#include "EngineUtils.h"
#include "GlobalBlackboardKeys.h"
#include "Perceptor.h"
#include "ShooterGASAICharacter.h"
#include "ShooterGASPerceptionComponent.h"

#include "BehaviorTree/BlackboardComponent.h"

#include "Components/PMTAIAbilityMessageSender.h"

#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Sight.h"

#include "Senses/PMTAIPerceptionManager.h"

#include "ShooterGAS/Characters/ShooterGASGun.h"
#include "ShooterGAS/Player/ShooterGASPlayerState.h"

AShooterGASAIController::AShooterGASAIController()
{
	bWantsPlayerState = true;

	PerceptionManager = CreateDefaultSubobject<UPMTAIPerceptionManager>(TEXT("PerceptionManager"));

	MessageSenderComponent = CreateDefaultSubobject<UPMTAIAbilityMessageSender>(TEXT("AbilityMessageSenderComponent"));

	PerceptionComponent = CreateDefaultSubobject<UShooterGASPerceptionComponent>(TEXT("AI Perception"));

	FAISenseAffiliationFilter AffiliationFilter;
	AffiliationFilter.bDetectEnemies = true;
	AffiliationFilter.bDetectFriendlies = true;
	AffiliationFilter.bDetectNeutrals = true;

	// Sight sense AI configuration
	SenseConfig_Sight = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConfig"));
	SenseConfig_Sight->DetectionByAffiliation = AffiliationFilter;
	SenseConfig_Sight->SetMaxAge(2.0f);
	PerceptionComponent->ConfigureSense(*SenseConfig_Sight);

	// Hearing sense AI configuration
	SenseConfig_Hearing = CreateDefaultSubobject<UAISenseConfig_Hearing>(TEXT("HearingConfig"));
	SenseConfig_Hearing->DetectionByAffiliation = AffiliationFilter;
	PerceptionComponent->ConfigureSense(*SenseConfig_Hearing);
}

void AShooterGASAIController::BeginPlay()
{
	Super::BeginPlay();

	PerceptionComponent->SetDominantSense(SenseConfig_Sight->GetSenseImplementation());
}

void AShooterGASAIController::InitPlayerState()
{
	Super::InitPlayerState();
	AShooterGASPlayerState* ShooterGASPlayerState = Cast<AShooterGASPlayerState>(PlayerState);
	if (ShooterGASPlayerState)
	{
		ShooterGASPlayerState->OnHealthChanged.AddDynamic(this, &AShooterGASAIController::PawnHealthChanged);
	} else
	{
		UE_LOG(LogTemp, Error, TEXT("The player state in the ShooterGASAIController is not valid"));
	}
}

void AShooterGASAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	RunBehaviorTree(AIBehavior);
	UBlackboardComponent* BlackboardComponent = GetBlackboardComponent();
	if (!BlackboardComponent)
	{
		return;
	}

	APawn* PawnPossessed = GetPawn();
	if(PawnPossessed)
	{
		BlackboardComponent->SetValueAsObject(UGlobalBlackboardKeys::GetSelfActor(), PawnPossessed);
		BlackboardComponent->SetValueAsVector(UGlobalBlackboardKeys::GetInitialLocation(), PawnPossessed->GetActorLocation());
	}
	if (AShooterGASCharacter* ShooterGASCharacter = Cast<AShooterGASCharacter>(PawnPossessed))
	{
		if (AShooterGASGun* Gun = ShooterGASCharacter->GetGun())
		{
			BlackboardComponent->SetValueAsFloat(UGlobalBlackboardKeys::GetAttackRange(), Gun->GetMaxRange());
		}
	}
}

FPathFollowingRequestResult AShooterGASAIController::MoveTo(const FAIMoveRequest& MoveRequest,
                                                            FNavPathSharedPtr* OutPath)
{
	AShooterGASAICharacter* ShooterGASAICharacter = Cast<AShooterGASAICharacter>(GetPawn());
	ShooterGASAICharacter->UnCrouch();
	return Super::MoveTo(MoveRequest, OutPath);
}

void AShooterGASAIController::ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors)
{
	OnActorPerceptionUpdate.Broadcast(UpdatedActors);
}

void AShooterGASAIController::PawnHealthChanged(const float OldHealth, const float NewHealth)
{
	UBlackboardComponent* BlackboardComponent = GetBlackboardComponent();

	if (NewHealth < OldHealth && BlackboardComponent)
	{
		BlackboardComponent->SetValueAsBool(UGlobalBlackboardKeys::GetJustDamaged(), true);
	}
}

