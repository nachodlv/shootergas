// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "Interfaces/PMTAIHaveAbilityMessageSender.h"

#include "ShooterGASAIController.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnActorPerceptionUpdate, const TArray<AActor*>&)

class UPMTAIPerceptionManager;

UCLASS()
class SHOOTERGAS_API AShooterGASAIController : public AAIController, public IPMTAIHaveAbilityMessageSender
{
	GENERATED_BODY()
public:

	FOnActorPerceptionUpdate OnActorPerceptionUpdate;

	AShooterGASAIController();

	virtual void BeginPlay() override;

	virtual void InitPlayerState() override;

	virtual void OnPossess(APawn* InPawn) override;

	virtual FPathFollowingRequestResult MoveTo(const FAIMoveRequest& MoveRequest, FNavPathSharedPtr* OutPath = nullptr) override;

	virtual UPMTAIAbilityMessageSender* GetMessageSender() override { return MessageSenderComponent; }

private:
	UPROPERTY(EditAnywhere, Category="ShooterGAS|AI", meta = (AllowPrivateAccess = "true"))
	class UBehaviorTree* AIBehavior;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="ShooterGAS|AI", meta = (AllowPrivateAccess = "true"))
	class UAISenseConfig_Sight* SenseConfig_Sight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="ShooterGAS|AI", meta = (AllowPrivateAccess = "true"))
    class UAISenseConfig_Hearing* SenseConfig_Hearing;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="ShooterGAS|AI", meta = (AllowPrivateAccess = "true"))
	class UPMTAIPerceptionManager* PerceptionManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category="ShooterGAS|AI", meta = (AllowPrivateAccess = "true"))
	UPMTAIAbilityMessageSender* MessageSenderComponent;

	UFUNCTION()
	virtual void ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors) override;

	UFUNCTION()
	virtual void PawnHealthChanged(float OldHealth, float NewHealth);
};
