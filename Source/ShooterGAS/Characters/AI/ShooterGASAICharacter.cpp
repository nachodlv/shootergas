﻿#include "ShooterGASAICharacter.h"


#include "AbilitySystemComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AShooterGASAICharacter::AShooterGASAICharacter()
{
	UCharacterMovementComponent* CM = GetCharacterMovement();
	if (CM)
	{
		FNavAgentProperties& NavAgentProperties = CM->NavAgentProps;
		NavAgentProperties.bCanCrouch = true;
	}
}

void AShooterGASAICharacter::GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const
{
	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	if (ASC)
	{
		ASC->GetOwnedGameplayTags(TagContainer);
	}
}


