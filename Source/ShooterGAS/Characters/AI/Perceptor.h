﻿#pragma once

#include "CoreMinimal.h"

#include "ShooterGASAIController.h"
#include "Components/ActorComponent.h"
#include "Perceptor.generated.h"

struct FActorPerceptionInfo;
class IAbilitySystemInterface;

UCLASS(ClassGroup=(AI), meta=(BlueprintSpawnableComponent))
class SHOOTERGAS_API UPerceptor : public UActorComponent
{
	GENERATED_BODY()

public:
	UPerceptor();

	void SetAIController(AShooterGASAIController* Controller);

protected:
	UPROPERTY(EditDefaultsOnly, Category="ShooterGAS")
	float TimeToGetOutOfCombat = 10.f;

	UPROPERTY()
	AShooterGASAIController* AIController;

	UFUNCTION()
	void ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors) const;

	virtual void BeginPlay() override;

	UBlackboardComponent* GetBlackboardComponent() const { return AIController ? AIController->GetBlackboardComponent() : nullptr; }
	APawn* GetPawn() const { return AIController ? AIController->GetPawn() : nullptr; }
	UAIPerceptionComponent* GetPerceptionComponent() const { return AIController ? AIController->GetPerceptionComponent() : nullptr; }
};
