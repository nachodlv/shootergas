// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "AffiliationTags.h"
#include "NavMesh/Interfaces/PMTAICanJump.h"



#include "ShooterGASCharacter.generated.h"

class AShooterGASGun;
class AShooterGASPlayerState;
class UShooterGASAbilitySystemComponent;

USTRUCT()
struct FShooterGASCharacterBones
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere)
	TArray<FName> HeadBones;
	UPROPERTY(EditAnywhere)
	TArray<FName> FeetBones;
};

UCLASS()
class SHOOTERGAS_API AShooterGASCharacter : public ACharacter, public IAbilitySystemInterface, public IAffiliationTags, public IPMTAICanJump
{
	GENERATED_BODY()

public:
	AShooterGASCharacter();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	AShooterGASGun* GetGun() const;

	FShooterGASCharacterBones GetBones() const { return Bones; }

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	virtual FGameplayTagContainer GetAffiliationTags() const override { return AffiliationTags; }

	UFUNCTION(Client, Reliable)
	void DrawSphere(const FVector& Center, const float Radius, const FColor& Color);

	virtual void Crouch(bool bClientSimulation = false) override;

	virtual void UnCrouch(bool bClientSimulation = false) override;

	void StartCrouching();

	void StopCrouching();

	virtual void Jump() override;

	virtual const FNavAgentProperties& GetAgentProperties() override;

	virtual void OnWalkingOffLedge_Implementation(const FVector& PreviousFloorImpactNormal, const FVector& PreviousFloorContactNormal, const FVector& PreviousLocation, float TimeDelta) override;

	virtual void Landed(const FHitResult& Hit) override;

	// IPMTAICanJump overrides
	virtual FICanJumpDelegate& GetOnJumpDelegate() override { return OnJump; }
	virtual FICanJumpDelegate& GetOnWalkOffLedgeDelegate() override { return OnWalkOffLedge; }
	virtual FICanJumpDelegate& GetOnLandDelegate() override { return OnLand; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "ShooterGAS")
    void OnPlayerStateReplicated(AShooterGASPlayerState* ShooterGasPlayerState);

private:
	UPROPERTY(EditAnywhere, Category="ShooterGAS|Movement", meta = (AllowPrivateAccess = "true"))
	float RotationRate = 10.f;
	UPROPERTY(EditAnywhere, Category="ShooterGAS|Movement", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UGameplayEffect> CrouchGameplayEffect;
	UPROPERTY(EditAnywhere, Category="ShooterGAS|Movement", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UGameplayEffect> UnCrouchGameplayEffect;

	UPROPERTY(EditDefaultsOnly, Category="ShooterGAS|Gun", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AShooterGASGun> GunClass;
	UPROPERTY()
	AShooterGASGun* Gun;
	UPROPERTY(EditAnywhere, Category="ShooterGAS|Gun", meta = (AllowPrivateAccess = "true"))
	FShooterGASCharacterBones Bones;

	UPROPERTY()
	class UShooterGASAttributeSet* Attributes;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "ShooterGAS|GameplayAbility", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UGameplayEffect> DefaultAttributeSet;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "ShooterGAS|GameplayAbility", meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<class UShooterGasGameplayAbility>> DefaultAbilities;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category="ShooterGAS|Affiliation", meta = (AllowPrivateAccess = "true"))
	FGameplayTagContainer AffiliationTags;

	// PMTAICanJump delegates
	FICanJumpDelegate OnJump;
	FICanJumpDelegate OnLand;
	FICanJumpDelegate OnWalkOffLedge;

	UFUNCTION()
	void OnAbilitySystemTagUpdate(const FGameplayTag& Tag, bool TagExists);

	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void LookUpRate(float AxisValue);
	void LookRightRate(float AxisValue);
	void AttachInitialGun();


	virtual void InitializeAttributes();
	virtual void GiveAbilities();
	virtual void OnRep_PlayerState() override;
	virtual void PossessedBy(AController* NewController) override;

};
