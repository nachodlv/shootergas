﻿#pragma once

#include "CoreMinimal.h"
#include "Subsystems/EngineSubsystem.h"
#include "ShooterGASEngineSubsystem.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERGAS_API UShooterGASEngineSubsystem : public UEngineSubsystem
{
	GENERATED_BODY()
public:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
};
