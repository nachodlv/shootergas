// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "ShooterGAS/ShooterGAS.h"

#include "ShooterGasGameplayAbility.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERGAS_API UShooterGasGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	EShooterGASAbilityInputID AbilityInputID = EShooterGASAbilityInputID::None;
};
