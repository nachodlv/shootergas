// Fill out your copyright notice in the Description page of Project Settings.


#include "LineTraceFireGameplayAbility.h"


#include "Kismet/GameplayStatics.h"
#include "ShooterGAS/Characters/ShooterGASCharacter.h"
#include "ShooterGAS/Characters/ShooterGASGun.h"
#include "ShooterGAS/GameAbilitySystem/ShooterGASAbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "GameplayAbilities/Public/Abilities/Tasks/AbilityTask.h"
#include "GameplayAbilities/Public/Abilities/GameplayAbilityTargetTypes.h"
#include "ShooterGAS/GameAbilitySystem/GameplayAbilityTasks/ShooterGASWaitTargetData.h"

ULineTraceFireGameplayAbility::ULineTraceFireGameplayAbility()
{
	AbilityInputID = EShooterGASAbilityInputID::PullTrigger;
	SetByCallerDamage = FGameplayTag::RequestGameplayTag(FName("SetByCaller.Damage"));
}

void ULineTraceFireGameplayAbility::FireBullet()
{
	UAbilitySystemComponent* OwnerASC = CurrentActorInfo->AbilitySystemComponent.Get();
	if (!OwnerASC)
	{
		return;
	}
	FScopedPredictionWindow	ScopedPrediction(OwnerASC, !OwnerASC->ScopedPredictionKey.IsValidForMorePrediction());

	FHitResult Hit = LineTrace();
	FGameplayAbilityTargetData_SingleTargetHit TargetData(Hit);
	FGameplayAbilityTargetData_SingleTargetHit* CopyTargetData = new FGameplayAbilityTargetData_SingleTargetHit();
	*CopyTargetData = TargetData;
	FGameplayAbilityTargetDataHandle DataHandle(CopyTargetData);
	OwnerASC->CallServerSetReplicatedTargetData(CurrentSpecHandle,
	                                            CurrentActivationInfo.GetActivationPredictionKey(), DataHandle,
	                                            FGameplayTag(), OwnerASC->GetPredictionKeyForNewAction());
	if (IsPredictingClient())
	{
		ValidTargetData(DataHandle);
	}
}

void ULineTraceFireGameplayAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                                    const FGameplayAbilityActorInfo* ActorInfo,
                                                    const FGameplayAbilityActivationInfo ActivationInfo,
                                                    const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		WaitTargetData = UShooterGASWaitTargetData::WaitClientTargetData(this, FName(), true);
		WaitTargetData->ValidData.AddDynamic(this, &ULineTraceFireGameplayAbility::ValidTargetData);
		WaitTargetData->ReadyForActivation();

		FireBullet();
	}
}

void ULineTraceFireGameplayAbility::InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
{
	EndAbility(Handle, ActorInfo, ActivationInfo, true , false);
}

void ULineTraceFireGameplayAbility::ValidTargetData(const FGameplayAbilityTargetDataHandle& Data)
{
	if (!CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo))
	{
		CancelAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true);
		return;
	}

	if (AShooterGASCharacter* Character = Cast<AShooterGASCharacter>(CurrentActorInfo->AvatarActor.Get()))
	{

		if (UShooterGASAbilitySystemComponent* ASC = Cast<UShooterGASAbilitySystemComponent>(Character->GetAbilitySystemComponent()))
		{
			const FGameplayAbilityTargetData_SingleTargetHit TargetData = *(static_cast<const FGameplayAbilityTargetData_SingleTargetHit*>(Data.Get(0)));
			FHitResult HitResult = LineTrace();

			IAbilitySystemInterface* HitActor = Cast<IAbilitySystemInterface>(HitResult.Actor);
			UAbilitySystemComponent* HitActorAbilitySystem = HitActor ? HitActor->GetAbilitySystemComponent() : nullptr;

			// Execute the Gameplay CUEs
			FGameplayCueParameters CueParameters (GetContextFromOwner(Data));
			CueParameters.Instigator = GetActorInfo().OwnerActor;
			ASC->ExecuteGameplayCue(CueTag, CueParameters);
			Character->MakeNoise(NoiseSettings.Loudness, Character, HitResult.Location, NoiseSettings.MaxRange, NoiseSettings.Tag);

			if(TSharedPtr<FGameplayEffectSpec> Spec = MakeOutgoingGameplayEffectSpec(FireDamage).Data)
			{
				if(AShooterGASGun* Gun = Character->GetGun())
				{
					// Need to multiply by -1 because the modifier operation is "Add"
					Spec->SetSetByCallerMagnitude(SetByCallerDamage, Gun->CalculateDamage(HitResult.BoneName) * -1);
				}
				if (HitActorAbilitySystem)
				{
					ASC->ApplyGameplayEffectSpecToTarget(*Spec.Get(), HitActorAbilitySystem, ASC->GetPredictionKeyForNewAction());
				}
			}
		}
	}

	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}


FHitResult ULineTraceFireGameplayAbility::LineTrace() const
{
	const AShooterGASCharacter* Character = Cast<AShooterGASCharacter>(CurrentActorInfo->OwnerActor);
	if (!Character)
	{
		return FHitResult();
	}
	const AController* Controller = Character->GetController();
	const AShooterGASGun* Gun = Character ? Character->GetGun() : nullptr;
	if (!Controller || !Gun)
	{
		return FHitResult();
	}

	FVector ViewportLocation;
	FRotator ViewportRotator;
	Controller->GetPlayerViewPoint(ViewportLocation, ViewportRotator);
	const FVector End = ViewportLocation + ViewportRotator.Vector() * Gun->GetMaxRange();

	const UWorld* World = GetWorld();
	FHitResult Hit;
	FCollisionQueryParams QueryParams;

	// Ignore the actor firing the ability
	if(CurrentActorInfo)
	{
		QueryParams.AddIgnoredActor(CurrentActorInfo->AvatarActor.Get());
	}

	World->LineTraceSingleByChannel(Hit, ViewportLocation, End, ECC_GameTraceChannel1, QueryParams);
	return Hit;
}
