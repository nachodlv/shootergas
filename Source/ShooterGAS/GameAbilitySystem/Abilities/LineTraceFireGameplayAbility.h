// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ShooterGasGameplayAbility.h"
#include "ShooterGAS/Characters/ShooterGASCharacter.h"


#include "LineTraceFireGameplayAbility.generated.h"

class UShooterGASWaitTargetData;

USTRUCT()
struct FNoiseSettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	float Loudness = 1.0f;

	UPROPERTY(EditAnywhere)
	float MaxRange = 1000.f;

	UPROPERTY(EditAnywhere)
	FName Tag;
};

UCLASS()
class SHOOTERGAS_API ULineTraceFireGameplayAbility : public UShooterGasGameplayAbility
{
	GENERATED_BODY()
public:
	ULineTraceFireGameplayAbility();

private:
	UPROPERTY(EditAnywhere, Category="ShooterGAS|Gameplay Effect")
	TSubclassOf<UGameplayEffect> FireDamage;
	UPROPERTY(EditAnywhere, Category="ShooterGAS|Gameplay Effect")
	FGameplayTag SetByCallerDamage;

	UPROPERTY(Transient)
	class UShooterGASWaitTargetData* WaitTargetData = nullptr;

	UPROPERTY(EditAnywhere, Category="ShooterGAS|Cue")
	FGameplayTag CueTag;

	UPROPERTY(EditAnywhere, Category="ShooterGAS")
	FNoiseSettings NoiseSettings;

	void FireBullet();

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	                             const FGameplayAbilityActivationInfo ActivationInfo,
	                             const FGameplayEventData* TriggerEventData) override;

	virtual void InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;

	UFUNCTION()
	void ValidTargetData(const FGameplayAbilityTargetDataHandle& Data);

	FHitResult LineTrace() const;
};
