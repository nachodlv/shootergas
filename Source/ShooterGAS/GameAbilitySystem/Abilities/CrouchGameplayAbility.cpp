﻿#include "CrouchGameplayAbility.h"

#include "AbilitySystemComponent.h"

UCrouchGameplayAbility::UCrouchGameplayAbility()
{
	AbilityInputID = EShooterGASAbilityInputID::Crouch;
}

void UCrouchGameplayAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                             const FGameplayAbilityActorInfo* ActorInfo,
                                             const FGameplayAbilityActivationInfo ActivationInfo,
                                             const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			CancelAbility(Handle, ActorInfo, ActivationInfo, true);
		}
		UAbilitySystemComponent* ASC = ActorInfo ? ActorInfo->AbilitySystemComponent.Get() : nullptr;
		if (!ASC)
		{
			return;
		}
		const FGameplayEffectContextHandle EffectHandle = MakeEffectContext(Handle, ActorInfo);
		ASC->ApplyGameplayEffectToSelf(CrouchGameplayEffect.GetDefaultObject(), 1, EffectHandle);
	}
}

void UCrouchGameplayAbility::InputReleased(const FGameplayAbilitySpecHandle Handle,
                                           const FGameplayAbilityActorInfo* ActorInfo,
                                           const FGameplayAbilityActivationInfo ActivationInfo)
{
	EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
}

void UCrouchGameplayAbility::EndAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
	UnCrouch();
}

void UCrouchGameplayAbility::CancelAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateCancelAbility)
{
	Super::CancelAbility(Handle, ActorInfo, ActivationInfo, bReplicateCancelAbility);
	UnCrouch();
}

void UCrouchGameplayAbility::UnCrouch() const
{
	UAbilitySystemComponent* ASC = CurrentActorInfo->AbilitySystemComponent.Get();
	if (!ASC)
	{
		return;
	}

	const FGameplayEffectContextHandle EffectHandle = MakeEffectContext(CurrentSpecHandle, CurrentActorInfo);
	ASC->ApplyGameplayEffectToSelf(CrouchGameplayEffect.GetDefaultObject(), 1, EffectHandle);
}


