﻿

#pragma once

#include "CoreMinimal.h"

#include "ShooterGasGameplayAbility.h"
#include "UObject/Object.h"
#include "CrouchGameplayAbility.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERGAS_API UCrouchGameplayAbility : public UShooterGasGameplayAbility
{
	GENERATED_BODY()

public:
	UCrouchGameplayAbility();

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	virtual void CancelAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility) override;
private:
	UPROPERTY(EditDefaultsOnly, Category="ShooterGAS", meta=(AllowPrivateAccess="true"))
	TSubclassOf<UGameplayEffect> CrouchGameplayEffect;
	UPROPERTY(EditDefaultsOnly, Category="ShooterGAS", meta=(AllowPrivateAccess="true"))
	TSubclassOf<UGameplayEffect> UnCrouchGameplayEffect;

	void UnCrouch() const;
};
