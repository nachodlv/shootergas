// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGASAbilitySystemComponent.h"

#include "GameplayCueManager.h"
#include "AbilitySystemGlobals.h"

void UShooterGASAbilitySystemComponent::ExecuteGameplayCueLocal(const FGameplayTag GameplayCueTag,
                                                                const FGameplayCueParameters& GameplayCueParameters)
{
	UAbilitySystemGlobals::Get().GetGameplayCueManager()->HandleGameplayCue(GetOwner(), GameplayCueTag, EGameplayCueEvent::Type::Executed, GameplayCueParameters);
}

void UShooterGASAbilitySystemComponent::OnTagUpdated(const FGameplayTag& Tag, bool TagExists)
{
	Super::OnTagUpdated(Tag, TagExists);
	OnTagUpdate.Broadcast(Tag, TagExists);
}
