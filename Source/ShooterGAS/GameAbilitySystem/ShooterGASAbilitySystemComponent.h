// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "ShooterGASAbilitySystemComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnTagUpdate, const FGameplayTag&, bool)
UCLASS()
class SHOOTERGAS_API UShooterGASAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()
public:
	FOnTagUpdate OnTagUpdate;

	UFUNCTION(BlueprintCallable, Category = "GameplayCue", Meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void ExecuteGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	virtual void OnTagUpdated(const FGameplayTag& Tag, bool TagExists) override;
};
