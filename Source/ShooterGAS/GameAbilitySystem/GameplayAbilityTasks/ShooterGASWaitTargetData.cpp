﻿#include "ShooterGASWaitTargetData.h"

#include "AbilitySystemComponent.h"

UShooterGASWaitTargetData::UShooterGASWaitTargetData()
{
}

UShooterGASWaitTargetData* UShooterGASWaitTargetData::WaitClientTargetData(UGameplayAbility* OwningAbility,
	FName TaskInstanceName, bool bInTriggerOnce)
{
	UShooterGASWaitTargetData* MyObj = NewAbilityTask<UShooterGASWaitTargetData>(OwningAbility, TaskInstanceName);
	MyObj->bTriggerOnce = bInTriggerOnce;
	return MyObj;
}

void UShooterGASWaitTargetData::Activate()
{
	Super::Activate();

	if(!Ability || !Ability->GetCurrentActorInfo()->IsNetAuthority())
	{
		return;
	}

	const FGameplayAbilitySpecHandle SpecHandle = GetAbilitySpecHandle();
	const FPredictionKey PredictionKey = GetActivationPredictionKey();
	TargetDataDelegateHandle = AbilitySystemComponent->AbilityTargetDataSetDelegate(SpecHandle, PredictionKey).AddUObject(this, &UShooterGASWaitTargetData::OnTargetDataReplicatedCallback);
}

void UShooterGASWaitTargetData::OnTargetDataReplicatedCallback(const FGameplayAbilityTargetDataHandle& Data, FGameplayTag ActivationTag)
{
	AbilitySystemComponent->ConsumeClientReplicatedTargetData(GetAbilitySpecHandle(), GetActivationPredictionKey());
	if (ShouldBroadcastAbilityTaskDelegates())
	{
		ValidData.Broadcast(Data);
	}
	if (bTriggerOnce)
	{
		EndTask();
	}
}

void UShooterGASWaitTargetData::OnDestroy(bool bInOwnerFinished)
{
	if(AbilitySystemComponent)
	{
		FGameplayAbilitySpecHandle SpecHandle = GetAbilitySpecHandle();
		FPredictionKey PredictionKey = GetActivationPredictionKey();
		AbilitySystemComponent->AbilityTargetDataSetDelegate(SpecHandle, PredictionKey).Remove(TargetDataDelegateHandle);
	}
	Super::OnDestroy(bInOwnerFinished);
}
