﻿

#pragma once

#include "CoreMinimal.h"

#include "Abilities/Tasks/AbilityTask.h"
#include "Abilities/Tasks/AbilityTask_WaitTargetData.h"
#include "UObject/Object.h"
#include "ShooterGASWaitTargetData.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERGAS_API UShooterGASWaitTargetData : public UAbilityTask
{
	GENERATED_BODY()
public:
	UShooterGASWaitTargetData();

	UFUNCTION(BlueprintCallable, meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "true", HideSpawnParms = "Instigator"), Category = "ShooterGAS|Ability|Tasks")
	static UShooterGASWaitTargetData* WaitClientTargetData(UGameplayAbility* OwningAbility, FName TaskInstanceName, bool bInTriggerOnce = true);

	virtual void Activate() override;

	UPROPERTY(BlueprintAssignable)
	FWaitTargetDataDelegate ValidData;

protected:
	bool bTriggerOnce = false;
	FDelegateHandle TargetDataDelegateHandle;

	UFUNCTION()
	void OnTargetDataReplicatedCallback(const FGameplayAbilityTargetDataHandle& Data, FGameplayTag ActivationTag);

	virtual void OnDestroy(bool bInOwnerFinished) override;
};
