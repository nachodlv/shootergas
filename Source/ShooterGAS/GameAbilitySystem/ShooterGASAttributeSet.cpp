// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGASAttributeSet.h"


#include "Net/UnrealNetwork.h"

UShooterGASAttributeSet::UShooterGASAttributeSet()
{
}

void UShooterGASAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME_CONDITION_NOTIFY(UShooterGASAttributeSet, Health, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UShooterGASAttributeSet, MaxHealth, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UShooterGASAttributeSet, Damage, COND_None, REPNOTIFY_Always);
}

void UShooterGASAttributeSet::OnRep_Health(const FGameplayAttributeData& OldHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UShooterGASAttributeSet, Health, OldHealth);
}

void UShooterGASAttributeSet::OnRep_MaxHealth(const FGameplayAttributeData& OldMaxHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UShooterGASAttributeSet, MaxHealth, OldMaxHealth);
}

void UShooterGASAttributeSet::OnRep_Damage(const FGameplayAttributeData& OldDamage)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UShooterGASAttributeSet, Damage, OldDamage);
}
