﻿

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "GlobalTags.generated.h"

/**
 *
 */
UCLASS(config = Game)
class SHOOTERGAS_API UGlobalTags : public UObject
{
	GENERATED_BODY()

public:
	UGlobalTags();

	/** The class to instantiate as the globals object. Defaults to this class but can be overridden */
	UPROPERTY(Config)
	FSoftClassPath ClassName;

	static const FName Player;
};
