﻿#include "ShooterGASEngineSubsystem.h"

#include "AbilitySystemGlobals.h"

void UShooterGASEngineSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	UAbilitySystemGlobals::Get().InitGlobalData();
}
