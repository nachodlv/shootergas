// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGASPlayerState.h"
#include "AbilitySystemComponent.h"
#include "ShooterGAS/GameAbilitySystem/ShooterGASAbilitySystemComponent.h"
#include "ShooterGAS/GameAbilitySystem/ShooterGASAttributeSet.h"

AShooterGASPlayerState::AShooterGASPlayerState()
{
	DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
	CrouchTag = FGameplayTag::RequestGameplayTag(FName("State.Crouch"));

	// Create ability system component, and set it to be explicitly replicated
	AbilitySystemComponent = CreateDefaultSubobject<UShooterGASAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);

	// Mixed mode means we only are replicated the GEs to ourself, not the GEs to simulated proxies. If another GDPlayerState (Hero) receives a GE,
	// we won't be told about it by the Server. Attributes, GameplayTags, and GameplayCues will still replicate to us.
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	// Create the attribute set, this replicates by default
	// Adding it as a subobject of the owning actor of an AbilitySystemComponent
	// automatically registers the AttributeSet with the AbilitySystemComponent
	AttributeSet = CreateDefaultSubobject<UShooterGASAttributeSet>(TEXT("AttributeSetBase"));

	HealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetHealthAttribute()).AddUObject(this, &AShooterGASPlayerState::HealthChanged);
}

UShooterGASAttributeSet* AShooterGASPlayerState::GetAttributeSet() const
{
	return AttributeSet;
}

UAbilitySystemComponent* AShooterGASPlayerState::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

float AShooterGASPlayerState::GetHealth() const
{
	return AttributeSet ? AttributeSet->GetHealth() : 1.0f;
}

float AShooterGASPlayerState::GetMaxHealth() const
{
	return AttributeSet ? AttributeSet->GetMaxHealth() : 1.0f;
}

bool AShooterGASPlayerState::HasMinimalReplicatedTag(const FGameplayTag Tag) const
{
	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	// This can be simplify with a return but the compiler doesn't want to.
	if (ASC && ASC->GetMinimalReplicationTags().TagMap.Find(Tag))
	{
		return true;
	}
	return false;
}

void AShooterGASPlayerState::HealthChanged(const FOnAttributeChangeData& Data)
{
	const float NewHealth = Data.NewValue;
	OnHealthChanged.Broadcast(Data.OldValue, NewHealth);
	if (NewHealth <= 0 && !HasMinimalReplicatedTag(DeadTag))
	{
		AbilitySystemComponent->AddMinimalReplicationGameplayTag(DeadTag);
		OnDie.Broadcast();
	}

}
