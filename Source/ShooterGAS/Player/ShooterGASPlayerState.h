// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include "GameFramework/PlayerState.h"
#include "ShooterGASPlayerState.generated.h"

class UShooterGASAbilitySystemComponent;
class UShooterGASAttributeSet;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FHealthChange, float, OldHealth, float, NewHealth);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDie);
/**
 *
 */
UCLASS()
class SHOOTERGAS_API AShooterGASPlayerState : public APlayerState, public IAbilitySystemInterface

{
	GENERATED_BODY()
public:
	FDelegateHandle HealthChangedDelegateHandle;

	UPROPERTY(BlueprintAssignable)
	FHealthChange OnHealthChanged;

	UPROPERTY(BlueprintAssignable)
	FOnDie OnDie;

	AShooterGASPlayerState();
	UShooterGASAttributeSet* GetAttributeSet() const;
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintPure)
	float GetHealth() const;

	float GetMaxHealth() const;

	const FGameplayTag& GetCrouchTag() const { return CrouchTag; }

	UFUNCTION(BlueprintPure)
	bool HasMinimalReplicatedTag(const FGameplayTag Tag) const;

private:
	FGameplayTag DeadTag;
	FGameplayTag CrouchTag;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="GameplayAbility", meta = (AllowPrivateAccess = "true"))
	UShooterGASAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="GameplayAbility", meta = (AllowPrivateAccess = "true"))
	UShooterGASAttributeSet* AttributeSet;

	void HealthChanged(const struct FOnAttributeChangeData& Data);
};
