# PMT Common

## Overview

This plugin contains a common set of components, actors, functions and other utilities that are commonly used by any games. It does not contain any other dependency except of course UE plugins like GameplayTags or similars.

## Table of Contents

1. [Actor Components](#actor-components-1)
1.1 [Combo Component](#combo-component)
2. [Debugging Utilities](#debugging-utils-1)
2.1 [Toggles](#debugging-toggle)
2.2 [Categories](#debugging-categories)
2.3 [Functions](#debugging-functions)
3. [Structures & Utilities](#structures-utils-1)
3.1 [Tag Attributes](#tag-attribute)
3.1.1 [Modifiers](#tag-attribute-modifier)
3.2 [Custom Data](#custom-data)
3.3 [Circular Buffer](#circular-buffer)

<a name="actor-components-1"></a>
## Actor Components
<a name="combo-component"></a>
### Combo Component (PMTCInputListenerComponent)
This component works as a listener for input without blocking the main input. You can think the usage of this like a combo buffer for games like Street Fighter or to just start a "combo window" (limited time to press a key or a set of keys) to trigger a specific action. This also works for AI controllers, since you can use `SimulateInputEvent` which takes a struct of data that represents an action or axis.

To make it work, it's just attaching this to anything you want to listen for events (this could either be a pawn or a controller) and call the corresponding methods to start/stop listening. To do so, call `PushInputListener` and `PopInputListener` respectively. There are three events you can listen for that are for start, finished and input events (called both in player and ai controllers).

<a name="debugging-utils-1"></a>
## Debugging Utilities
In case you desire to log any variable in runtime (**only in editor builds**), this plugin provides an easy way to display information in 3D for any object in the world. To do so, we created some custom meta properties for *UPROPERTY* called `PMTCDebugToggleBy` and `PMTCDebugCategory`.

<a name="debugging-toggle"></a>
### Toggles
You can toggle on/off log information of any variable of an object by using the meta `PMTCDebugToggleBy`. We strongly suggest that you create some editor-only variables for debugging like the following example.

```
#if WITH_EDITORONLY_DATA

// These variables are only in editor builds.
UPROPERTY(EditAnywhere)
bool bDebugMyVariable = true;

#endif // WITH_EDITORONLY_DATA

UPROPERTY(meta = (PMTCDebugToggleBy="bDebugMyVariable"))
int32 MyVariable = 5;
```
Now whenever you toggle off/on your boolean `bDebugMyVariable`, log info will be hidden/displayed.

<a name="debugging-toggle"></a>
### Categories
If you want to toggle multiple variables to display log information rather than toggling one by one, you can use categories instead of toggles. To do so, you can follow the next example.

```
#if WITH_EDITORONLY_DATA

// These variables are only in editor builds.
UPROPERTY(EditAnywhere)
bool bDebugStatic = true;

#endif // WITH_EDITORONLY_DATA

UPROPERTY(meta = (PMTCDebugCategory="bDebugStatic"))
int32 MyVariable = 5;

UPROPERTY(meta = (PMTCDebugCategory="bDebugStatic"))
int32 MyOtherVariable = 2;
```
Now whenever you toggle off/on your boolean `bDebugStatic`,  both variables will be hidden/displayed rather than toggling one boolean for each.

Both options, toggles and categories can be used together in case you desire maximum customization of debugging. To do so, you just add them together.

```
#if WITH_EDITORONLY_DATA

// These variables are only in editor builds.
UPROPERTY(EditAnywhere)
bool bDebugStatic = true;

UPROPERTY(EditAnywhere)
bool bDebugMyVariable = false;

#endif // WITH_EDITORONLY_DATA

UPROPERTY(meta = (PMTCDebugToggleBy = "bDebugMyVariable", PMTCDebugCategory="bDebugStatic"))
int32 MyVariable = 5;

UPROPERTY(meta = (PMTCDebugCategory="bDebugStatic"))
int32 MyOtherVariable = 2;
```
<a name="debugging-functions"></a>
### Functions
To actually enable debugging in 3D, you need to register/unregister the desired object using the following macro:

```
PMTC_REGISTER_UNREGISTER_FOR_3DLOG(<object>, <bool condition>);
```
You can call it anywhere you want, but two places that are good enough are `BeginPlay` (register), `EndPlay` (unregister) and `PostEditChangeProperty`. You can add a boolean that turn on/off completely the debugging and use that. An example of this is the following.

```

void MyClass::BeginPlay()
{
	Super::BeginPlay();
	PMTC_REGISTER_UNREGISTER_FOR_3DLOG(this, bMyToggleBool);
}

void MyClass::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	// Make sure to unregister when destroyed.
	PMTC_REGISTER_UNREGISTER_FOR_3DLOG(this, false); 
	Super::EndPlay(EndPlayReason);
}

#if WITH_EDITOR

void MyClass::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	// This is used to react to changes in instances from editor.
	Super::PostEditChangeProperty(PropertyChangedEvent);
	if (PropertyChangedEvent.Property && PropertyChangedEvent.Property->GetNameCPP() == GET_MEMBER_NAME_STRING_CHECKED(MyClass, bMyToggleBool))
	{
		PMTC_REGISTER_UNREGISTER_FOR_3DLOG(this, bMyToggleBool);
	}
}

#endif // WITH_EDITOR
```

<a name="structures-utils-1"></a>
## Structures & Utilities
The plugin contains a set of other structures and utilities that can be helpful in the development of any game.

<a name="tag-attribute"></a>
### Tag Attributes
There was a desire to implement a simplified version of attributes like GAS does with them. Bringing GAS for simple things like max amount of bullets that are rarely modified and with specific values is too much, but we still want to have some flexibility. A `TagAttribute` joins a `GameplayTag` with a `Max value` and `Current value`. The structure is called `FPMTCTagAttribute` and it is pretty straightforward to understand. It supports stacking, multiplicative, additive and divide operations (see `EPMTCOperationType`).

The order of modifier's application is shown in the following equation.

`Value = ( (BaseValue + Additive) * Multiplicative ) / Divider`

<a name="tag-attribute-modifier"></a>
### Modifiers
The concept of modifiers is similar to what a `Modifier` is for `GAS`. A simple class that modifies a value using an operation. Again, this is simple to understand (see `UPMTCTagAttributeModifier`)

<a name="custom-data"></a>
### Custom Data
A structure called `FPMTCCustomData` was created to send any data we desire via replication (same concept used by `GAS` for `FGameplayAbilityTargetData`).

To do so, we also had to create a handle that contains a sharedptr of the above structure called `FPMTCCustomDataHandle`. The usage is pretty simple. See the following example.

```
// Macro mandatory.
USTRUCT()
struct FMyCustomData : public FPMTCCustomData
{
	// Macro mandatory
	GENERATED_BODY()

public:

	// Also mandatory.
	UScriptStruct* GetScriptStruct() const override
	{
		return FMyCustomData::StaticStruct();
	}

	/** Mandatory. Here you serialize all your data. */
	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
	{
		Ar << Value;
	}

protected:

	UPROPERTY(Transient)
	int32 Value = 0;
};

template<>
struct TStructOpsTypeTraits<FMyCustomData> : public TStructOpsTypeTraitsBase2<FMyCustomData>
{
	enum
	{
		WithNetSerializer = true // This is mandatory.
	};
};
```
Then you can instantiate your data like the following

```
FPMTCCustomDataHandle Handle;
Handle.Data = TSharedPtr<FPMTCCustomData>(new FMyCustomData());
FMyCustomData* MyData = static_cast<FMyCustomData*>(Handle.Data.Get());
// Do anything you want with your data.
```

<a name="circular-buffer"></a>
### Circular Buffer
TODO(Brian): Document circular buffer.