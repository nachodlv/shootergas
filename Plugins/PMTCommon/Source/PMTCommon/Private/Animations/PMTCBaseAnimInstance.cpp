// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Animations/PMTCBaseAnimInstance.h"

// PMTC Includes
#include "Animations/PMTCAnimInstanceDataProxy.h"
#include "Interfaces/PMTCAnimDataProxyUpdateInterface.h"

UPMTCBaseAnimInstance::UPMTCBaseAnimInstance()
{
	AnimInstanceDataProxyClass = UPMTCAnimInstanceDataProxy::StaticClass();
}

void UPMTCBaseAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	AnimInstanceDataProxy = NewObject<UPMTCAnimInstanceDataProxy>(AnimInstanceDataProxyClass.LoadSynchronous());
	CacheProperties();
}

void UPMTCBaseAnimInstance::NativeUninitializeAnimation()
{
	Super::NativeUninitializeAnimation();
	AnimInstanceDataProxy = nullptr;
	CachedProperties.Reset();
}

void UPMTCBaseAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	Super::NativeUpdateAnimation(DeltaTime);
	TScriptInterface<IPMTCAnimDataProxyUpdateInterface> Interface = GetProxyUpdateInterface();
	if (AnimInstanceDataProxy && Interface)
	{
		Interface->UpdateAnimProxyData(AnimInstanceDataProxy, DeltaTime);
		if (bUpdateWithReflection)
		{
			UpdateUsingReflection(DeltaTime);
		}
	}
}

UPMTCAnimInstanceDataProxy* UPMTCBaseAnimInstance::GetAnimInstanceDataProxy() const
{
	return AnimInstanceDataProxy;
}

void UPMTCBaseAnimInstance::UpdateUsingReflection(float DeltaTime)
{
	if (!AnimInstanceDataProxy)
	{
		return;
	}
	for (auto& PropPair : CachedProperties)
	{
		const FName& PropName = PropPair.Key;
		// Make sure that any of the properties from the parent are not picked up.
		if (!AnimInstanceDataProxy->DoesValueExists(PropName))
		{
			continue;
		}
		FProperty* Property = PropPair.Value;
		if (FBoolProperty* BoolProperty = CastField<FBoolProperty>(Property))
		{
			BoolProperty->SetPropertyValue_InContainer(this, AnimInstanceDataProxy->GetBoolValue(PropName), 0);
		}
		else if (FFloatProperty* FloatProperty = CastField<FFloatProperty>(Property))
		{
			FloatProperty->SetPropertyValue_InContainer(this, AnimInstanceDataProxy->GetFloatValue(PropName), 0);
		}
		else if (FIntProperty* IntProperty = CastField<FIntProperty>(Property))
		{
			IntProperty->SetPropertyValue_InContainer(this, AnimInstanceDataProxy->GetIntValue(PropName), 0);
		}
		else if (FStructProperty* StructProperty = CastField<FStructProperty>(Property))
		{
			if (StructProperty->Struct == TBaseStructure<FVector>::Get())
			{
				const FVector VectorValue = AnimInstanceDataProxy->GetVectorValue(PropName);
				*StructProperty->ContainerPtrToValuePtr<FVector>(this) = VectorValue;
			}
			else if (StructProperty->Struct == TBaseStructure<FRotator>::Get())
			{
				const FRotator RotatorValue = AnimInstanceDataProxy->GetRotatorValue(PropName);
				*StructProperty->ContainerPtrToValuePtr<FRotator>(this) = RotatorValue;
			}
			else if (StructProperty->Struct == TBaseStructure<FTransform>::Get())
			{
				const FTransform TransformValue = AnimInstanceDataProxy->GetTransformValue(PropName);
				*StructProperty->ContainerPtrToValuePtr<FTransform>(this) = TransformValue;
			}
		}
	}
}

TScriptInterface<IPMTCAnimDataProxyUpdateInterface> UPMTCBaseAnimInstance::GetProxyUpdateInterface()
{
	if (ProxyUpdateInterface)
	{
		return ProxyUpdateInterface;
	}
	ProxyUpdateInterface = GetSkelMeshComponent()->GetOwner();
	return ProxyUpdateInterface;
}

void UPMTCBaseAnimInstance::CacheProperties()
{
	CachedProperties.Reset();
	for (TFieldIterator<FProperty> It(GetClass(), EFieldIteratorFlags::IncludeSuper, EFieldIteratorFlags::ExcludeDeprecated, EFieldIteratorFlags::ExcludeInterfaces); It; ++It)
	{
		FName PropName = It->GetFName();
		FProperty* Property = *It;
		const bool bSupportedProperty =
			CastField<FBoolProperty>(Property) != nullptr ||
			CastField<FIntProperty>(Property) != nullptr ||
			CastField<FFloatProperty>(Property) != nullptr;// ||
			// TODO(Brian): Support enum and byte properties.
			//CastField<FByteProperty>(Property) != nullptr ||
			//CastField<FEnumProperty>(Property) != nullptr;

		if (bSupportedProperty)
		{
			CachedProperties.Add(PropName, Property);
		}
		else if (FStructProperty* StructProperty = CastField<FStructProperty>(Property))
		{
			const bool bSupportedStruct =
				StructProperty->Struct == TBaseStructure<FVector>::Get() ||
				StructProperty->Struct == TBaseStructure<FRotator>::Get() ||
				StructProperty->Struct == TBaseStructure<FTransform>::Get();
			if (bSupportedStruct)
			{
				CachedProperties.Add(PropName, Property);
			}
		}
	}
}
