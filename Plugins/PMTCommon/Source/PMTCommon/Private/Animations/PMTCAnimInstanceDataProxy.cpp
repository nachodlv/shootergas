// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Animations/PMTCAnimInstanceDataProxy.h"

namespace
{
	template<typename T>
	T GetAnimValue(FName InName, const TMap<FName, T>& InMap, const T& DefaultValue)
	{
		const T* Value = InMap.Find(InName);
		if (Value)
		{
			return *Value;
		}
		return DefaultValue;
	}

	template<typename T>
	T SetAnimValue(FName InName, TMap<FName, T>& InMap, const T& InValue)
	{
		return InMap.Add(InName, InValue);
	}
}

bool UPMTCAnimInstanceDataProxy::SetBoolValue(FName InName, bool InValue)
{
	return SetAnimValue<bool>(InName, BoolValues, InValue);
}

int32 UPMTCAnimInstanceDataProxy::SetIntValue(FName InName, int32 InValue)
{
	return SetAnimValue<int32>(InName, IntValues, InValue);
}

float UPMTCAnimInstanceDataProxy::SetFloatValue(FName InName, float InValue)
{
	return SetAnimValue<float>(InName, FloatValues, InValue);
}

FVector UPMTCAnimInstanceDataProxy::SetVectorValue(FName InName, FVector InValue)
{
	return SetAnimValue<FVector>(InName, VectorValues, InValue);
}

FRotator UPMTCAnimInstanceDataProxy::SetRotatorValue(FName InName, FRotator InValue)
{
	return SetAnimValue<FRotator>(InName, RotatorValues, InValue);
}

FTransform UPMTCAnimInstanceDataProxy::SetTransformValue(FName InName, FTransform InValue)
{
	return SetAnimValue<FTransform>(InName, TransformValues, InValue);
}

bool UPMTCAnimInstanceDataProxy::GetBoolValue(FName InName)
{
	return GetAnimValue<bool>(InName, BoolValues, false);
}

int32 UPMTCAnimInstanceDataProxy::GetIntValue(FName InName)
{
	return GetAnimValue<int32>(InName, IntValues, INDEX_NONE);
}

float UPMTCAnimInstanceDataProxy::GetFloatValue(FName InName)
{
	return GetAnimValue<float>(InName, FloatValues, TNumericLimits<float>::Min());
}

FVector UPMTCAnimInstanceDataProxy::GetVectorValue(FName InName)
{
	return GetAnimValue<FVector>(InName, VectorValues, FVector::ZeroVector);
}

FRotator UPMTCAnimInstanceDataProxy::GetRotatorValue(FName InName)
{
	return GetAnimValue<FRotator>(InName, RotatorValues, FRotator::ZeroRotator);
}

FTransform UPMTCAnimInstanceDataProxy::GetTransformValue(FName InName)
{
	return GetAnimValue<FTransform>(InName, TransformValues, FTransform());
}

bool UPMTCAnimInstanceDataProxy::DoesValueExists(FName ValueName) const
{
	return
		BoolValues.Contains(ValueName) ||
		IntValues.Contains(ValueName) ||
		FloatValues.Contains(ValueName) ||
		VectorValues.Contains(ValueName) ||
		RotatorValues.Contains(ValueName) ||
		TransformValues.Contains(ValueName);
}
