// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Actors/PMTCAbilitySystemProxy.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "AttributeSet.h"
#include "GameFramework/Controller.h"

APMTCAbilitySystemProxy::APMTCAbilitySystemProxy()
{
	PrimaryActorTick.bCanEverTick = false;

	AbilitySystemComponentClass = UAbilitySystemComponent::StaticClass();
	MainAttributeSetClass = UAttributeSet::StaticClass();
}

TArray<APMTCAbilitySystemProxy*> APMTCAbilitySystemProxy::CreateProxiesForController(
	AController* InController,
	TSubclassOf<APMTCAbilitySystemProxy> ProxyClass, int32 InCount)
{
	check(InController);
	check(ProxyClass);
	check(InCount > 0);

	TArray<APMTCAbilitySystemProxy*> Proxies;
	Proxies.Reserve(InCount);

	UWorld* World = InController->GetWorld();
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = InController;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	for (int32 CurrentCount = 0; CurrentCount < InCount; ++CurrentCount)
	{
		APMTCAbilitySystemProxy* Proxy = World->SpawnActor<APMTCAbilitySystemProxy>(ProxyClass, FTransform(), SpawnParameters);
		check(Proxy);
		Proxy->InitializeProxy(InController);
		Proxies.Add(Proxy);
	}
	return Proxies;
}

UAbilitySystemComponent* APMTCAbilitySystemProxy::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

UAttributeSet* APMTCAbilitySystemProxy::GetMainAttributeSet() const
{
	return MainAttributeSet;
}

FActiveGameplayEffectHandle APMTCAbilitySystemProxy::ApplyGameplayEffect(FGameplayEffectSpecHandle SpecHandle)
{
	FActiveGameplayEffectHandle ReturnHandle;
	if (SpecHandle.IsValid())
	{
		ReturnHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data.Get());
		if (ReturnHandle.IsValid())
		{
			ActiveGameplayEffects.Add(ReturnHandle);
		}
	}
	return ReturnHandle;
}

void APMTCAbilitySystemProxy::RemoveGameplayEffect(FActiveGameplayEffectHandle InEffectHandle, int32 StacksToRemove)
{
	int32 Index = 0;
	for (FActiveGameplayEffectHandle EffectHandle : ActiveGameplayEffects)
	{
		if (EffectHandle == InEffectHandle)
		{
			AbilitySystemComponent->RemoveActiveGameplayEffect(EffectHandle, StacksToRemove);
			ActiveGameplayEffects.RemoveSingleSwap(Index);
			break;
		}
		++Index;
	}
}

void APMTCAbilitySystemProxy::RemoveAllEffects()
{
	for (FActiveGameplayEffectHandle EffectHandle : ActiveGameplayEffects)
	{
		AbilitySystemComponent->RemoveActiveGameplayEffect(EffectHandle);
	}
	ActiveGameplayEffects.Reset();
}

bool APMTCAbilitySystemProxy::HasObjectLinked() const
{
	return OwnerHandle.IsValid();
}

bool APMTCAbilitySystemProxy::MatchesHandle(FPMTCSimpleHandle InHandle) const
{
	return InHandle.IsValid() && OwnerHandle.IsValid() && InHandle == OwnerHandle;
}

FPMTCSimpleHandle APMTCAbilitySystemProxy::GetLinkedHandle() const
{
	return OwnerHandle;
}

void APMTCAbilitySystemProxy::InitializeProxy(AController* InController)
{
	AbilitySystemComponent = NewObject<UAbilitySystemComponent>(this, AbilitySystemComponentClass);
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Minimal);
	AbilitySystemComponent->SetIsReplicated(false);
	AbilitySystemComponent->RegisterComponent();

	MainAttributeSet = NewObject<UAttributeSet>(this, MainAttributeSetClass);

	AbilitySystemComponent->AddAttributeSetSubobject(MainAttributeSet);
	AbilitySystemComponent->InitAbilityActorInfo(this, this);
}
