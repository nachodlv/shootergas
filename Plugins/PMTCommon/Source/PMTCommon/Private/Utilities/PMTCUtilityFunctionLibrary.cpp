// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Utilities/PMTCUtilityFunctionLibrary.h"

TArray<FHitResult> UPMTCUtilityFunctionLibrary::RemoveDuplicateComponentHits(const TArray<FHitResult>& InHits)
{
	TSet<AActor*> HitActors;
	TArray<FHitResult> FilteredHitResult;
	for (const FHitResult& SingleHit : InHits)
	{
		AActor* ActorHit = SingleHit.GetActor();
		if (ActorHit && !HitActors.Contains(ActorHit))
		{
			HitActors.Add(ActorHit);
			FilteredHitResult.Add(SingleHit);
		}
	}
	return FilteredHitResult;
}

TArray<FHitResult> UPMTCUtilityFunctionLibrary::RemoveDuplicateFromExistentHits(const TArray<FHitResult>& InHits,
	const TArray<FHitResult>& ExistentHits)
{
	TArray<FHitResult> FilteredHitResult;
	for (const FHitResult& SingleHit : InHits)
	{
		bool bFound = false;
		for (const FHitResult& Hit : ExistentHits)
		{
			if (SingleHit.GetActor() == Hit.GetActor())
			{
				bFound = true;
				break;
			}
		}
		if (!bFound)
		{
			FilteredHitResult.Add(SingleHit);
		}
	}
	return FilteredHitResult;
}

TArray<FHitResult> UPMTCUtilityFunctionLibrary::FilterHitsByInterface(const TArray<FHitResult>& InHits, TSubclassOf<UInterface> Interface)
{
	TArray<FHitResult> FilteredHitResult;
	for (const FHitResult& SingleHit : InHits)
	{
		AActor* ActorHit = SingleHit.GetActor();
		if (ActorHit && (ActorHit->GetClass()->ImplementsInterface(Interface)))
		{
			FilteredHitResult.Add(SingleHit);
		}
	}
	return FilteredHitResult;
}
