// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Utilities/PMTCCommonTypes.h"

void FPMTCTagAttribute::RecalculateValue()
{
	// Make sure to apply additives first, then multiplicatives, then dividers.
	Modifiers.Sort([](const FPMTCSimpleAttributeModifier& Lhs, const FPMTCSimpleAttributeModifier& Rhs)
	{
		const int32 LhsOperation = static_cast<int32>(Lhs.ModifierClass.GetDefaultObject()->GetOperationType());
		const int32 RhsOperation = static_cast<int32>(Lhs.ModifierClass.GetDefaultObject()->GetOperationType());
		return LhsOperation < RhsOperation;
	});
	CalculatedValue = BaseValue;
	for (const FPMTCSimpleAttributeModifier& SimpleModifier : Modifiers)
	{
		for (uint8 CurrentStack = 0; CurrentStack < SimpleModifier.CurrentStacks; ++CurrentStack)
		{
			CalculatedValue = SimpleModifier.ModifierClass.GetDefaultObject()->ModifyValue(CalculatedValue, SimpleModifier.Value);
		}
	}
}

bool FPMTCTagAttribute::SetBaseValueAndRecalculate(float InBaseValue)
{
	if (BaseValue != InBaseValue)
	{
		BaseValue = InBaseValue;
		RecalculateValue();
		return true;
	}
	return false;
}

bool FPMTCTagAttribute::SetCurrentValue(float InCurrentValue, float MinClamp)
{
	const float OldCurrentValue = CurrentValue;
	CurrentValue = FMath::Clamp(InCurrentValue, MinClamp, CalculatedValue);
	return CurrentValue != OldCurrentValue;
}

void FPMTCTagAttribute::PostReplicatedAdd(const FPMTCTagAttributeArray& InArraySerializer)
{
	RecalculateValue();
	if (InArraySerializer.Owner)
	{
		InArraySerializer.Owner->NotifyTagAttributeChanged(*this);
	}
}

void FPMTCTagAttribute::PostReplicatedChange(const FPMTCTagAttributeArray& InArraySerializer)
{
	RecalculateValue();
	if (InArraySerializer.Owner)
	{
		InArraySerializer.Owner->NotifyTagAttributeChanged(*this);
	}
}

void FPMTCTagAttributeArray::AddTagAttribute(const FPMTCTagAttribute& InTagAttribute, bool bReplaceOld)
{
	bool bFound = false;
	for (FPMTCTagAttribute& Attribute : Attributes)
	{
		if (Attribute.Tag == InTagAttribute.Tag)
		{
			bFound = true;
			if (bReplaceOld)
			{
				Attribute = InTagAttribute;
				MarkItemDirty(Attribute);
				Attribute.PostReplicatedChange(*this);
			}
			break;
		}
	}
	if (!bFound)
	{
		FPMTCTagAttribute& ItemRef = Attributes.Add_GetRef(InTagAttribute);
		MarkItemDirty(ItemRef);
		ItemRef.PostReplicatedAdd(*this);
	}
}

float FPMTCTagAttributeArray::GetAttributeCurrentValue(FGameplayTag AttributeTag, bool* bExists) const
{
	for (const FPMTCTagAttribute& Attribute : Attributes)
	{
		if (Attribute.Tag == AttributeTag)
		{
			if (bExists)
			{
				*bExists = true;
			}
			return Attribute.GetAttributeValue();
		}
	}
	if (bExists)
	{
		*bExists = false;
	}
	return TNumericLimits<float>::Min();
}

float UPMTCTagAttributeModifier::ModifyValue(float InValue, float InModifierValue) const
{
	if (Operation == EPMTCOperationType::Additive)
	{
		return InValue + InModifierValue;
	}
	else if (Operation == EPMTCOperationType::Multiplicative)
	{
		return InValue * InModifierValue;
	}
	else if (Operation == EPMTCOperationType::Divide && InModifierValue != 0.0f)
	{
		return InValue / InModifierValue;
	}
	return InValue;
}

bool FPMTCCustomDataHandle::NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
{
	TCheckedObjPtr<UScriptStruct> ScriptStruct = IsValid() ? CustomData->GetScriptStruct() : nullptr;
	Ar << ScriptStruct;
	if (ScriptStruct.IsValid())
	{
		if (Ar.IsLoading())
		{
			check(!CustomData.IsValid());

			FPMTCCustomData* NewData = (FPMTCCustomData*)FMemory::Malloc(ScriptStruct->GetStructureSize());
			ScriptStruct->InitializeStruct(NewData);

			CustomData = TSharedPtr<FPMTCCustomData>(NewData, FPMTCCustomDataDeleter());
		}

		if (ScriptStruct->StructFlags & STRUCT_NetSerializeNative)
		{
			ScriptStruct->GetCppStructOps()->NetSerialize(Ar, Map, bOutSuccess, CustomData.Get());
		}
		else
		{
			// This shouldn't be called. If we got this far, it probably means
			// we don't have the traits defined for the structure.
			// See FPMTCCustomData example.
			check(false);
		}
	}
	else if (ScriptStruct.IsError())
	{
		UE_LOG(LogTemp, Error, TEXT("FPMTCCustomDataHandle::NetSerialize: Bad ScriptStruct serialized, can't recover."));
		Ar.SetError();
	}

	if (Ar.IsError())
	{
		// Something bad happened, make sure to not have invalid pointers.
		CustomData.Reset();
		bOutSuccess = false;
		return false;
	}

	bOutSuccess = true;
	return true;
}
