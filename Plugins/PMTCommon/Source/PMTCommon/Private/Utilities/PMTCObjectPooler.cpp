﻿#include "Utilities/PMTCObjectPooler.h"

UPMTCObjectPooler::UPMTCObjectPooler()
{
}

void UPMTCObjectPooler::InstantiateInitialObjects(int32 Quantity, PooleableBuilder NewBuilder)
{
	Builder = NewBuilder;
	DeActivatedObjects.Reserve(Quantity);
	for (int32 i = 0; i < Quantity; ++i)
	{
		Grow();
	}
}

void UPMTCObjectPooler::Grow()
{
	const TScriptInterface<IPMTCPooleable> Pooleable = Builder();
	Pooleable->GetOnDeactivateDelegate().AddUObject(this, &UPMTCObjectPooler::ObjectDeactivated);
	DeActivatedObjects.Add(Pooleable);
}

void UPMTCObjectPooler::ObjectDeactivated(const TScriptInterface<IPMTCPooleable> Pooleable)
{
	ActivatedObjects.Remove(Pooleable);
	DeActivatedObjects.Add(Pooleable);
}
