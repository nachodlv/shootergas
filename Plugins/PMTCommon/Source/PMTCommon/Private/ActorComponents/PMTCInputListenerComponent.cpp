// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "ActorComponents/PMTCInputListenerComponent.h"

namespace PMTCInputListenerCompFunctions
{
	namespace
	{
		UEnum* GetBindEnum(const FString& InEnumName) { return FindObject<UEnum>(ANY_PACKAGE, *InEnumName); }
	}
}

UPMTCInputListenerComponent::UPMTCInputListenerComponent()
{
	bBlockInput = false;
	bWantsInitializeComponent = true;
	// Make sure to set the highest priority possible to always listen for any input.
	Priority = TNumericLimits<int32>::Max();

	SetIsReplicatedByDefault(true);
}

bool UPMTCInputListenerComponent::SimulateInputEvent(const FPMTCInputTimestamp& InInput)
{
	if (!bListening)
	{
		return false;
	}
	if (InInput.IsAxisInput())
	{
		OnAxisInputEvent(InInput.Value, InInput.BindingName);
	}
	else
	{
		OnActionInputEvent(InInput.BindingName, InInput.InputEvent);
	}
	return true;
}

void UPMTCInputListenerComponent::PushInputListener(AController* InController, bool bForcePush)
{
	check(InController);
	check(!bListening);
	StartTimePush = GetWorld()->GetTimeSeconds();
	InputWindow.Reset();
	bListening = true;
	StartedListening.Broadcast(this);

	APlayerController* PlayerController = Cast<APlayerController>(InController);

	// If the owner is the pawn controlled by the controller
	// it gets picked up automatically.
	if (PlayerController && (bForcePush || GetOwner() == PlayerController))
	{
		PlayerController->PushInputComponent(this);
		if (IsClientInput())
		{
			// Since this is client only, the server will not have recursive calls.
			ServerStartListeningInput(InController, StartTimePush);
		}
	}
}

void UPMTCInputListenerComponent::PopInputListener(AController* InController)
{
	check(InController);
	check(bListening);
	bListening = false;
	EndTimePop = GetWorld()->GetTimeSeconds();
	FinishedListening.Broadcast(this, InputWindow.GenerateContiguousArray());

	APlayerController* PlayerController = Cast<APlayerController>(InController);

	if (PlayerController)
	{
		PlayerController->PopInputComponent(this);
		if (IsClientInput())
		{
			ServerStopListeningInput(InController, EndTimePop);
		}
	}
}

bool UPMTCInputListenerComponent::IsListening() const
{
	return bListening;
}

bool UPMTCInputListenerComponent::DoesInputWindowEndWithAction(FName InActionName, EPMTCInputActionEventType EventType, float RequiredTimeToBeHeld) const
{
	if (InputWindow.Num() == 0 || InActionName.IsNone())
	{
		return false;
	}
	TArray<FPMTCInputTimestamp> InputTimestamps = InputWindow.GenerateContiguousArray();
	const FPMTCInputTimestamp* PressedInput = nullptr;
	const FPMTCInputTimestamp* ReleasedInput = nullptr;
	for (const FPMTCInputTimestamp& InputTimestamp : InputTimestamps)
	{
		if (InputTimestamp.BindingName == InActionName)
		{
			if (InputTimestamp.InputEvent == EInputEvent::IE_Pressed)
			{
				PressedInput = &InputTimestamp;
			}
			else if (InputTimestamp.InputEvent == EInputEvent::IE_Released)
			{
				ReleasedInput = &InputTimestamp;
			}
		}
	}
	const bool bIgnoreTime = RequiredTimeToBeHeld == 0.0f;
	if (EventType == EPMTCInputActionEventType::Pressed)
	{
		return PressedInput == &InputTimestamps.Last();
	}
	else if (EventType == EPMTCInputActionEventType::Released)
	{
		return ReleasedInput == &InputTimestamps.Last();
	}
	else if (EventType == EPMTCInputActionEventType::Both)
	{
		if (PressedInput == nullptr || ReleasedInput == nullptr)
		{
			return false;
		}
		if (ReleasedInput != &InputTimestamps.Last())
		{
			return false;
		}
		return bIgnoreTime || (ReleasedInput->Time - PressedInput->Time > RequiredTimeToBeHeld);
	}
	// EventType == Any
	return PressedInput == &InputTimestamps.Last() || ReleasedInput == &InputTimestamps.Last();

}

bool UPMTCInputListenerComponent::DoesInputWindowEndWithActionSequence(const FPMTCActionSequence& ActionSequences)
{
	if (InputWindow.Num() == 0 || InputWindow.Num() < ActionSequences.Num())
	{
		return false;
	}
	// An empty action sequence is valid always.
	if (ActionSequences.Num() == 0)
	{
		return true;
	}

	TArray<FPMTCInputTimestamp> InputTimestamps = InputWindow.GenerateContiguousArray();
	int32 InputWindowIndex = InputWindow.Num() - 1;
	for (int32 Index = ActionSequences.Num() - 1; Index > -1; --Index)
	{
		if (InputTimestamps[InputWindowIndex].BindingName != ActionSequences[Index])
		{
			return false;
		}
		--InputWindowIndex;
	}
	return true;
}

#if WITH_EDITOR
bool UPMTCInputListenerComponent::CanEditChange(const FProperty* InProperty) const
{
	if (InProperty && InProperty->GetNameCPP() == GET_MEMBER_NAME_STRING_CHECKED(UInputComponent, bBlockInput))
	{
		return false;
	}
	return Super::CanEditChange(InProperty);
}
#endif // WITH_EDITOR

bool UPMTCInputListenerComponent::ServerStartListeningInput_Validate(AController* InController, float StartTime)
{
	return true;
}

void UPMTCInputListenerComponent::ServerStartListeningInput_Implementation(AController* InController, float StartTime)
{
	check(!IsListening());
	PushInputListener(InController, false);
	StartTimePush = StartTime;
}

bool UPMTCInputListenerComponent::ServerStopListeningInput_Validate(AController* InController, float EndTime)
{
	return true;
}

void UPMTCInputListenerComponent::ServerStopListeningInput_Implementation(AController* InController, float EndTime)
{
	check(IsListening());
	PopInputListener(InController);
	EndTimePop = EndTime;
}

bool UPMTCInputListenerComponent::ServerSendInputEventReliable_Validate(const FPMTCInputTimestamp& InInput)
{
	return true;
}

void UPMTCInputListenerComponent::ServerSendInputEventReliable_Implementation(const FPMTCInputTimestamp& InInput)
{
	check(IsListening());
	if (InInput.Time >= StartTimePush)
	{
		FPMTCInputTimestamp& TimeStampRef = InputWindow.AddElement(InInput);
		InputOccurred.Broadcast(this, TimeStampRef);
	}
	else
	{
		// TODO(Brian): Change log category.
		UE_LOG(LogTemp, Warning, TEXT("'%s': Input received with time '%f' but start time is less than '%f'. Ignoring."), ANSI_TO_TCHAR(__FUNCTION__), InInput.Time, StartTimePush);
	}
}

bool UPMTCInputListenerComponent::ServerSendInputEventUnReliable_Validate(const FPMTCInputTimestamp& InInput)
{
	return true;
}

void UPMTCInputListenerComponent::ServerSendInputEventUnReliable_Implementation(const FPMTCInputTimestamp& InInput)
{
	if (IsListening() && InInput.Time >= StartTimePush)
	{
		FPMTCInputTimestamp& TimeStampRef = InputWindow.AddElement(InInput);
		InputOccurred.Broadcast(this, TimeStampRef);
	}
	else if (!IsListening() && InInput.Time <= EndTimePop)
	{
		// TODO(Brian): Should we do something here?
	}
}

void UPMTCInputListenerComponent::InitializeComponent()
{
	Super::InitializeComponent();

	const UEnum* ActionEnumBinds = PMTCInputListenerCompFunctions::GetBindEnum(ActionInputIdName);
	if (ActionEnumBinds)
	{
		for (int32 Index = 0; Index < ActionEnumBinds->NumEnums(); ++Index)
		{
			const FString FullStr = ActionEnumBinds->GetNameStringByIndex(Index);
			const FName ActionName = FName(*FullStr);

			if (bListenForPressedEvents)
			{
				FInputActionBinding AB(ActionName, IE_Pressed);
				AB.bConsumeInput = 0;
				AB.ActionDelegate.GetDelegateForManualSet().BindUObject(this, &UPMTCInputListenerComponent::OnActionInputEvent, ActionName, EInputEvent::IE_Pressed);
				AddActionBinding(AB);
			}

			if (bListenForReleasedEvents)
			{
				FInputActionBinding AB(ActionName, IE_Released);
				AB.bConsumeInput = 0;
				AB.ActionDelegate.GetDelegateForManualSet().BindUObject(this, &UPMTCInputListenerComponent::OnActionInputEvent, ActionName, EInputEvent::IE_Released);
				AddActionBinding(AB);
			}
		}
	}
	
	const UEnum* AxisEnumBinds = PMTCInputListenerCompFunctions::GetBindEnum(AxisInputIdName);
	if (AxisEnumBinds)
	{
		for (int32 Index = 0; Index < AxisEnumBinds->NumEnums(); ++Index)
		{
			const FString FullStr = AxisEnumBinds->GetNameStringByIndex(Index);
			const FName AxisName = FName(*FullStr);

			{
				FInputAxisBinding AB(AxisName);
				AB.bConsumeInput = 0;
				AB.AxisDelegate.GetDelegateForManualSet().BindUObject(this, &UPMTCInputListenerComponent::OnAxisInputEvent, AxisName);
				AxisBindings.Emplace(MoveTemp(AB));
			}
		}
	}

	if (bUnlimitedInputTrack)
	{
		constexpr bool bReserve = false;
		InputWindow.SetMaxElements(TNumericLimits<int32>::Max(), bReserve);
	}
	else
	{
		constexpr bool bReserve = true;
		InputWindow.SetMaxElements(MaxAmountOfInput, bReserve);
	}
	
}

void UPMTCInputListenerComponent::OnActionInputEvent(FName ActionName, EInputEvent InputEvent)
{
	// If we were force pushed or we are in a pawn, then actions events will called, but we dont
	// want to listen for them if we are not interested.
	if (!bListening)
	{
		return;
	}
	const UWorld* World = GetWorld();
	const float CurrentGameSeconds = World->GetTimeSeconds();
	if (bOverwriteOnLimit || !InputWindow.IsFull())
	{
		FPMTCInputTimestamp& TimeStampRef = InputWindow.EmplaceElement(ActionName, CurrentGameSeconds, InputEvent, 0.0f);
		InputOccurred.Broadcast(this, TimeStampRef);
		TryToSendInputToServer(TimeStampRef);
	}
}

void UPMTCInputListenerComponent::OnAxisInputEvent(float AxisValue, FName AxisName)
{
	if (!bListening)
	{
		return;
	}
	if (AxisValue != 0.0f)
	{
		const UWorld* World = GetWorld();
		const float CurrentGameSeconds = World->GetTimeSeconds();
		if (bOverwriteOnLimit || !InputWindow.IsFull())
		{
			FPMTCInputTimestamp& TimeStampRef = InputWindow.EmplaceElement(AxisName, CurrentGameSeconds, EInputEvent::IE_Repeat, AxisValue);
			InputOccurred.Broadcast(this, TimeStampRef);
			TryToSendInputToServer(TimeStampRef);
		}
	}
}

bool UPMTCInputListenerComponent::IsClientInput() const
{
	return GetWorld()->GetNetMode() == NM_Client && !GetOwner()->HasAuthority();
}

void UPMTCInputListenerComponent::TryToSendInputToServer(const FPMTCInputTimestamp& InTimestamp)
{
	if (IsClientInput() && InputReplication != EPMTCInputReplication::None)
	{
		if (InputReplication == EPMTCInputReplication::Unreliable)
		{
			// TODO(Brian):
			// If we send unreliable events, there's a chance that this rpc arrives before
			// "starting" to listen for input, therefore we need to cache the input until a start
			// arrives and then we broadcast these keys.
			ServerSendInputEventUnReliable(InTimestamp);
		}
		else if (InputReplication == EPMTCInputReplication::Reliable)
		{
			ServerSendInputEventReliable(InTimestamp);
		}
	}
}
