// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "ActorComponents/PMTCAbilityProxyRepComponent.h"

// UE Includes
#include "Net/UnrealNetwork.h"

UPMTCAbilityProxyRepComponent::UPMTCAbilityProxyRepComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);
}

void UPMTCAbilityProxyRepComponent::AddAbilitiesGivenByProxy(FPMTCSimpleHandle ProxyHandle, const TArray<FGameplayAbilitySpecHandle>& AbilitiesGiven)
{
	if (!GetOwner()->HasAuthority())
	{
		return;
	}
	for (FPMTCProxyAbilityOwnershipItem& ProxyAbility : ProxyAbilityArray.AbilitiesByProxy)
	{
		if (ProxyAbility.Handle == ProxyHandle)
		{
			ProxyAbility.AbilityHandles = AbilitiesGiven;
			ProxyAbilityArray.MarkItemDirty(ProxyAbility);
			return;
		}
	}
	FPMTCProxyAbilityOwnershipItem& Item = ProxyAbilityArray.AbilitiesByProxy.Add_GetRef(FPMTCProxyAbilityOwnershipItem(ProxyHandle, AbilitiesGiven));
	ProxyAbilityArray.MarkItemDirty(Item);
}

void UPMTCAbilityProxyRepComponent::RemoveProxy(FPMTCSimpleHandle ProxyHandle)
{
	if (!GetOwner()->HasAuthority())
	{
		return;
	}
	int32 Index = 0;
	for (FPMTCProxyAbilityOwnershipItem& ProxyAbility : ProxyAbilityArray.AbilitiesByProxy)
	{
		if (ProxyAbility.Handle == ProxyHandle)
		{
			ProxyAbilityArray.AbilitiesByProxy.RemoveAt(Index);
			ProxyAbilityArray.MarkArrayDirty();
			return;
		}
		++Index;
	}
}

FPMTCSimpleHandle UPMTCAbilityProxyRepComponent::GetProxyHandleThatGaveAbility(FGameplayAbilitySpecHandle AbilityHandle) const
{
	for (const FPMTCProxyAbilityOwnershipItem& ProxyAbility : ProxyAbilityArray.AbilitiesByProxy)
	{
		for (FGameplayAbilitySpecHandle AbilitySpec : ProxyAbility.AbilityHandles)
		{
			if (AbilitySpec == AbilityHandle)
			{
				return ProxyAbility.Handle;
			}
		}
	}
	return FPMTCSimpleHandle();
}

void UPMTCAbilityProxyRepComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UPMTCAbilityProxyRepComponent, ProxyAbilityArray, COND_OwnerOnly);
}
