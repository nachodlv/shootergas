// Copyright Epic Games, Inc. All Rights Reserved.

#include "PMTCommon.h"

// PMTC Includes
#include "Globals/PMTCGlobals.h"

#define LOCTEXT_NAMESPACE "FPMTCommonModule"

class FPMTCommonModule : public IPMTCommonModule
{
public:

	/** IModuleInterface implementation */
	void StartupModule() override
	{
		CommonGlobals = nullptr;
	}

	void ShutdownModule() override
	{
		if (CommonGlobals && CommonGlobals->IsRooted())
		{
			CommonGlobals->RemoveFromRoot();
		}
		CommonGlobals = nullptr;
	}

	UPMTCGlobals* GetCommonGlobals() override
	{
		QUICK_SCOPE_CYCLE_COUNTER(STAT_IPMTFiregunWeaponsModule_GetFiregunWeaponsGlobals);
		// Defer loading of globals to the first time it is requested
		if (!CommonGlobals)
		{
			FSoftClassPath CommonClassName = (UPMTCGlobals::StaticClass()->GetDefaultObject<UPMTCGlobals>())->PMTCommonClassName;

			UClass* SingletonClass = CommonClassName.TryLoadClass<UObject>();
			checkf(SingletonClass != nullptr, TEXT("Common config value PMTCommonClassName is not a valid class name."));

			CommonGlobals = NewObject<UPMTCGlobals>(GetTransientPackage(), SingletonClass, NAME_None);
			CommonGlobals->AddToRoot();
		}
		check(CommonGlobals);
		return CommonGlobals;
	}

	bool IsCommonGlobalsAvailable() override
	{
		return CommonGlobals != nullptr;
	}

	// Module members.
	UPMTCGlobals* CommonGlobals = nullptr;
};

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FPMTCommonModule, PMTCommon)