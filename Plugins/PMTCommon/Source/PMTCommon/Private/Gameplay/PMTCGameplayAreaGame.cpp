// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Gameplay/PMTCGameplayAreaGame.h"

// UE Includes
#include "Net/UnrealNetwork.h"

APMTCGameplayAreaGame::APMTCGameplayAreaGame()
{
	PrimaryActorTick.bCanEverTick = false;

	SetReplicates(true);
}

void APMTCGameplayAreaGame::ChangeGameState(FName InState)
{
	check(HasAuthority());
	if (CurrentState != InState)
	{
		const FName OldState = CurrentState;
		CurrentState = InState;
		OnRep_CurrentGameState(OldState);
	}
}

void APMTCGameplayAreaGame::AddPlayerToGameArea(APlayerState* InPlayer)
{
	check(HasAuthority());
	for (APlayerState* Player : PlayersInGame)
	{
		if (Player == InPlayer)
		{
			return;
		}
	}
	PlayersInGame.Add(InPlayer);
	OnRep_PlayersInGame();
}

void APMTCGameplayAreaGame::RemovePlayerFromGameArea(APlayerState* InPlayer)
{
	check(HasAuthority());
	bool bRemoved = false;
	int32 Index = 0;
	for (APlayerState* Player : PlayersInGame)
	{
		if (Player == InPlayer)
		{
			PlayersInGame.RemoveAtSwap(Index);
			bRemoved = true;
			break;
		}
		++Index;
	}
	if (bRemoved)
	{
		OnRep_PlayersInGame();
	}
}

const TArray<APlayerState*>& APMTCGameplayAreaGame::GetPlayersInGameArea() const
{
	return PlayersInGame;
}

void APMTCGameplayAreaGame::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APMTCGameplayAreaGame, PlayersInGame);
	DOREPLIFETIME(APMTCGameplayAreaGame, CurrentState);
}

void APMTCGameplayAreaGame::ResetGameState()
{

}

void APMTCGameplayAreaGame::HandleChangeOfState(FName PreviousState, FName NewState)
{

}

void APMTCGameplayAreaGame::OnRep_CurrentGameState(FName PreviousState)
{
	HandleChangeOfState(PreviousState, CurrentState);
}

void APMTCGameplayAreaGame::OnRep_PlayersInGame()
{
	if (PlayersInGame.Num() == 0 && bResetIfNoPlayers)
	{
		ResetGameState();
	}
}
