// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Gameplay/PMTCGameplayAreaManager.h"

// PMTC Includes
#include "Gameplay/PMTCGameplayArea.h"

// UE Includes
#include "Engine/AssetManager.h"
#include "Engine/StreamableManager.h"
#include "Net/UnrealNetwork.h"

APMTCGameplayAreaManager* APMTCGameplayAreaManager::GetGameplayAreaManager(UObject* InWorldContext)
{
	// TODO(Brian): Fix me.
	return nullptr;
}

APMTCGameplayArea* APMTCGameplayAreaManager::GetGameplayAreaForActor(AActor* InActor)
{
	APMTCGameplayAreaManager* GameplayAreaManager = GetGameplayAreaManager(InActor);
	if (GameplayAreaManager)
	{
		return GameplayAreaManager->NativeGetGameplayAreaForActor(InActor);
	}
	return nullptr;
}

APMTCGameplayAreaManager::APMTCGameplayAreaManager()
{
	PrimaryActorTick.bCanEverTick = false;
	bAlwaysRelevant = true;

	SetReplicates(true);

	GameplayAreaClass = FSoftClassPath(TEXT("/Script/PMTCommon.PMTCGameplayArea"));
}

APMTCGameplayArea* APMTCGameplayAreaManager::NativeGetGameplayAreaForActor(AActor* InActor) const
{
	// TODO(Brian): Fix me.
	return nullptr;
}

void APMTCGameplayAreaManager::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		auto StremeableDel = FStreamableDelegate::CreateUObject(this, &APMTCGameplayAreaManager::OnGameplayAreaClassLoaded);
		UAssetManager& AssetManager = UAssetManager::Get();
		AssetManager.LoadAssetList(TArray<FSoftObjectPath>({ GameplayAreaClass.ToSoftObjectPath() }), StremeableDel);
	}
}

void APMTCGameplayAreaManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void APMTCGameplayAreaManager::OnGameplayAreaClassLoaded()
{
	CachedGameplayAreaClass = GameplayAreaClass.Get();
}
