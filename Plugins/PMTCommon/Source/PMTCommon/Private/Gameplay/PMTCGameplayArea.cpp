// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Gameplay/PMTCGameplayArea.h"

// PMTC Includes
#include "Gameplay/PMTCGameplayAreaGame.h"
#include "Gameplay/PMTCGameplayAreaManager.h"

// UE Includes
#include "Components/BoxComponent.h"
#include "GameFramework/Controller.h"
#include "Net/UnrealNetwork.h"
#include "TimerManager.h"

APMTCGameplayArea::APMTCGameplayArea()
{
	PrimaryActorTick.bCanEverTick = false;
	SetReplicates(true);

	// We don't want to use the standard collider. We are going to use async traces
	// since our location can be huge.
	Area = CreateDefaultSubobject<UBoxComponent>(TEXT("Area"));
	Area->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Area->SetCollisionResponseToAllChannels(ECR_Ignore);
	Area->SetGenerateOverlapEvents(false);

	RootComponent = Area;
}

void APMTCGameplayArea::CreateAreaGame(TSubclassOf<APMTCGameplayAreaGame> AreaGameClass)
{
	check(AreaGameClass);
	check(HasAuthority());

	GameplayAreaGame = GetWorld()->SpawnActor<APMTCGameplayAreaGame>(AreaGameClass);
	OnRep_GameplayAreaGame();
}

void APMTCGameplayArea::ActivateGameplayArea()
{
	if (!HasAuthority())
	{
		return;
	}
	GetWorld()->GetTimerManager().SetTimer(TraceTimerHandle, this, &APMTCGameplayArea::OnTraceTimerFinished, TraceTime, true);
}

void APMTCGameplayArea::DeActivateGameplayArea()
{
	if (!HasAuthority())
	{
		return;
	}
	GetWorld()->GetTimerManager().ClearTimer(TraceTimerHandle);
}

bool APMTCGameplayArea::IsActorInGameplayArea(const AActor* InActor) const
{
	if (!InActor)
	{
		return false;
	}
	const FVector ActorLocation = InActor->GetActorLocation();
	const FVector AreaLocation = GetActorLocation();
	const FVector AreaExtent = Area->GetScaledBoxExtent();
	const FBox Box(AreaLocation - AreaExtent, AreaLocation + AreaExtent);
	return Box.IsInside(ActorLocation);
}

bool APMTCGameplayArea::IsControllerInGameplayArea(const AController* InController) const
{
	return IsActorInGameplayArea(InController ? InController->GetPawn() : nullptr);
}

void APMTCGameplayArea::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// TODO(Brian): Investigate if we can use "COND_InitialOnly".
	// Since the areas are replicated only to the players who are inside,
	// probably initial only won't work. It requires a simple investigation.
	DOREPLIFETIME(APMTCGameplayArea, GameplayAreaGame);
}

void APMTCGameplayArea::BeginPlay()
{
	Super::BeginPlay();

	AsyncTraceDelegate = FOverlapDelegate::CreateUObject(this, &APMTCGameplayArea::OnAsyncTrace);
}

void APMTCGameplayArea::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	DeActivateGameplayArea();
	Super::EndPlay(EndPlayReason);
}

void APMTCGameplayArea::OnRep_GameplayAreaGame()
{

}

void APMTCGameplayArea::HandleAsyncTrace(const TArray<FOverlapResult>& OverlapResults)
{
	// Make a copy so we detect which actors exited.
	TSet<AActor*> CopyActors = ActorsInArea;
	ActorsInArea.Reset();
	// First add the new actors that entered this area.
	for (const FOverlapResult& OverlapResult : OverlapResults)
	{
		AActor* HitActor = OverlapResult.GetActor();
		if (HitActor && !CopyActors.Contains(HitActor))
		{
			OnActorEnteredArea.Broadcast(this, HitActor);
			ActorsInArea.Add(HitActor);
		}
	}

	// Then check who where the ones that exited.
	for (AActor* OldActor : CopyActors)
	{
		if (OldActor && !ActorsInArea.Contains(OldActor))
		{
			OnActorExitedArea.Broadcast(this, OldActor);
		}
	}
}

void APMTCGameplayArea::OnTraceTimerFinished()
{
	const FVector StartPosition = GetActorLocation();
	const FVector BoxExtent = Area->GetScaledBoxExtent();
	const FRotator ActualRotation = GetActorRotation();
	// TODO(Brian): Pass proper parameters for the queries.
	FCollisionObjectQueryParams ObjectQueryParams;
	FCollisionQueryParams QueryParams;
	AsyncTraceHandle = GetWorld()->AsyncOverlapByObjectType(StartPosition, ActualRotation.Quaternion(), ObjectQueryParams, FCollisionShape::MakeBox(BoxExtent), QueryParams, &AsyncTraceDelegate);
}

void APMTCGameplayArea::OnAsyncTrace(const FTraceHandle& InTraceHandle, FOverlapDatum& OverlapData)
{
	HandleAsyncTrace(OverlapData.OutOverlaps);
}
