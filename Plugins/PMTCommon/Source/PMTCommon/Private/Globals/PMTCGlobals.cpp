// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Globals/PMTCGlobals.h"

// PMTC Includes
#include "FunctionLibraries/PMTCDebugFunctionLibrary.h"
#include "PMTCommon.h"

UPMTCGlobals::UPMTCGlobals()
{
	PMTCommonClassName = FSoftClassPath(TEXT("/Script/PMTCommon.PMTCGlobals"));
}

UPMTCGlobals& UPMTCGlobals::Get()
{
	return *IPMTCommonModule::Get().GetCommonGlobals();
}

UPMTCGlobals* UPMTCGlobals::GetPMTCommonGlobals()
{
	return IPMTCommonModule::Get().GetCommonGlobals();
}

void UPMTCGlobals::ChangeDebugTime(UObject* WorldContext, float InTime)
{
	UPMTCGlobals& Globals = UPMTCGlobals::Get();
	if (!WorldContext || !WorldContext->GetWorld() || !Globals.GetWorld() || !Globals.DebugTimerHandle.IsValid())
	{
		return;
	}
	UWorld* ObjectWorld = WorldContext->GetWorld();
	ObjectWorld->GetTimerManager().SetTimer(Globals.DebugTimerHandle, &Globals, &UPMTCGlobals::OnDebugTimerFinished, InTime, true);
}

void UPMTCGlobals::RegisterOrUnregisterObjectForDebugging(UObject* InObject, bool bRegister)
{
	UPMTCGlobals& Globals = UPMTCGlobals::Get();
	if (bRegister)
	{
		bool bValidWorld = false;
		const bool bHasAnyObject = Globals.HasAnyObjectToDebug(InObject, bValidWorld);
		if (bValidWorld)
		{
			if (!bHasAnyObject)
			{
				UWorld* World = InObject->GetWorld();
				Globals.CachedWorld = World;
				World->GetTimerManager().SetTimer(Globals.DebugTimerHandle, &Globals, &UPMTCGlobals::OnDebugTimerFinished, 0.015f, true);
			}
			if (AActor* ObjectAsActor = Cast<AActor>(InObject))
			{
				bool bFound = false;
				for (FPMTCActorDebugging& SingleActor : Globals.ActorsToDebug)
				{
					if (SingleActor.ActorToDebug == ObjectAsActor)
					{
						bFound = true;
						SingleActor.bDebugActor = true;
						break;
					}
				}
				if (!bFound)
				{
					FPMTCActorDebugging ActorDebugging;
					ActorDebugging.bDebugActor = true;
					ActorDebugging.ActorToDebug = ObjectAsActor;
					Globals.ActorsToDebug.Emplace(ActorDebugging);
				}
			}
			else if (UActorComponent* ObjectAsComponent = Cast<UActorComponent>(InObject))
			{
				bool bFound = false;
				for (FPMTCActorDebugging& SingleActor : Globals.ActorsToDebug)
				{
					if (SingleActor.ActorToDebug == ObjectAsActor)
					{
						SingleActor.ComponentsToDebug.Add(ObjectAsComponent);
						bFound = true;
						break;
					}
				}
				if (!bFound)
				{
					FPMTCActorDebugging ActorDebugging;
					ActorDebugging.bDebugActor = false;
					ActorDebugging.ActorToDebug = ObjectAsComponent->GetOwner();
					ActorDebugging.ComponentsToDebug.Add(ObjectAsComponent);
					Globals.ActorsToDebug.Emplace(ActorDebugging);
				}
			}
			else
			{
				Globals.DebuggingObjects.Add(InObject);
			}
		}
	}
	else
	{
		if (AActor* ObjectAsActor = Cast<AActor>(InObject))
		{
			for (int32 Index = 0; Index < Globals.ActorsToDebug.Num(); ++Index)
			{
				FPMTCActorDebugging& SingleActor = Globals.ActorsToDebug[Index];
				if (SingleActor.ActorToDebug == ObjectAsActor)
				{
					if (SingleActor.ComponentsToDebug.Num() > 0)
					{
						SingleActor.bDebugActor = false;
					}
					else
					{
						Globals.ActorsToDebug.RemoveAtSwap(Index);
					}
					break;
				}
			}
		}
		else if (UActorComponent* ObjectAsComponent = Cast<UActorComponent>(InObject))
		{
			AActor* ObjectOwner = ObjectAsComponent->GetOwner();
			for (int32 Index = 0; Index < Globals.ActorsToDebug.Num(); ++Index)
			{
				FPMTCActorDebugging& SingleActor = Globals.ActorsToDebug[Index];
				if (SingleActor.ActorToDebug == ObjectOwner)
				{
					SingleActor.ComponentsToDebug.Remove(ObjectAsComponent);
					if (SingleActor.ComponentsToDebug.Num() == 0 && !SingleActor.bDebugActor)
					{
						Globals.ActorsToDebug.RemoveAtSwap(Index);
					}
					break;
				}
			}
		}
		else
		{
			Globals.DebuggingObjects.Remove(InObject);
		}
		bool bValidWorld = false;
		const bool bHasAnyObject = Globals.HasAnyObjectToDebug(InObject, bValidWorld);
		if (bValidWorld && !Globals.DebugTimerHandle.IsValid() && !bHasAnyObject)
		{
			InObject->GetWorld()->GetTimerManager().ClearTimer(Globals.DebugTimerHandle);
			Globals.DebugTimerHandle.Invalidate();
			Globals.CachedWorld = nullptr;
		}
	}
}

bool UPMTCGlobals::HasAnyObjectToDebug(UObject* InWorldContext, bool& bValidWorld) const
{
	UWorld* World = InWorldContext->GetWorld();
	if (!World)
	{
		bValidWorld = false;
		return false;
	}
	bValidWorld = true;
	return DebuggingObjects.Num() != 0 || ActorsToDebug.Num() != 0;
}

void UPMTCGlobals::OnDebugTimerFinished()
{
	if (!CachedWorld)
	{
		return;
	}
	const float TimerTimeRate = CachedWorld->GetTimerManager().GetTimerRate(DebugTimerHandle);
	for (UObject* ObjectToDebug : DebuggingObjects)
	{
		UPMTCDebugFunctionLibrary::DebugStep(ObjectToDebug, TimerTimeRate, FVector::ZeroVector);
	}
	for (FPMTCActorDebugging& SingleActor : ActorsToDebug)
	{
		FVector TextLocation = FVector::ZeroVector;
		if (SingleActor.bDebugActor)
		{
			TextLocation = UPMTCDebugFunctionLibrary::DebugStep(SingleActor.ActorToDebug, TimerTimeRate, TextLocation);
		}
		for (UActorComponent* ActorComponent : SingleActor.ComponentsToDebug)
		{
			TextLocation = UPMTCDebugFunctionLibrary::DebugStep(ActorComponent, TimerTimeRate, TextLocation);
		}
	}
}
