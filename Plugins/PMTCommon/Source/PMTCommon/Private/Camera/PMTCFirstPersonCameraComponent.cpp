// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Camera/PMTCFirstPersonCameraComponent.h"

UPMTCFirstPersonCameraComponent::UPMTCFirstPersonCameraComponent()
{
	MeshSocket = TEXT("Head");
	bUsePawnControlRotation = true;
}
