// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "PMTCSimpleHandleType.generated.h"

/** Lightweight structure that uniquely identifies an object. */
USTRUCT(BlueprintType)
struct FPMTCSimpleHandle
{
	GENERATED_BODY()

public:

	FPMTCSimpleHandle() {}
	FPMTCSimpleHandle(int32 InHandle) : Handle(InHandle) {}
	FPMTCSimpleHandle(const FPMTCSimpleHandle& InHandle) :
		Handle(InHandle.Handle) {}

	bool IsValid() const { return Handle != INDEX_NONE; }

	void Reset() { Handle = INDEX_NONE; }

	UPROPERTY(Transient)
	int32 Handle = INDEX_NONE;

	friend bool operator==(FPMTCSimpleHandle Lhs, FPMTCSimpleHandle Rhs)
	{
		return Lhs.Handle == Rhs.Handle;
	}

	friend bool operator!=(FPMTCSimpleHandle Lhs, FPMTCSimpleHandle Rhs)
	{
		return !operator==(Lhs, Rhs);
	}

	friend FArchive& operator<<(FArchive& Ar, FPMTCSimpleHandle& Handle)
	{
		Ar << Handle.Handle;
		return Ar;
	}

	operator bool() const
	{
		return IsValid();
	}

	static FPMTCSimpleHandle GenerateNewHandle()
	{
		static int32 NewHandle = 0;
		++NewHandle;
		return FPMTCSimpleHandle(NewHandle);
	}

	friend uint32 GetTypeHash(const FPMTCSimpleHandle& SimpleHandle)
	{
		return ::GetTypeHash(SimpleHandle.Handle);
	}
};
