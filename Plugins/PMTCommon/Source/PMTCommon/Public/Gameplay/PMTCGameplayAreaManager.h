// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PMTCGameplayAreaManager.generated.h"

class APMTCGameplayArea;
class APMTCGameplayAreaManager;

/** The gameplay area manager takes care of controlling any gameplay area
* so we can query about them or see if there's an actor that lies in any of the areas. */
UCLASS(Config = PMTCGameplayArea)
class PMTCOMMON_API APMTCGameplayAreaManager : public AActor
{
	GENERATED_BODY()
	
public:	

	/** Get gameplay area manager. */
	UFUNCTION(BlueprintCallable, Category = "Gameplay Area")
	static APMTCGameplayAreaManager* GetGameplayAreaManager(UObject* InWorldContext);
	/** Get the gameplay area that corresponds to the given actor. */
	UFUNCTION(BlueprintCallable, Category = "Gameplay Area")
	static APMTCGameplayArea* GetGameplayAreaForActor(AActor* InActor);

	APMTCGameplayAreaManager();

	/** Retrieve the gameplay area that corresponds to the given actor. */
	APMTCGameplayArea* NativeGetGameplayAreaForActor(AActor* InActor) const;

protected:

	/** Start loading the gameplay area class. */
	void BeginPlay() override;
	/** Set variables to replicate. */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/** Class used to create the gameplay areas. Defaults to APMTCGameplayArea.*/
	UPROPERTY(EditDefaultsOnly, Config)
	TSoftClassPtr<APMTCGameplayArea> GameplayAreaClass;

private:

	/** Callback called when the gameplay area class is loaded. */
	void OnGameplayAreaClassLoaded();

	/** All the gameplay areas that exists in the game. */
	UPROPERTY(Transient)
	TArray<APMTCGameplayArea*> GameplayAreas;
	/** Cache the gameplay area class to be used. */
	UPROPERTY(Transient)
	TSubclassOf<APMTCGameplayArea> CachedGameplayAreaClass = nullptr;

};
