// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PMTCGameplayArea.generated.h"

class APMTCGameplayArea;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPMTCOnActorEnteredArea, APMTCGameplayArea*, Area, AActor*, Actor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPMTCOnActorExitedArea, APMTCGameplayArea*, Area, AActor*, Actor);

class APMTCGameplayAreaGame;
class AController;
class UBoxComponent;

/** A gameplay area is a place where specific rules and actors live.
* Since we have only one "GameMode" and one "GameState", there's no way
* to define custom modes in a single session. An area fulfills that purpose
* and opens the possibility to use a custom replication graph. */
UCLASS(Config = "PMTCGameplayArea")
class PMTCOMMON_API APMTCGameplayArea : public AActor
{
	GENERATED_BODY()
	
public:	

	APMTCGameplayArea();

	/** Create the area game used by this area. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void CreateAreaGame(TSubclassOf<APMTCGameplayAreaGame> AreaGameClass);
	/** Activates the area. This starts doing traces. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void ActivateGameplayArea();
	/** Activates the area. This stops doing traces. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void DeActivateGameplayArea();

	/** Verifies if the given actor is in the gameplay area. */
	UFUNCTION(BlueprintPure)
	bool IsActorInGameplayArea(const AActor* InActor) const;
	/** Verifies if the given controller is in the gameplay area. */
	UFUNCTION(BlueprintPure)
	bool IsControllerInGameplayArea(const AController* InController) const;

	/** Delegate called when an actor enters the gameplay area.
	* Only called on server. */
	UPROPERTY(BlueprintAssignable)
	FPMTCOnActorEnteredArea OnActorEnteredArea;
	/** Delegate called when an actor exits the gameplay area.
	* Only called in server. */
	UPROPERTY(BlueprintAssignable)
	FPMTCOnActorExitedArea OnActorExitedArea;

protected:

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	/** Register itself to the volume manager. */
	void BeginPlay() override;
	/** Unregister itself from the volume manager. */
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	/** Gives a chance to handle the async trace. Calls the enter/exit for any actor that was returned/exited. */
	virtual void HandleAsyncTrace(const TArray<FOverlapResult>& OverlapResults);

	/** Callback called when the gameplay area game is replicated. */
	UFUNCTION()
	virtual void OnRep_GameplayAreaGame();

	/** After how much time do we make a trace to look for actors that are overlapping the area. */
	UPROPERTY(EditDefaultsOnly, Config)
	float TraceTime = 0.15f;
	/** Gameplay area game owned by this area. */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_GameplayAreaGame)
	APMTCGameplayAreaGame* GameplayAreaGame = nullptr;

private:

	/** Callback called when the timer for doing a trace finished. */
	void OnTraceTimerFinished();
	/** Callback called when the async trace finished. */
	void OnAsyncTrace(const FTraceHandle& InTraceHandle, FOverlapDatum& OverlapData);

	/** Component used to get the area covered. This does not uses collisions.
	* Do not enable them, we make async traces. */
	UPROPERTY(VisibleAnywhere)
	UBoxComponent* Area;
	/** Store the actors that are within the area. */
	UPROPERTY(Transient)
	TSet<AActor*> ActorsInArea;
	/** Handle used to do traces. */
	FTimerHandle TraceTimerHandle;
	/** Trace delegate used to make async traces. */
	FOverlapDelegate AsyncTraceDelegate;
	/** Handle used by the async trace. */
	FTraceHandle AsyncTraceHandle;

};
