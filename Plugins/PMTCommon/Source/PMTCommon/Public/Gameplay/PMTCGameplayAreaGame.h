// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PMTCGameplayAreaGame.generated.h"

/** This represents the rules, states and whatever you need to define a "game".
* You can think of this like UE's "GameMode" and "GameState" with the difference that this lives
* in a single APMTCGameplayAreaGame. So you can have multiple and different games in a single session. */
UCLASS()
class PMTCOMMON_API APMTCGameplayAreaGame : public AActor
{
	GENERATED_BODY()
	
public:	

	APMTCGameplayAreaGame();

	/** Changes the state of the area game. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "Game Area")
	void ChangeGameState(FName InState);
	/** Add the given player to the game area. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "Game Area")
	void AddPlayerToGameArea(APlayerState* InPlayer);
	/** Remove the given player from the game area. Optionally, the game's state is reset
	* if there are no players. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "Game Area")
	void RemovePlayerFromGameArea(APlayerState* InPlayer);

	/** Retrive the players that are in the game area. */
	UFUNCTION(BlueprintPure)
	const TArray<APlayerState*>& GetPlayersInGameArea() const;

protected:

	/** Set variables to replicate. */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	/** Reset the game's state. Empty by default but left for override if desired.
	* Called both on client and server, so make sure to differentiate between both. */
	virtual void ResetGameState();
	/** Handles the change of state in both server and client. */
	virtual void HandleChangeOfState(FName PreviousState, FName NewState);
	/** Callback called when we replicate the current state. */
	UFUNCTION()
	virtual void OnRep_CurrentGameState(FName PreviousState);
	/** Callback when the players in the game are updated. */
	UFUNCTION()
	virtual void OnRep_PlayersInGame();

	/** Track the players that are inside this game area. */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_PlayersInGame)
	TArray<APlayerState*> PlayersInGame;
	/** Current state of the game. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_CurrentGameState)
	FName CurrentState;
	/** Whenever we reset the state of the game if there are no players. */
	UPROPERTY(EditAnywhere, Category = "State")
	bool bResetIfNoPlayers = true;


};
