// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PMTCFirstPersonViewInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPMTCFirstPersonViewInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Minimal interface to determine whenever a character implements a first person view or not.
 */
class PMTCOMMON_API IPMTCFirstPersonViewInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	/** Whenever we support first person view. */
	virtual bool DoesSupportFirstPersonView() const { return true; }
	/** Whenever we support true first person view. */
	virtual bool DoesSupportTrueFirstPersonView() const { return true; }
	/** Whenever the first person view is active. */
	virtual bool IsFirstPersonViewActive() const { return false; }
	/** Gives a chance to update the camera before tick is called. */
	virtual void PreUpdateFirstPersonCamera(float DeltaTime) {}
};
