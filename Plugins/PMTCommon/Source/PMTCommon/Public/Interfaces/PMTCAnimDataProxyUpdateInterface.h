// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PMTCAnimDataProxyUpdateInterface.generated.h"

class UPMTCAnimInstanceDataProxy;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPMTCAnimDataProxyUpdateInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interface that can be added to actors to update data for animations.
 * Avoids direct access from an animation instance to a pawn and vice-versa
 */
class PMTCOMMON_API IPMTCAnimDataProxyUpdateInterface
{
	GENERATED_BODY()

public:

	virtual void UpdateAnimProxyData(UPMTCAnimInstanceDataProxy* AnimInstanceDataProxy, float AnimationDeltaTime) {}
};
