// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "PMTCAnimationFunctionLibrary.generated.h"

/**
 * Common functions used for animations.
 */
UCLASS()
class PMTCOMMON_API UPMTCAnimationFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/**
	 * Returns degree of the angle betwee velocity and Rotation forward vector
	 * The range of return will be from [-180, 180], and this can be used to feed blendspace directional value.
	 * (copied from UAnimInstance::CalculateDirection(const FVector& Velocity, const FRotator& BaseRotation) and exposed
	 * for static usage).
	 */
	UFUNCTION(BlueprintCallable, Category = "PMTC|Animations")
	static float CalculateDirectionForBlendspace(const FVector& Velocity, const FRotator& BaseRotation);
	
};
