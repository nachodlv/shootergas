// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "GameplayTagContainer.h"

// PTMC Includes
#include "Utilities/PMTCCommonTypes.h"

#include "PMTCAttributeTagFuncLibrary.generated.h"

/**
 * Helper functions to work with tag attributes.
 */
UCLASS()
class PMTCOMMON_API UPMTCAttributeTagFuncLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/** Set the current value of the given attribute tag within an array. Mark it for replication.
	* @returns true if the value was modified, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "PMTCCommon|TagAttribute")
	static bool SetTagAttributeCurrentValueInArray(UPARAM(ref) FPMTCTagAttributeArray& TagAttributeArray, FGameplayTag AttributeTag, float InValue, float MinClamp = 0.0f);
	/** Set the current value of the given attribute.
	* @returns true if the value was modified, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "PMTCCommon|TagAttribute")
	static bool SetTagAttributeCurrentValue(UPARAM(ref) FPMTCTagAttribute& TagAttribute, float InValue, float MinClamp = 0.0f);
	/** Set the initial value of the given attribute tag within an array. Mark it for replication.
	* @returns true if the value was modified, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "PMTCCommon|TagAttribute")
	static bool SetTagAttributeInitialValueInArray(UPARAM(ref) FPMTCTagAttributeArray& TagAttributeArray, FGameplayTag AttributeTag, float InValue);
	/** Set the initial value of the given attribute.
	* @returns true if the value was modified, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "PMTCCommon|TagAttribute")
	static bool SetTagAttributeInitialValue(UPARAM(ref) FPMTCTagAttribute& TagAttribute, float InValue);
	/** Add modifier to an attribute.
	* if "InValue" is 0.0f, then the value from the modifier will be used instead.
	* @returns true if the modifier was applied, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "PMTCCommon|TagAttribute")
	static bool AddModifierToTagAttribute(UPARAM(ref) FPMTCTagAttribute& TagAttribute,
		TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, float InValue = 0.0f);
	/** Add modifier to an attribute and mark given array as dirty if applies.
	* if "InValue" is 0.0f, then the value from the modifier will be used instead.
	* @returns true if the modifier was applied, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "PMTCCommon|TagAttribute")
	static bool AddModifierToTagAttributeInArray(UPARAM(ref) FPMTCTagAttributeArray& TagAttributeArray,
		FGameplayTag AttributeTag, TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, float InValue = 0.0f);
	/** Remove modifier from the given tag attribute with the given stack amount.
	* If StacksToRemove is -1, all stacks are removed.
	* @returns true if the modifier was removed, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "PMTCCommon|TagAttribute")
	static bool RemoveModifierFromTagAttribute(UPARAM(ref) FPMTCTagAttribute& TagAttribute,
		TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, int32 StacksToRemove = -1);
	/** Remove modifier from the given tag attribute with the given stack amount and mark the array as dirty if applies.
	* If StacksToRemove is -1, all stacks are removed.
	* @returns true if the modifier was removed, false otherwise. */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "PMTCCommon|TagAttribute")
	static bool RemoveModifierFromTagAttributeInArray(UPARAM(ref) FPMTCTagAttributeArray& TagAttributeArray,
		FGameplayTag AttributeTag, TSubclassOf<UPMTCTagAttributeModifier> InModifierClass, int32 StacksToRemove = -1);
};
