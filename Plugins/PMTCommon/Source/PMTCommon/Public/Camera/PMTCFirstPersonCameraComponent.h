// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "PMTCFirstPersonCameraComponent.generated.h"

/**
 * Camera used for first person views. Either true first person or just hands.
 */
UCLASS()
class PMTCOMMON_API UPMTCFirstPersonCameraComponent : public UCameraComponent
{
	GENERATED_BODY()

public:

	UPMTCFirstPersonCameraComponent();

	/** Whenever this camera is supposed to be used for true first person view. */
	UFUNCTION(BlueprintPure)
	virtual bool IsTrueFirstPersonCamera() const { return bIsTrueFirstPerson; }
	/** Get the name of the socket we should attach for the mesh if true first person view is enabled. */
	UFUNCTION(BlueprintPure, Category = "True First Person")
	FName GetCameraSocketName() const { return MeshSocket; }

protected:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "First Person View")
	bool bIsTrueFirstPerson = true;
	/** In case bIsTrueFirstPerson is true, to which bone of our parent we are attached.
	* Defaults to "Head". */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "True First Person View")
	FName MeshSocket;
	
};
