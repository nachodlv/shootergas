// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// UE Includes
#include "GameplayAbilitySpec.h"

// PMTC Includes
#include "Utilities/Types/PMTCSimpleHandleType.h"

#include "PMTCAbilityProxyRepComponent.generated.h"

/** Simple data that denotes the abilities that were granted by a proxy. */
USTRUCT(BlueprintType)
struct FPMTCProxyAbilityOwnershipItem : public FFastArraySerializerItem
{
	GENERATED_BODY()

public:

	FPMTCProxyAbilityOwnershipItem() {}

	FPMTCProxyAbilityOwnershipItem(FPMTCSimpleHandle InHandle, const TArray<FGameplayAbilitySpecHandle>& InAbilityHandles) :
		Handle(InHandle), AbilityHandles(InAbilityHandles) {}

	/** Proxy's identifier. */
	UPROPERTY(Transient)
	FPMTCSimpleHandle Handle;
	/** Abilities granted by the proxy. */
	UPROPERTY(Transient)
	TArray<FGameplayAbilitySpecHandle> AbilityHandles;
};

/** Saves some bandwidth while replicating the abilities given by a proxy. */
USTRUCT(BlueprintType)
struct FPMTFWProxyAbilityArray : public FFastArraySerializer
{
	GENERATED_BODY()

	/** Array of replicated proxy states. */
	UPROPERTY(Transient)
	TArray<FPMTCProxyAbilityOwnershipItem> AbilitiesByProxy;

	bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParms)
	{
		return FFastArraySerializer::FastArrayDeltaSerialize<FPMTCProxyAbilityOwnershipItem, FPMTFWProxyAbilityArray>(AbilitiesByProxy, DeltaParms, *this);
	}
};

/** Specified to allow fast TArray replication */
template<>
struct TStructOpsTypeTraits<FPMTFWProxyAbilityArray> : public TStructOpsTypeTraitsBase2<FPMTFWProxyAbilityArray>
{
	enum
	{
		WithNetDeltaSerializer = true,
	};
};

/** Component that holds replication data for proxies, since they are not replicated. */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMTCOMMON_API UPMTCAbilityProxyRepComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UPMTCAbilityProxyRepComponent();

	/** Link a proxy and the abilities that it gave. */
	UFUNCTION(BlueprintCallable)
	void AddAbilitiesGivenByProxy(FPMTCSimpleHandle ProxyHandle, const TArray<FGameplayAbilitySpecHandle>& AbilitiesGiven);
	/** Remove a proxy entry that is being tracked. */
	UFUNCTION(BlueprintCallable)
	void RemoveProxy(FPMTCSimpleHandle ProxyHandle);
	/** Verifies if the given proxy handle added the given ability. */
	UFUNCTION(BlueprintPure)
	FPMTCSimpleHandle GetProxyHandleThatGaveAbility(FGameplayAbilitySpecHandle AbilityHandle) const;

protected:

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(Transient, Replicated)
	FPMTFWProxyAbilityArray ProxyAbilityArray;
		
};
