// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

// UE Includes
#include "AbilitySystemInterface.h"
#include "GameplayEffectTypes.h"

// PMTC Includes
#include "Utilities/Types/PMTCSimpleHandleType.h"

#include "PMTCAbilitySystemProxy.generated.h"

class AController;
class UAbilitySystemComponent;
class UAttributeSet;

/** Actor that acts as a proxy between the player that has an ASC and the active fireguns.
* The idea of this actor is to keep the functionality of GAS for firegun attributes
* WITHOUT replication. We force NO replication because otherwise the controller will
* have to manage all the ASC and this could come with more overhead. Instead,
* there are a set of functions that will help us in the prediction of GE's to apply to fireguns
* without having to manage many ASCs.
* The advantage of this approach is having the great power of GAS with modifying attributes
* without having to implement the same system again.
* So this DOES NOT support:
* - Abilities
* - Cues
* But this DOES support:
* + GE to modify attributes.
* + React to added tags to the owner of these fireguns to apply specific behavior. */
UCLASS()
class PMTCOMMON_API APMTCAbilitySystemProxy : public AActor, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
public:	

	/** Create a given amount of proxies for the given controller. */
	UFUNCTION(BlueprintCallable)
	static TArray<APMTCAbilitySystemProxy*> CreateProxiesForController(AController* InController,
		TSubclassOf<APMTCAbilitySystemProxy> ProxyClass, int32 InCount = 1);

	APMTCAbilitySystemProxy();

	/** Retrieve ASC. */
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	/** Retrieve main attribute set. */
	UAttributeSet* GetMainAttributeSet() const;

	/** Applies the given gameplay effect if there's a valid ASC. */
	UFUNCTION(BlueprintCallable)
	FActiveGameplayEffectHandle ApplyGameplayEffect(FGameplayEffectSpecHandle SpecHandle);
	/** Remove an active effect by handle. */
	UFUNCTION(BlueprintCallable)
	void RemoveGameplayEffect(FActiveGameplayEffectHandle InEffectHandle, int32 StacksToRemove = -1);
	/** Remove all active effects. */
	UFUNCTION(BlueprintCallable)
	void RemoveAllEffects();

	/** Verifies if this proxy has anything linked with the ASC. */
	UFUNCTION(BlueprintPure)
	bool HasObjectLinked() const;
	/** Verifies if the given handle matches with the one that the proxy has. */
	UFUNCTION(BlueprintPure)
	bool MatchesHandle(FPMTCSimpleHandle InHandle) const;
	/** Retrieves the handle that this proxy is linked to. */
	UFUNCTION(BlueprintPure)
	FPMTCSimpleHandle GetLinkedHandle() const;

protected:

	/** Initialize proxy with the given player controller. */
	virtual void InitializeProxy(AController* InController);

	/** Ability system component class. */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UAbilitySystemComponent> AbilitySystemComponentClass;
	/** Attribute set class. */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UAttributeSet> MainAttributeSetClass;

	/** Handle that is linked to this proxy. */
	FPMTCSimpleHandle OwnerHandle;

private:

	UPROPERTY(Transient, VisibleInstanceOnly)
	UAbilitySystemComponent* AbilitySystemComponent = nullptr;
	UPROPERTY(Transient)
	UAttributeSet* MainAttributeSet = nullptr;

	/** Active gameplay effects in this proxy. */
	TArray<FActiveGameplayEffectHandle> ActiveGameplayEffects;

};
