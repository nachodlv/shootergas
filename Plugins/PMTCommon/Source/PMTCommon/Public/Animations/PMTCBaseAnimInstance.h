// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "PMTCBaseAnimInstance.generated.h"

class IPMTCAnimDataProxyUpdateInterface;
class UPMTCAnimInstanceDataProxy;

/**
 * Minimal anim instance used to update variables using an anim instance data proxy.
 */
UCLASS()
class PMTCOMMON_API UPMTCBaseAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	UPMTCBaseAnimInstance();

	/** Create the anim instance data proxy. */
	void NativeInitializeAnimation() override;
	/** Null out the proxy so GC can collect it. */
	void NativeUninitializeAnimation() override;
	/** Update anim instance data proxy. */
	void NativeUpdateAnimation(float DeltaTime) override;

	UFUNCTION(BlueprintPure)
	UPMTCAnimInstanceDataProxy* GetAnimInstanceDataProxy() const;

protected:

	/** Update anim instance variables using reflection.  */
	virtual void UpdateUsingReflection(float DeltaTime);

	/** Retrive proxy used to update animation data, if any. */
	TScriptInterface<IPMTCAnimDataProxyUpdateInterface> GetProxyUpdateInterface();
	/** Class used for the data proxy. Defaults to UPMTCAnimInstanceDataProxy. */
	UPROPERTY(EditDefaultsOnly, Category = "Data")
	TSoftClassPtr<UPMTCAnimInstanceDataProxy> AnimInstanceDataProxyClass;
	/** Determine whenever we update variables created using reflection.
	* This should be toggled off and use proxy instead, but it's good to debug
	* and to iterate in animations. */
	UPROPERTY(EditDefaultsOnly, Category = "Data")
	bool bUpdateWithReflection = true;
	
private:

	/** Stores all the properties and cache them. */
	void CacheProperties();

	/** Data used to update animations. */
	UPROPERTY(BlueprintReadOnly, Transient, meta = (AllowPrivateAccess = "true"))
	UPMTCAnimInstanceDataProxy* AnimInstanceDataProxy = nullptr;
	/** Cache the pointer to the owner for ProxyUpdateInterface.  */
	UPROPERTY(Transient)
	TScriptInterface<IPMTCAnimDataProxyUpdateInterface> ProxyUpdateInterface = nullptr;

	/** In case bUpdateWithReflection, we cache the properties that are required to update
	* using the anim instance data proxy. */
	TMap<FName, FProperty*> CachedProperties;
};
