// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PMTCAnimInstanceDataProxy.generated.h"

/**
 * Simple proxy between an anim instance and a character to gather data.
 */
UCLASS()
class PMTCOMMON_API UPMTCAnimInstanceDataProxy : public UObject
{
	GENERATED_BODY()

public:

	/** Map a key to a bool value. If already exists, overrides the value.
	* @returns the value set. */
	UFUNCTION(BlueprintCallable)
	bool SetBoolValue(FName InName, bool InValue);
	/** Map a key to a int32 value. If already exists, overrides the value.
	* @returns the value set. */
	UFUNCTION(BlueprintCallable)
	int32 SetIntValue(FName InName, int32 InValue);
	/** Map a key to a float value. If already exists, overrides the value.
	* @returns the value set. */
	UFUNCTION(BlueprintCallable)
	float SetFloatValue(FName InName, float InValue);
	/** Map a key to a vector value. If already exists, overrides the value.
	* @returns the value set. */
	UFUNCTION(BlueprintCallable)
	FVector SetVectorValue(FName InName, FVector InValue);
	/** Map a key to a rotator value. If already exists, overrides the value.
	* @returns the value set. */
	UFUNCTION(BlueprintCallable)
	FRotator SetRotatorValue(FName InName, FRotator InValue);
	/** Map a key to a transform value. If already exists, overrides the value.
	* @returns the value set. */
	UFUNCTION(BlueprintCallable)
	FTransform SetTransformValue(FName InName, FTransform InValue);

	/** Retrieve a bool value with the given name. Ensure triggered if no value is found.
	* @returns stored value, otherwise false. */
	UFUNCTION(BlueprintPure)
	bool GetBoolValue(FName InName);
	/** Retrieve a int32 value with the given name. Ensure triggered if no value is found.
	* @returns stored value, otherwise INDEX_NONE. */
	UFUNCTION(BlueprintPure)
	int32 GetIntValue(FName InName);
	/** Retrieve a float value with the given name. Ensure triggered if no value is found.
	* @returns stored value, otherwise TNumericLimits<float>::Min(). */
	UFUNCTION(BlueprintPure)
	float GetFloatValue(FName InName);
	/** Retrieve a vector value with the given name. Ensure triggered if no value is found.
	* @returns stored value, otherwise ZeroVector */
	UFUNCTION(BlueprintPure)
	FVector GetVectorValue(FName InName);
	/** Retrieve a rotator value with the given name. Ensure triggered if no value is found.
	* @returns stored value, otherwise ZeroRotator. */
	UFUNCTION(BlueprintPure)
	FRotator GetRotatorValue(FName InName);
	/** Retrieve a transform value with the given name. Ensure triggered if no value is found.
	* @returns stored value, otherwise default transform. */
	UFUNCTION(BlueprintPure)
	FTransform GetTransformValue(FName InName);

	/** Verifies if the given key exists. */
	UFUNCTION(BlueprintPure)
	bool DoesValueExists(FName ValueName) const;

private:

	/** Container of bool values mapped to a key. */
	TMap<FName, bool> BoolValues;
	/** Container of int values mapped to a key. */
	TMap<FName, int32> IntValues;
	/** Container of float values mapped to a key. */
	TMap<FName, float> FloatValues;
	/** Container of vector values mapped to a key. */
	TMap<FName, FVector> VectorValues;
	/** Container of rotator values mapped to a key. */
	TMap<FName, FRotator> RotatorValues;
	/** Container of transform values mapped to a key. */
	TMap<FName, FTransform> TransformValues;
	
};
