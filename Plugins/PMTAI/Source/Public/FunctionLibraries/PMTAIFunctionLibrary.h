﻿#pragma once

#include "PMTAIFunctionLibrary.generated.h"

class UBehaviorTreeComponent;
class UAbilitySystemComponent;
class UPMTAIAbilityMessageSender;

/** Function library for anything related to AI */
UCLASS()
class PMTAI_API UPMTAIFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Returns the AbilitySystemComponent from the AIController's Pawn of the BehaviorTree */
	UFUNCTION(BlueprintPure)
	static UAbilitySystemComponent* GetAbilitySystemComponent(UBehaviorTreeComponent* OwnerComp);

	/** Returns the PMTAIAbilityMessageSender component from the BehaviorTree's AIController */
	UFUNCTION(BlueprintPure)
	static UPMTAIAbilityMessageSender* GetAbilityMessageSenderComponent(UBehaviorTreeComponent* OwnerComp);
};
