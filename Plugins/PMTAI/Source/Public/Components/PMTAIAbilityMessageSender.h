﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "AITypes.h"
#include "Components/ActorComponent.h"

#include "PMTAIAbilityMessageSender.generated.h"

/** Handle used by the FPMTAIMessageSenderHelper when someone subscribe to its messages */
USTRUCT(BlueprintType)
struct FPMTAIMessageSenderHandle
{
	GENERATED_BODY()
	FPMTAIMessageSenderHandle() {}
	FPMTAIMessageSenderHandle(int32 InHandle) : Handle(InHandle) {}

	bool IsValid() const { return Handle != INDEX_NONE; }

	UPROPERTY(Transient)
	int32 Handle = INDEX_NONE;

	friend bool operator==(FPMTAIMessageSenderHandle Lhs, FPMTAIMessageSenderHandle Rhs)
	{
		return Lhs.Handle == Rhs.Handle;
	}

	friend bool operator!=(FPMTAIMessageSenderHandle Lhs, FPMTAIMessageSenderHandle Rhs)
	{
		return !operator==(Lhs, Rhs);
	}

	operator bool() const
	{
		return IsValid();
	}

	static FPMTAIMessageSenderHandle GenerateNewHandle()
	{
		static int32 AttachmentHandleNumber = 0;
		++AttachmentHandleNumber;
		return FPMTAIMessageSenderHandle(AttachmentHandleNumber);
	}
};

class UBrainComponent;

/** Sends messages to the BrainComponent if there are least one subscriber */
USTRUCT()
struct FPMTAIMessageSenderHelper
{
	GENERATED_BODY()

	FPMTAIMessageSenderHelper() {}

	FPMTAIMessageSenderHelper(UObject* InSender, UBrainComponent* InBrainComponent, const FName InMessage) :
		Message(InMessage), BrainComponent(InBrainComponent), Sender(InSender)
	{
	}

	FPMTAIMessageSenderHelper& operator=(const FPMTAIMessageSenderHelper& Rhs)
	{
		Message = Rhs.Message;
		Sender = Rhs.Sender;
		BrainComponent = Rhs.BrainComponent;
		return *this;
	}

	/** Sends a message to the BrainComponent if there is at least one element in the array of Handles */
	void SendMessage(const UObject* ObjectToSend);

	/** Subscribes to the sending of messages */
	const FPMTAIMessageSenderHandle& SubscribeToMessages();

	/**
	 * Unsubscribe to the sending of messages.
	 * If there still more subscribers then you will still be receiving messages.
	 */
	bool UnSubscribeToMessages(const FPMTAIMessageSenderHandle& Handle);

	/** Returns an object with the given RequestID. If no object is found with that RequestID it'll return nullptr */
	template<typename T>
	const T* GetObjectByRequestId(const FAIRequestID& RequestID)
	{
		const UObject** Object = ObjectsByRequestId.Find(RequestID);
		if (!Object)
		{
			return nullptr;
		}
		return Cast<T>(*Object);
	}

private:
	/** Current subscribers */
	UPROPERTY(Transient)
	TArray<FPMTAIMessageSenderHandle> Handles;

	/** Message that will be sent to the BrainComponent */
	FName Message;

	/** To whom will be sent the messages */
	UPROPERTY(Transient)
	UBrainComponent* BrainComponent = nullptr;

	/** The sender of the messages */
	UPROPERTY(Transient)
	UObject* Sender = nullptr;

	/** The stored objects to be retrieve by a request id sent in the messages */
	UPROPERTY(Transient)
	TMap<FAIRequestID, const UObject*> ObjectsByRequestId;

	/** Unique id used for sending the messages */
	static int32 RequestID;

};

class UGameplayAbility;
struct FAbilityEndedData;
struct FGameplayTagContainer;

/**
 * Component meant to be used with an AIController as an owner.
 * Sends messages to the brain component of the AIController whenever an ability is activated, has ended or has failed.
 * The task needs to be subscribed to these messages.
 */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class PMTAI_API UPMTAIAbilityMessageSender : public UActorComponent
{
	GENERATED_BODY()

public:
	UPMTAIAbilityMessageSender();

	/** Returns the MessageSender for when an ability is activated */
	FPMTAIMessageSenderHelper& GetAbilityActivatedMessageSender() { return AbilityActivatedMessageSender; }

	/** Returns the MessageSender for when an ability ends */
	FPMTAIMessageSenderHelper& GetAbilityEndedMessageSender() { return AbilityEndedMessageSender; }

	/** Returns the MessageSender for when an ability fails */
	FPMTAIMessageSenderHelper& GetAbilityFailedMessageSender() { return AbilityFailedMessageSender; }

protected:

	/**
	 * Initializes the MessageSenderHelpers and subscribes to the abilities activated, failed and ended from the
	 *  pawn's owner
	 */
	virtual void BeginPlay() override;

	/** Sends a message to the AbilityActivatedMessageSender. This method is called when an ability is activated */
	virtual void AbilityActivated(UGameplayAbility* AbilityActivated);

	/** Sends a message to the AbilityEndedMessageSender. This method is called when an ability has ended */
	virtual void AbilityEnded(const FAbilityEndedData& AbilityEndedData);

	/** Sends a message to the AbilityFailedMessageSender. This method is called when an ability has failed */
	virtual void AbilityFailed(const UGameplayAbility* FailedAbility, const FGameplayTagContainer& FailedReason);

private:

	/** BrainComponent from the owner */
	UPROPERTY()
	UBrainComponent* BrainComponent;

	/** Sends messages to the BrainComponent when the ability is activated */
	FPMTAIMessageSenderHelper AbilityActivatedMessageSender;

	/** Sends messages to the BrainComponent when the ability has ended */
	FPMTAIMessageSenderHelper AbilityEndedMessageSender;

	/** Sends messages to the BrainComponent when the ability has failed */
	FPMTAIMessageSenderHelper AbilityFailedMessageSender;

public:
	/** Message sent when an ability is activated */
	static const FName AIMessage_AbilityActivated;

	/** Message sent when an ability has ended */
	static const FName AIMessage_AbilityEnded;

	/** Message sent when an ability has failed */
	static const FName AIMessage_AbilityFailed;
};
