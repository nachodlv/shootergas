﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

// PMTAI Includes
#include "Utilities/PMTCPooleable.h"

#include "PMTAIBreadcrumbActorListener.generated.h"


class IPMTAICanJump;

/**
 * Listens to an actor jumping, landing and walking off ledge.
 * Once landed it will try to create a Breadcrumb using the BreadcrumbsGenerator
 */
UCLASS()
class PMTAI_API APMTAIBreadcrumbActorListener : public AActor, public IPMTCPooleable
{
	GENERATED_BODY()

public:
	/** Sets the BreadcrumbsGenerator */
	void Initialize(class APMTAIBreadcrumbsGenerator* NewBreadcrumbsGenerator);

	/** Binds to the actor jump, land and walk off ledge */
	void ListenToActor(const TScriptInterface<IPMTAICanJump>& Actor);

	/** Unbinds from the ActorListened jump, land and walk off ledge */
	virtual void BeginDestroy() override;

	// ~ Begin IPooleable

	virtual bool IsActive() const override { return bActive; }

	/** Activates the pooleable. While activated, it cannot be pooled from the ObjectPooler */
	virtual void Activate() override { bActive = true; }

	/** Deactivates the pooleable. While deactivated, it can be pooled from the ObjectPooler */
	virtual void DeActivate() override;

	/** Returns the delegate that should be broadcast when the pooleable is deactivated */
	virtual FOnDeActivate& GetOnDeactivateDelegate() override { return OnDeActivate; }

	// ~ End IPooleable

private:
	/** Method called when the actor jumps. Sets the vector From */
	UFUNCTION()
	void ActorJumped(const FVector& Location);

	/** Method called when the actor walks off ledge. Sets the vector From */
	UFUNCTION()
    void ActorWalkedOffLedge(const FVector& PreviousPosition);

	/**
	 * Method called when the actor lands.
	 * Places a breadcrumb with the vector From and the position where it landed
	 */
	UFUNCTION()
	void ActorLanded(const FVector& Location);

	/** Used to set the breadcrumbs when the ActorListened lands */
	UPROPERTY(Transient)
	APMTAIBreadcrumbsGenerator* BreadcrumbsGenerator = nullptr;

	/** Actor being listened */
	UPROPERTY(Transient)
	TScriptInterface<IPMTAICanJump> ActorListened = nullptr;

	/** Position from where the actor jumped or walked off ledge */
	FVector From;

	/** Delegates */
	FDelegateHandle JumpHandle;
	FDelegateHandle WalkOffLedgeHandle;
	FDelegateHandle LandHandle;

	/** Determines if the pooleable is active */
	bool bActive = false;

	/** Delegate that will be called when the pooleable is deactivated */
	FOnDeActivate OnDeActivate;
};

