﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

#include "Interfaces/PMTAINavLinkProxyProvider.h"

#include "Navigation/NavLinkProxy.h"
#include "Utilities/PMTCPooleable.h"

#include "PMTNavLinkProxy.generated.h"

/** Link that launches the character from the starting position to the end position */
UCLASS()
class PMTAI_API APMTNavLinkProxy : public ANavLinkProxy, public IPMTAINavLinkProxyProvider
{
	GENERATED_BODY()

public:
	APMTNavLinkProxy();

protected:
	/** Binds to the OnSmartLinkReached from the parent class */
	virtual void BeginPlay() override;

	/** Launches the agent to the DestPoint. The agent should extend from ACharacter */
	UFUNCTION()
	void NotifySmartLinkReached(AActor* PathingAgent, const FVector& DestPoint);

	// ~ Begin IPMTAIPooleable

	virtual bool IsActive() const override { return bActive; }

	/** Activates the pooleable. While activated, it cannot be pooled from the ObjectPooler */
	virtual void Activate() override;

	/** Deactivates the pooleable. While deactivated, it can be pooled from the ObjectPooler */
	virtual void DeActivate() override;

	/** Returns the delegate that should be broadcast when the pooleable is deactivated */
	virtual FOnDeActivate& GetOnDeactivateDelegate() override { return OnDeActivate; }

	// ~ End IPMTAIPooleable

	// ~ Begin IPMTAINavLinkProxyProvider

	/** Returns a delegate that will be called when this nav link proxy is reached */
	virtual FNavLinkProxyReached& GetOnNavLinkProxyReached() override { return OnNavLinkProxyReached; }

	/** Returns the Smart Link Component from the parent class */
	virtual UNavLinkCustomComponent* GetSmartLink() override { return GetSmartLinkComp(); }

	// ~ End IPMTAINavLinkProxyProvider

private:
	/** Calculates the velocity that needs to have the actor to reach its destination */
	FVector CalculateLaunchVelocity(const AActor* LaunchedActor, const FVector& Destination) const;

	/** Determines if the pooleable is active */
	bool bActive = false;

	/** Delegate that will be called when the pooleable is deactivated */
	FOnDeActivate OnDeActivate;

	FNavLinkProxyReached OnNavLinkProxyReached;
};
