﻿

#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "UObject/Object.h"

// PMTCommon includes
#include "Utilities/PMTCPooleable.h"

#include "PMTAINavLinkPooler.generated.h"

typedef TFunction<TScriptInterface<IPMTCPooleable>()> PooleableBuilder;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnNavLinkUsed, const struct FNavLinkWrapper&);

class IPMTAINavLinkProxyProvider;

/** NavLinkProxyProvider Wrapper. Contains the NavLinkProxyProvider and the last time the nav link was used. */
USTRUCT()
struct FNavLinkWrapper
{
	GENERATED_BODY()

public:
	FNavLinkWrapper() {};

	FNavLinkWrapper(const TScriptInterface<IPMTAINavLinkProxyProvider> InNavLinkProvider);

	~FNavLinkWrapper();

	/** Returns the last time the nav link was used */
	float GetLastTimeUsed() const { return LastTimeUsed; }

	/** Return the NavLinkProxyProvider that contains this wrapper */
	const TScriptInterface<IPMTAINavLinkProxyProvider>& GetNavLinkProvider() const { return NavLinkProvider; }

	/** Delegate called when the nav link is reached */
	FOnNavLinkUsed OnNavLinkUsed;

private:
	/** Function binded to the OnNavLinkProxyReached of the NavLinkProxyProvider */
	void NavLinkReached(AActor* Actor, const FVector& Destination);

	/** The nav link this wrapper contains */
	UPROPERTY(Transient)
	TScriptInterface<IPMTAINavLinkProxyProvider> NavLinkProvider = nullptr;

	/** Last time the nav link was used */
	float LastTimeUsed = 0.0f;

	/** Handle of the OnNavLinkProxyReached bind */
	FDelegateHandle OnNavLinkReachedDelegate;
};

/**
 * Provides NavLinkProxyProviders. It will only have activated a certain amount of links at the same time. When the
 * limit is reached it will deactivate the least used link.
 */
UCLASS(Blueprintable)
class PMTAI_API UPMTAINavLinkPooler : public UObject
{
	GENERATED_BODY()
public:
	UPMTAINavLinkPooler();

	/** Initializes the pooler */
	void Initialize(UWorld* World);

	/**
	 * Creates a new wrapper and returns a NavLinkProxyProvider activated.
	 * Important: the Initialize method needs to be called first.
	 */
	const TScriptInterface<IPMTAINavLinkProxyProvider>& GetNavLink();

	virtual void BeginDestroy() override;

protected:
	/**
	 * Creates a new wrapper with a pooled nav link. If the max link is reached then it will deactivate the link with
	 * the oldest "LastTimeUsed".
	 */
	FNavLinkWrapper& InstantiateWrapper();

private:
	/** Current wrappers that are active in the world. */
	UPROPERTY(Transient)
	TArray<FNavLinkWrapper> NavLinkWrappers;

	/** Pooler of NavLinkProxyProviders */
	UPROPERTY(Transient)
	class UPMTCObjectPooler* NavLinksPooler = nullptr;

	/** Class that will be used to instantiate the NavLinkProxyProviders */
	UPROPERTY(EditDefaultsOnly, Category = "PMTAI", meta = (AllowPrivateAccess="true", MustImplement="PMTAINavLinkProxyProvider"))
	TSubclassOf<class ANavLinkProxy> LinkProxyClass;

	/** Maximum quantity of links active at the same time */
	UPROPERTY(EditDefaultsOnly, Category = "PMTAI", meta = (AllowPrivateAccess="true"))
	int32 MaxLinks = 15;
};
