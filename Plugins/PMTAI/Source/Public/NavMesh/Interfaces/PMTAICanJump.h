﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "PMTAICanJump.generated.h"

UINTERFACE()
class UPMTAICanJump : public UInterface
{
	GENERATED_BODY()
};

DECLARE_MULTICAST_DELEGATE_OneParam(FICanJumpDelegate, const FVector&);

/** Provides a character and delegates from jumping and walking off ledge */
class PMTAI_API IPMTAICanJump
{
	GENERATED_BODY()

public:
	virtual FICanJumpDelegate& GetOnJumpDelegate() = 0;
	virtual FICanJumpDelegate& GetOnWalkOffLedgeDelegate() = 0;
	virtual FICanJumpDelegate& GetOnLandDelegate() = 0;

	virtual const FNavAgentProperties& GetAgentProperties() { return FNavAgentProperties::DefaultProperties; }
};
