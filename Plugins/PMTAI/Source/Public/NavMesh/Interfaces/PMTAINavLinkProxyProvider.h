﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "Navigation/NavLinkProxy.h"

// PMTCommon Includes
#include "Utilities/PMTCObjectPooler.h"

#include "PMTAINavLinkProxyProvider.generated.h"

UINTERFACE()
class UPMTAINavLinkProxyProvider : public UPMTCPooleable
{
	GENERATED_BODY()
};

DECLARE_MULTICAST_DELEGATE_TwoParams(FNavLinkProxyReached, AActor*, const FVector&);

/** A pooleable which provides a smart link and a delegate to know then the nav link is reached */
class PMTAI_API IPMTAINavLinkProxyProvider : public IPMTCPooleable
{
	GENERATED_BODY()

public:
	/** Returns a delegate that will be called when the nav link is reached */
	virtual FNavLinkProxyReached& GetOnNavLinkProxyReached() = 0;

	/** Returns the smart link component of the nav link proxy */
	virtual UNavLinkCustomComponent* GetSmartLink() = 0;
};
