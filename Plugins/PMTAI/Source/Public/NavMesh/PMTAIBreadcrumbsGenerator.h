﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

// PMTAI Includes
#include "Interfaces/PMTAICanJump.h"

#include "PMTAIBreadcrumbsGenerator.generated.h"

class UNavLinkComponent;

USTRUCT(BlueprintType)
struct FPMTAIActorRegisteredHandle
{
	GENERATED_BODY()

	FPMTAIActorRegisteredHandle() {}
	FPMTAIActorRegisteredHandle(int32 InHandle) : Handle(InHandle) {}
	FPMTAIActorRegisteredHandle(const FPMTAIActorRegisteredHandle& InHandle) : Handle(InHandle.Handle) {}

	bool IsValid() const { return Handle != INDEX_NONE; }

	UPROPERTY(Transient)
	int32 Handle = INDEX_NONE;

	friend bool operator==(FPMTAIActorRegisteredHandle Lhs, FPMTAIActorRegisteredHandle Rhs)
	{
		return Lhs.Handle == Rhs.Handle;
	}

	friend bool operator!=(FPMTAIActorRegisteredHandle Lhs, FPMTAIActorRegisteredHandle Rhs)
	{
		return !operator==(Lhs, Rhs);
	}

	operator bool() const
	{
		return IsValid();
	}

	static FPMTAIActorRegisteredHandle GenerateNewHandle()
	{
		static int32 AttachmentHandleNumber = 0;
		++AttachmentHandleNumber;
		return FPMTAIActorRegisteredHandle(AttachmentHandleNumber);
	}
};

struct FBreadcrumbToBePlaced
{
	const FVector From;
	const FVector To;

	FBreadcrumbToBePlaced(const FVector InFrom, const FVector InTo): From(InFrom), To(InTo) {}
};

class ANavLinkProxy;
class UPMTCObjectPooler;
class UPMTAINavLinkPooler;
class IPMTAINavLinkProxyProvider;

/** Listens to the jump of registered actors to dynamically create custom links simulating this jump. */
UCLASS()
class PMTAI_API APMTAIBreadcrumbsGenerator : public AActor
{
	GENERATED_BODY()

public:
	APMTAIBreadcrumbsGenerator();

	/**
	 * Binds a delegate to the path async result.
	 * Initializes the pooler ActorListenersPooler.
	 */
	virtual void BeginPlay() override;

	/** De activates all the ActorListeners */
	virtual void BeginDestroy() override;

	/**
	 * Registers an Actor that implements the interface IPMTAICanJump.
	 * The BreadcrumbsGenerator will now listen to the jumps and falls from the actor.
	 * Returns the handle id. It is used to unregister the actor.
	 */
	UFUNCTION(BlueprintCallable, Category="PMTAI", meta=(DisplayName="RegisterActor"))
	FPMTAIActorRegisteredHandle RegisterActor(const TScriptInterface<IPMTAICanJump>& Actor);

	/**
	 * Unbinds the handle associated with the ListenerId.
	 * The BreadcrumbsGenerator will stop listening to the jumps and falls from the actor associated with that handle.
	 */
	UFUNCTION(BlueprintCallable, Category="PMTAI", meta=(DisplayName="UnRegisterActor"))
	bool UnRegisterActor(const FPMTAIActorRegisteredHandle& ListenerId);

	/**
	 * Tries placing a link between the vector From and the vector To.
	 * If any of the vector cannot be projected to the nav mesh the link will not be placed.
	 * If the cost between these two points are less than the MinimumCostForNewLink the link will not be placed.
	 * Returns if the placing of the link was successful.
	 */
	bool PlaceBreadcrumb(const FNavAgentProperties& AgentProperties, const FVector& From, const FVector& To);

private:
	/** Creates a NavLink between the vector From and the point To */
	void CreateNavLink(const FVector& From, const FVector& To) const;

	/**
	 * It will be called when the path between two points are queried.
	 * Calls the method CreateNavLink if the result of the query is successful.
	 */
	void PathResult(uint32 QueryId, ENavigationQueryResult::Type Result, FNavPathSharedPtr Path);

	/** The extent that will be used when projecting the points to place a link */
	UPROPERTY(EditDefaultsOnly, Category="PMTAI", meta=(AllowPrivateAccess="true"))
	FVector NavMeshProjectionExtent = FVector(100.f, 100.f, 150.f);

	/** Minimum distance between to points for the link to be placed */
	UPROPERTY(EditDefaultsOnly, Category="PMTAI", meta=(AllowPrivateAccess="true"))
	float MinimumCostForNewLink = 200.f;

	/** Class that will be use to retrieve the Nav Links */
	UPROPERTY(EditDefaultsOnly, Category="PMTAI", meta=(AllowPrivateAccess="true"))
	TSubclassOf<UPMTAINavLinkPooler> NavLinkPoolerClass;

	/** Used to retrieve the Nav Links */
	UPROPERTY()
	UPMTAINavLinkPooler* NavLinkPooler;

	/** Current BreadcrumbActorListeners that are being listening to an specific actor */
	TMap<FPMTAIActorRegisteredHandle, class APMTAIBreadcrumbActorListener*> ActorListeners;

	/** Queries currently executing to find the path between two points */
	TMap<uint32, FBreadcrumbToBePlaced> Queries;

	/** Pool of BreadcrumbActorListener objects */
	UPROPERTY()
	UPMTCObjectPooler* ActorListenersPooler;

	/** Delegate that will be called when a path between two points finish loading */
	FNavPathQueryDelegate FindPathAsync;
};
