﻿#pragma once

// UE Includes
#include "BehaviorTree/BTDecorator.h"
#include "CoreMinimal.h"

#include "BTDecorator_CompareFloats.generated.h"

UENUM(BlueprintType)
enum class EFloatComparisonType : uint8
{
	Equal,
	GreaterThan,
	// There is no need to add "LesserThan" because of the inverse boolean from the BTDecorator class.
};

/** Compares two floats. */
UCLASS()
class PMTAI_API UBTDecorator_CompareFloats : public UBTDecorator
{
	GENERATED_BODY()
public:
	UBTDecorator_CompareFloats();

	/** Compares the left float with the right float */
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

	/** If the right float or the left float are blackboard keys then it will subscribe to any of its changes */
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/**
	 * It will be called when the left float or the right float change their values on the blackboard.
	 * It requests the execution of the CalculateRawConditionValue.
	 */
	EBlackboardNotificationResult OnKeyValueChanged(const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID);

	/** Float used on the left side of the comparison */
	UPROPERTY(EditAnywhere, Category= "PMTAI", meta = (EditCondition = "!bLeftValueAsKey"))
	float LeftFloat = 0.0f;
	UPROPERTY(EditAnywhere, Category= "PMTAI", meta = (EditCondition = "bLeftValueAsKey"))
	FBlackboardKeySelector KeyLeftFloat;
	UPROPERTY(EditDefaultsOnly, Category= "PMTAI", meta = (InlineEditConditionToggle))
	bool bLeftValueAsKey = true;

	/** Float used on the right side of the comparison */
	UPROPERTY(EditAnywhere, Category= "PMTAI", meta=(EditCondition = "!bRightValueAsKey"))
	float RightFloat = 0.0f;
	UPROPERTY(EditAnywhere, Category= "PMTAI", meta = (EditCondition = "bRightValueAsKey"))
	FBlackboardKeySelector KeyRightFloat;
	UPROPERTY(EditDefaultsOnly, meta=(InlineEditConditionToggle))
	bool bRightValueAsKey = false;

	/** How the two floats will be compared */
	UPROPERTY(EditAnywhere, Category= "PMTAI")
	EFloatComparisonType ComparisonType = EFloatComparisonType::Equal;

	/** Precision that will be used when comparing the equality of two floats */
	UPROPERTY(EditAnywhere, Category= "PMTAI")
	float ComparisonPrecision = 0.001f;
};
