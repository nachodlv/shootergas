﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "PMTAIHaveAbilityMessageSender.generated.h"

UINTERFACE()
class UPMTAIHaveAbilityMessageSender : public UInterface
{
	GENERATED_BODY()
};

class UPMTAIAbilityMessageSender;

/** Provides a PMTAIAbilityMessageSender */
class PMTAI_API IPMTAIHaveAbilityMessageSender
{
	GENERATED_BODY()

public:

	/** Returns the PMTAIAbilityMessageSender component from the actor */
	virtual UPMTAIAbilityMessageSender* GetMessageSender() = 0;
};
