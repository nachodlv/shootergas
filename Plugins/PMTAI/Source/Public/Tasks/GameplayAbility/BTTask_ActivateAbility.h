﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"

// PMTAI Includes
#include "Components/PMTAIAbilityMessageSender.h"

#include "BTTask_ActivateAbility.generated.h"

class UGameplayAbility;

/** Memory of the UBTTask_ActivateAbility */
struct FActivateAbilityMemory
{
	FPMTAIMessageSenderHandle EndedAbilityHandle;
	FPMTAIMessageSenderHandle FailedAbilityHandle;
};

/**
 * Activates an ability and stats in progress until the ability fails or ends.
 * If the ability fails then the task will failed
 */
UCLASS()
class PMTAI_API UBTTask_ActivateAbility : public UBTTaskNode
{
	GENERATED_BODY()

public:

	UBTTask_ActivateAbility();

	/**
	 * Tries to activate abilities by the ability tag and keeps the task in progress.
	 * Binds itself for the Ability Ended and the Ability Failed.
	 */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** Tags that will be use to activate the ability */
	UPROPERTY(EditAnywhere, Category="PMTAI")
	FGameplayTagContainer AbilityTags;

protected:
	/**
	 * Is triggered whenever an ability fails or finishes. If it's the same ability that was triggered on ExecuteTask
	 * then the task will end.
	 */
	virtual void OnMessage(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, FName Message, int32 RequestID, bool bSuccess) override;

	virtual uint16 GetInstanceMemorySize() const override;

	/** Cancels de ability and unsubscribe from the abilities delegates */
	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** Returns a string containing description of this node with all setup values */
	virtual FString GetStaticDescription() const override;

private:
	/** Unsubscribe from the MTAIAbilityMessageSender and from the messages */
	void UnSubscribe(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, UPMTAIAbilityMessageSender* MessageSender) const;
};
