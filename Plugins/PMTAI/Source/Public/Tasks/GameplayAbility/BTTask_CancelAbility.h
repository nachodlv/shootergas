﻿

#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_CancelAbility.generated.h"

/** Cancels an ability with a given class or tags */
UCLASS()
class PMTAI_API UBTTask_CancelAbility : public UBTTaskNode
{
	GENERATED_BODY()

public:
	/** The class that will be used to cancel the ability if bCancelAbilityWithTags is false */
	UPROPERTY(EditAnywhere, Category="PMTAI", meta=(EditCondition = "!bCancelAbilityWithTags"))
	TSubclassOf<class UGameplayAbility> AbilityToCancel;

	/** Tags that will be used to cancel abilities if bCancelAbilityWithTags is true */
	UPROPERTY(EditAnywhere, Category="PMTAI", meta=(EditCondition = "bCancelAbilityWithTags"))
	FGameplayTagContainer AbilityToCancelWithTags;

	/** If true tags will be used instead of a class when canceling the ability */
	UPROPERTY(EditDefaultsOnly, Category ="PMTAI", meta=(InlineEditConditionToggle))
	bool bCancelAbilityWithTags;

	/** Gets the AbilitySystemComponent from the AIController's Pawn and cancels an ability */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** @return string containing description of this node with all setup values */
	virtual FString GetStaticDescription() const override;
};
