﻿#pragma once

// UE Includes
#include "BehaviorTree/BTTaskNode.h"
#include "CoreMinimal.h"
#include "GameplayEffectTypes.h"

#include "BTTask_ApplyGameplayEffect.generated.h"

/** Memory of the BTTask_ApplyGameplayEffect */
struct FApplyGameplayEffectMemory
{
	FActiveGameplayEffectHandle GameplayEffectHandle;
};

/**
 * Applies a gameplay effect to the pawn
 */
UCLASS()
class PMTAI_API UBTTask_ApplyGameplayEffect : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_ApplyGameplayEffect();

	/** Applies the gameplay effect. If bFireAndForget is set to false the task will remain in progress till aborted */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** Gameplay effect that will be applied */
	UPROPERTY(EditAnywhere, Category="PMTAI", meta = (EditCondition = "!bUseGameplayEffectKey"))
	TSubclassOf<class UGameplayEffect> GameplayEffect;
	UPROPERTY(EditAnywhere, Category="PMTAI", meta = (EditCondition = "bUseGameplayEffectKey"))
	FBlackboardKeySelector GameplayEffectKey;
	UPROPERTY(EditAnywhere, Category="PMTAI", meta = (InlineEditConditionToggle))
	bool bUseGameplayEffectKey = false;

	/** Should the task finish when applying the gameplay effect or should stay in progress till the abort of the task */
	UPROPERTY(EditAnywhere, Category="PMTAI")
	bool bFireAndForget = true;

	/** Level of the gameplay effect */
	UPROPERTY(EditAnywhere, Category="PMTAI")
	float Level = 1.0f;

protected:
	/** Removes the gameplay effect applied in the execute of the task */
	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual uint16 GetInstanceMemorySize() const override;

	/** Returns a string containing description of this node with all setup values */
	virtual FString GetStaticDescription() const override;

	/**
	 * Returns the gameplay effect from a blackboard key or from the default object of the gameplay effect depending
	 * on the value of bUseGameplayEffectKey.
	 */
	UGameplayEffect* GetGameplayEffect(UBehaviorTreeComponent& OwnerComp) const;
};
