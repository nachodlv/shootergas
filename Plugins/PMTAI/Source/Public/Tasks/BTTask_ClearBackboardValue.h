#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_ClearBackboardValue.generated.h"

/** Clears a blackboard value with the given blackboard key*/
UCLASS()
class PMTAI_API UBTTask_ClearBackboardValue : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
public:
	UBTTask_ClearBackboardValue();

protected:

	/** Starts this task, should return Succeeded, Failed or InProgress */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** @return string containing description of this node with all setup values */
	virtual FString GetStaticDescription() const override;
};
