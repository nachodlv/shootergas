﻿#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"

#include "BTTask_SetFocus.generated.h"

/** Task that sets the focus of a Pawn */
UCLASS()
class PMTAI_API UBTTask_SetFocus : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_SetFocus();

protected:

	/** Pawn that will be change focus. Can only derive from a Pawn class */
	UPROPERTY(EditAnywhere, Category="Blackboard")
	struct FBlackboardKeySelector Pawn;

	/** Focus that the pawn will focus to. Can only derive from an Actor class */
	UPROPERTY(EditAnywhere, Category="Blackboard")
	struct FBlackboardKeySelector ActorToFocus;

	/** If set to true then it will clear the focus of the Pawn */
	UPROPERTY(EditAnywhere, Category="Blackboard")
	bool ClearFocus;

	/** Starts this task, should return Succeeded, Failed or InProgress */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** @return string containing description of this node with all setup values */
	virtual FString GetStaticDescription() const override;
};
