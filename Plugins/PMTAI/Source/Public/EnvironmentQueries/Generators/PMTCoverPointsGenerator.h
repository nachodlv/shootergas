﻿#pragma once

#include "CoreMinimal.h"

#include "DataProviders/AIDataProvider.h"
#include "EnvironmentQuery/EnvQueryGenerator.h"
#include "PMTCoverPointsGenerator.generated.h"

/**
 *	Query that generates all the cover points around the context locations.
 *	TODO (Ignacio) I still needs to think of a way to prioritize bigger covers
 */
UCLASS()
class PMTAI_API UPMTCoverPointsGenerator : public UEnvQueryGenerator
{
	GENERATED_BODY()
public:
	UPMTCoverPointsGenerator();

	/**
	 * Gets all the cover points around the Context locations.
	 * It will start searching with the MaxRadius and this radius will decrease by the DepthPrecision if no locations
	 * are found. If the radius reach the MinRadius then no cover points will be queried.
	 */
	virtual void GenerateItems(FEnvQueryInstance& QueryInstance) const override;
protected:

	/** Distance from the cover point in the opposite direction from the target */
	UPROPERTY(EditDefaultsOnly, Category="QueryGenerator")
	float OffsetFromCover = 50.f;

	/** How many traces will be traced around the target */
	UPROPERTY(EditDefaultsOnly, Category="QueryGenerator")
	int32 QuantityOfTraces = 36;

	/** Distance between each search */
	UPROPERTY(EditDefaultsOnly, Category="QueryGenerator")
	float DepthPrecision = 36;

	/** Minimum radius that the query will check */
	UPROPERTY(EditDefaultsOnly, Category="QueryGenerator")
	float MinRadius = 100.f;

	/** Radius where the query will start checking for cover */
	UPROPERTY(EditDefaultsOnly, Category="QueryGenerator")
	FAIDataProviderFloatValue MaxRadius;

	UPROPERTY(EditDefaultsOnly, Category="QueryGenerator")
	FAIDataProviderFloatValue FullHeightCover;

	/**
	 *	Minimum height of covers.
	 *	If an object is smaller than the MinHeightCover then it'll not be taking into account
	 */
	UPROPERTY(EditDefaultsOnly, Category="QueryGenerator")
	FAIDataProviderFloatValue MinHeightCover;

	/** Context which the query will use as a target */
	UPROPERTY(EditDefaultsOnly, Category="QueryGenerator")
	TSubclassOf<UEnvQueryContext> GenerateAround;

	/**
	 *	Makes lines traces all around the ContextLocation. From the radius border to the ContextLocation.
	 *	If something is hit then it's used as a cover.
	 */
	void SearchCover(TArray<FNavLocation>& Locations, float CurrentRadius, FVector& ContextLocation, float HeightCover, float MinHeight, UWorld* World) const;
};
