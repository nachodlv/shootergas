﻿#pragma once

#include "CoreMinimal.h"

#include "EnvironmentQuery/EnvQueryContext.h"
#include "PMTTargetQueryContext.generated.h"

/** Provides the target of the querier as context. */
UCLASS()
class PMTAI_API UPMTTargetQueryContext : public UEnvQueryContext
{
	GENERATED_BODY()

public:
	UPMTTargetQueryContext(const FObjectInitializer& ObjectInitializer);

	/** Blackboard key where the target should be stored */
	UPROPERTY(EditDefaultsOnly)
	FName TargetBlackboardKey;

	/** Gets the target from the querier by getting its blackboard component */
	virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;
};
