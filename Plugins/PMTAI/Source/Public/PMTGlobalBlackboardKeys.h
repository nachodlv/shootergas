﻿#pragma once

#include "CoreMinimal.h"

#include "UObject/Object.h"
#include "PMTGlobalBlackboardKeys.generated.h"

/** Stores all keys's names from the blackboards */
UCLASS(Config = Game)
class PMTAI_API UPMTGlobalBlackboardKeys : public UObject
{
	GENERATED_BODY()
public:
	UPMTGlobalBlackboardKeys();

	/** Gets the AI's current target key name */
	static FName GetTarget();
};
