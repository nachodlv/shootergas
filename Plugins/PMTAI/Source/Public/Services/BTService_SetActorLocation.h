﻿

#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "BTService_SetActorLocation.generated.h"

/** Sets an actor location in a blackboard key */
UCLASS()
class PMTAI_API UBTService_SetActorLocation : public UBTService
{
	GENERATED_BODY()
public:
	UBTService_SetActorLocation();

protected:

	/** Actor where the location will be taken. The key must be an AActor */
	UPROPERTY(EditAnywhere, Category=Blackboard)
	struct FBlackboardKeySelector Actor;

	/** The blackboard key where the actor location will be saved. The key must be a FVector */
	UPROPERTY(EditAnywhere, Category=Blackboard)
	struct FBlackboardKeySelector Location;

	/** Calls the SetActorLocation function */
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	/** Called when auxiliary node becomes active. Calls the SetActorLocation function */
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** Gets the actor location from the "Actor" key and saves it in the "Location" key */
	void SetActorLocation(UBehaviorTreeComponent& OwnerComp) const;

	/** @return string containing description of this node with all setup values */
	virtual FString GetStaticDescription() const override;
};
