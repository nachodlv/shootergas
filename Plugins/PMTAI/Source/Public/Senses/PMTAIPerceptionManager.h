﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Perception/AIPerceptionTypes.h"

#include "PMTAIPerceptionManager.generated.h"

class AAIController;
class UBlackboardComponent;
class UAIPerceptionComponent;
class UAISense;
class IPMTAIPerceptionToBlackboardKey;

/** Contains a blackboard key name and a UObject that implements PMTAIPerceptionToBlackboardKey. */
USTRUCT()
struct FPMTAIBlackboardKey
{
	GENERATED_BODY()

	/** Casts the PerceptionToBlackboardKey to a IPMTAIPerceptionToBlackboardKey */
	IPMTAIPerceptionToBlackboardKey* GetPerceptionToBlackboardKey() const;

	UPROPERTY(EditAnywhere, Category = "PMTAI")
	FName BlackboardKeyName;

private:
	/** Object that knows how to set an AIPerception into a blackboard with a blackboard key */
	UPROPERTY(EditAnywhere, Category = "PMTAI", meta = (MustImplement = PMTAIPerceptionToBlackboardKey, AllowPrivateAccess = "true"))
	TSubclassOf<UObject> PerceptionToBlackboardKey;
};

/** Links a sense with an array of FPMTAIBlackboardKey */
USTRUCT(BlueprintType)
struct FPMTAISenseWithBlackboardKeys
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "PMTAI")
	TSubclassOf<UAISense> Sense;

	UPROPERTY(EditAnywhere, Category = "PMTAI")
	TArray<FPMTAIBlackboardKey> BlackboardKeys;
};

/**
 * Converts the senses perceived to blackboard keys
 * # TODO
 * - Implement the receive damage sense
 * - Implement the accumulation of damage (no idea where should I do this)
 */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class PMTAI_API UPMTAIPerceptionManager : public UActorComponent
{
	GENERATED_BODY()

public:
	UPMTAIPerceptionManager();

protected:
	virtual void BeginPlay() override;

	/**
	 * Method called when the perception from an actor changes.
	 * Adds the keys to the blackboard from the sense detected.
	 */
	UFUNCTION()
	void ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors);

	/** Method called when a stimulus expired. Removes the blackboard keys from the stimulus sense */
	UFUNCTION()
	void StimulusExpired(FAIStimulus& Stimulus);

	/** Returns true if the blackboard key has not been set by a sense with higher priority */
	bool CanOverrideBlackboardKey(const FName& BlackboardKey, int32 SensePriority);

private:

	/** Removes the sense from the blackboard and the KeysSetWithPriorities */
	void RemoveSenseFromBlackboard(const FName& BlackboardKeyName);

	/** Returns the blackboard component from the AIController */
	UBlackboardComponent* GetBlackboardComponent() const;

	/** Returns the perception component from the AIController */
	UAIPerceptionComponent* GetPerceptionComponent() const;

	/** Blackboard and Perception component owner */
	UPROPERTY()
	AAIController* AIController;

	/** Used to know if the instigator from the sources are the same as the owner */
	UPROPERTY()
	APawn* Pawn;

	/**
	 * The senses with their respective blackboard keys ordered by priority.
	 * The smallest the index the greater the priority.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PMTAI", meta = (AllowPrivateAccess = "true"))
	TArray<FPMTAISenseWithBlackboardKeys> SensesByPriority;

	/** Used to know with which priority the keys in the blackboard were set */
	UPROPERTY()
	TMap<FName, int32> KeysSetWithPriorities;
};
