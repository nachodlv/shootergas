﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

// PMTAI Includes
#include "Senses/Interfaces/PMTAIPerceptionToBlackboardKey.h"

#include "PMTAITargetToBlackboardKey.generated.h"

struct FActorPerceptionInfo;
class UBlackboardComponent;

/** Sets the target from a FActorPerceptionInfo into a blackboard key */
UCLASS()
class PMTAI_API UPMTAITargetToBlackboardKey : public UObject, public IPMTAIPerceptionToBlackboardKey
{
	GENERATED_BODY()
public:
	/** Gets the target from the FActorPerceptionInfo and sets it into the UBlackboardComponent */
	virtual void SetToBlackboard(const FName& BlackboardKey, const FActorPerceptionInfo* PerceptionInfo, UBlackboardComponent* BlackboardComponent) const override;
};
