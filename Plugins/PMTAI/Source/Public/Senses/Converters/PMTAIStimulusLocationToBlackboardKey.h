﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

// PMTAI Includes
#include "Senses/Interfaces/PMTAIPerceptionToBlackboardKey.h"

#include "PMTAIStimulusLocationToBlackboardKey.generated.h"

/** Sets the LastStimulusLocation from a FActorPerceptionInfo into a blackboard key */
UCLASS()
class PMTAI_API UPMTAIStimulusLocationToBlackboardKey : public UObject, public IPMTAIPerceptionToBlackboardKey
{
	GENERATED_BODY()
public:
	/** Gets the LastStimulusLocation from the FActorPerceptionInfo and sets it into the UBlackboardComponent */
	virtual void SetToBlackboard(const FName& BlackboardKey, const FActorPerceptionInfo* PerceptionInfo, UBlackboardComponent* BlackboardComponent) const override;
};
