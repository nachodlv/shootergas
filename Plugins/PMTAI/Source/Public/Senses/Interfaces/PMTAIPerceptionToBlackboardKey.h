﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "PMTAIPerceptionToBlackboardKey.generated.h"

UINTERFACE()
class UPMTAIPerceptionToBlackboardKey : public UInterface
{
	GENERATED_BODY()
};

struct FActorPerceptionInfo;
class UBlackboardComponent;

/** Provides a way to set a blackboard key having a struct FActorPerceptionInfo */
class PMTAI_API IPMTAIPerceptionToBlackboardKey
{
	GENERATED_BODY()

public:
	/** Sets the corresponding variable from the FActorPerceptionInfo into the UBlackboardComponent. */
	virtual void SetToBlackboard(const FName& BlackboardKey, const FActorPerceptionInfo* PerceptionInfo, UBlackboardComponent* BlackboardComponent) const = 0;
};
