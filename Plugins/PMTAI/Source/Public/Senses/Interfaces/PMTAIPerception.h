﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "PMTAIPerception.generated.h"

UINTERFACE()
class UPMTAIPerception : public UInterface
{
	GENERATED_BODY()
};

struct FAIStimulus;
class FPerceptionUpdatedDelegate;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnStimulusExpired, FAIStimulus&);

/** Interface that returns delegates to know when the perception of an actor is updated and when a stimulus has expired */
class PMTAI_API IPMTAIPerception
{
	GENERATED_BODY()

public:
	/** Returns a delegate that will be called when the a stimulus has expired */
	virtual FOnStimulusExpired& GetOnStimulusExpiredDelegate() = 0;

	/** Returns a delegate that will be called when the perception of an actor has changes */
	virtual FPerceptionUpdatedDelegate& GetOnPerceptionUpdatedDelegate() = 0;

};
