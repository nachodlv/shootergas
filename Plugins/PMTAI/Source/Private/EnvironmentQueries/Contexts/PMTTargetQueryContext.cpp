﻿#include "EnvironmentQueries/Contexts/PMTTargetQueryContext.h"

#include "AIController.h"
#include "PMTGlobalBlackboardKeys.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"

UPMTTargetQueryContext::UPMTTargetQueryContext(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	TargetBlackboardKey = UPMTGlobalBlackboardKeys::GetTarget();
}

void UPMTTargetQueryContext::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	APawn* Pawn = Cast<APawn>(QueryInstance.Owner.Get());
	AAIController* Controller = Pawn ? Pawn->GetController<AAIController>() : nullptr;
	UBlackboardComponent* BlackboardComponent = Controller ? Controller->GetBlackboardComponent() : nullptr;

	// Gets the target from the AI
	// TODO (Ignacio) we might want to grab the ViewPoint from the pawn
	AActor* Target = BlackboardComponent ? Cast<AActor>(BlackboardComponent->GetValueAsObject(TargetBlackboardKey)) : nullptr;
	if (Target)
	{
		UEnvQueryItemType_Actor::SetContextHelper(ContextData, Target);
	} else
	{
		UEnvQueryItemType_Actor::SetContextHelper(ContextData, Cast<AActor>(QueryInstance.Owner.Get()));
	}
}
