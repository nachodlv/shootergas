﻿#include "EnvironmentQueries/Generators/PMTCoverPointsGenerator.h"

#include "DrawDebugHelpers.h"
#include "NavigationSystem.h"
#include "EnvironmentQueries/Contexts/PMTTargetQueryContext.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Point.h"

UPMTCoverPointsGenerator::UPMTCoverPointsGenerator()
{
	ItemType = UEnvQueryItemType_Point::StaticClass();
	GenerateAround = UPMTTargetQueryContext::StaticClass();

	MaxRadius.DefaultValue = 1000.f;
	FullHeightCover.DefaultValue = 100.0f;
	MinHeightCover.DefaultValue = 30.0f;
}

void UPMTCoverPointsGenerator::GenerateItems(FEnvQueryInstance& QueryInstance) const
{
	if (QuantityOfTraces <= 0)
	{
		return;
	}

	UObject* Owner = QueryInstance.Owner.Get();

	TArray<AActor*> Targets;
	QueryInstance.PrepareContext(GenerateAround, Targets);

	MaxRadius.BindData(Owner, QueryInstance.QueryID);
	FullHeightCover.BindData(Owner, QueryInstance.QueryID);
	MinHeightCover.BindData(Owner, QueryInstance.QueryID);
	float CurrentRadius = MaxRadius.GetValue();
	const float HeightCoverBinded = FullHeightCover.GetValue();
	const float MinHeightCoverBinded = MinHeightCover.GetValue();

	UWorld* World = QueryInstance.World;

	for (const AActor* Target : Targets)
	{
		if (!Target)
		{
			continue;
		}
		const APawn* Pawn = Cast<APawn>(Target);
		FVector ContextLocation = Pawn ? Pawn->GetPawnViewLocation() : Target->GetActorLocation();

		TArray<FNavLocation> Locations;

		while (CurrentRadius > MinRadius)
		{
			Locations.Empty();
			SearchCover(Locations, CurrentRadius, ContextLocation, HeightCoverBinded, MinHeightCoverBinded, World);
			if (Locations.Num() > 0)
			{
				QueryInstance.AddItemData<UEnvQueryItemType_Point>(Locations);
				break;
			}
			CurrentRadius -= DepthPrecision;
		}

	}
}

void UPMTCoverPointsGenerator::SearchCover(TArray<FNavLocation>& Locations, float CurrentRadius, FVector& ContextLocation, float MaxHeightCover, float MinHeight, UWorld* World) const
{
	UNavigationSystemV1* NavigationSystem = UNavigationSystemV1::GetNavigationSystem(World);
	if (!NavigationSystem)
	{
		return;
	}

	if (!World || !NavigationSystem)
	{
		return;
	}

	const int32 DegreesBetweenTraces = 360 / QuantityOfTraces;
	for (int32 i = 0; i < QuantityOfTraces; ++i)
	{
		FQuat Rotator = FQuat::MakeFromEuler(FVector(0, 0, i * DegreesBetweenTraces));
		FVector Start = Rotator.RotateVector(FVector(1.0f, 0, 0)) * CurrentRadius + ContextLocation;
		FNavLocation StartNav;
		FVector Extent (0, 0, 0);
		const bool bSuccess = NavigationSystem->ProjectPointToNavigation(Start, StartNav, Extent);
		if (!bSuccess)
		{
			continue;
		}

		StartNav.Location += FVector(0.f, 0.f, MinHeight);
		FHitResult Hit;
		if (World->LineTraceSingleByChannel(Hit, StartNav, ContextLocation, ECC_Visibility))
		{
			Locations.Emplace(FNavLocation(Hit.Location + Hit.ImpactNormal * OffsetFromCover + FVector(0, 0, MaxHeightCover)));
		}
	}
}

