﻿#include "PMTGlobalBlackboardKeys.h"

namespace
{
	const FName TargetKeyName = FName(TEXT("Target"));
}

UPMTGlobalBlackboardKeys::UPMTGlobalBlackboardKeys()
{
}

FName UPMTGlobalBlackboardKeys::GetTarget()
{
	return TargetKeyName;
}



