#include "Tasks/BTTask_ClearBackboardValue.h"

#include "BehaviorTree/BlackboardComponent.h"

UBTTask_ClearBackboardValue::UBTTask_ClearBackboardValue()
{
	NodeName = TEXT("Clear Blackboard Value");
}

EBTNodeResult::Type UBTTask_ClearBackboardValue::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	const FName SelectedKey = GetSelectedBlackboardKey();
	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	if (Blackboard)
	{
		Blackboard->ClearValue(SelectedKey);
		return EBTNodeResult::Succeeded;
	}
	return EBTNodeResult::Failed;
}

FString UBTTask_ClearBackboardValue::GetStaticDescription() const
{
	return FString::Printf(TEXT("Clear: %s"), *BlackboardKey.SelectedKeyName.ToString());
}
