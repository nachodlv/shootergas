﻿#include "Tasks/BTTask_SetFocus.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTask_SetFocus::UBTTask_SetFocus()
{
	NodeName = TEXT("Set Focus");
	Pawn.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_SetFocus, Pawn), APawn::StaticClass());
	ActorToFocus.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_SetFocus, ActorToFocus), AActor::StaticClass());
}

EBTNodeResult::Type UBTTask_SetFocus::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if (!BlackboardComponent)
	{
		return EBTNodeResult::Failed;
	}

	APawn* SelectedPawn = Cast<APawn>(BlackboardComponent->GetValueAsObject(Pawn.SelectedKeyName));
	AAIController* Controller = Cast<AAIController>(SelectedPawn ? SelectedPawn->GetController() : nullptr);

	if (!Controller)
	{
		UE_LOG(LogTemp, Warning, TEXT("UBTTask_ClearFocus::ExecuteTask: The pawn is not possesed by an AIController"))
		return EBTNodeResult::Failed;
	}

	if (ClearFocus)
	{
		Controller->ClearFocus(EAIFocusPriority::Gameplay);
	}
	else if (AActor* SelectedFocus = Cast<AActor>(BlackboardComponent->GetValueAsObject(ActorToFocus.SelectedKeyName)))
	{
		Controller->SetFocus(SelectedFocus);
	}
	else
	{
		return EBTNodeResult::Failed;
	}

	return EBTNodeResult::Succeeded;
}

FString UBTTask_SetFocus::GetStaticDescription() const
{
	if (ClearFocus)
	{
		return FString::Printf(TEXT("%s clear focus"), *Pawn.SelectedKeyName.ToString());
	}

	return FString::Printf(TEXT("%s: %s focusing %s"),
	                       *Super::GetStaticDescription(),
	                       *Pawn.SelectedKeyName.ToString(),
	                       *ActorToFocus.SelectedKeyName.ToString());
}


