﻿
#include "Tasks/GameplayAbility/BTTask_CancelAbility.h"

#include "AbilitySystemComponent.h"
#include "FunctionLibraries/PMTAIFunctionLibrary.h"

EBTNodeResult::Type UBTTask_CancelAbility::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	UAbilitySystemComponent* ASC = UPMTAIFunctionLibrary::GetAbilitySystemComponent(&OwnerComp);
	if (!ASC)
	{
		return EBTNodeResult::Failed;
	}
	if (bCancelAbilityWithTags)
	{
		ASC->CancelAbilities(&AbilityToCancelWithTags);
	} else
	{
		ASC->CancelAbility(AbilityToCancel.GetDefaultObject());
	}
	return EBTNodeResult::Succeeded;
}

FString UBTTask_CancelAbility::GetStaticDescription() const
{
	if (bCancelAbilityWithTags)
	{
		return FString::Printf(TEXT("Cancels abilities with tag %s"), *AbilityToCancelWithTags.ToString());
	}
	if (*AbilityToCancel)
	{
		return FString::Printf(TEXT("Cancels the ability %s"), *AbilityToCancel->GetName());
	}

	return FString();
}
