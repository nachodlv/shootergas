﻿#include "Tasks/GameplayAbility/BTTask_ApplyGameplayEffect.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "AIController.h"

// PMTAI Includes
#include "BehaviorTree/BlackboardComponent.h"
#include "FunctionLibraries/PMTAIFunctionLibrary.h"

UBTTask_ApplyGameplayEffect::UBTTask_ApplyGameplayEffect()
{
	GameplayEffectKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_ApplyGameplayEffect, GameplayEffectKey), UGameplayEffect::StaticClass());
}

EBTNodeResult::Type UBTTask_ApplyGameplayEffect::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* AIController = OwnerComp.GetAIOwner();
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	UAbilitySystemComponent* ASC = UPMTAIFunctionLibrary::GetAbilitySystemComponent(&OwnerComp);
	if (!ASC)
	{
		return EBTNodeResult::Failed;
	}

	UGameplayEffect* GE = GetGameplayEffect(OwnerComp);

	FApplyGameplayEffectMemory* InstanceNodeMemory = CastInstanceNodeMemory<FApplyGameplayEffectMemory>(NodeMemory);
	FGameplayEffectContextHandle EffectContext = ASC->MakeEffectContext();
	EffectContext.AddSourceObject(AIController);
	InstanceNodeMemory->GameplayEffectHandle = ASC->ApplyGameplayEffectToSelf(GE, Level, EffectContext);

	if (!InstanceNodeMemory->GameplayEffectHandle.IsValid())
	{
		return EBTNodeResult::Failed;
	}
	return bFireAndForget ? EBTNodeResult::Succeeded : EBTNodeResult::InProgress;
}

EBTNodeResult::Type UBTTask_ApplyGameplayEffect::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (bFireAndForget)
	{
		return Super::AbortTask(OwnerComp, NodeMemory);
	}

	UAbilitySystemComponent* ASC = UPMTAIFunctionLibrary::GetAbilitySystemComponent(&OwnerComp);
	if (!ASC)
	{
		return EBTNodeResult::Failed;
	}

	FApplyGameplayEffectMemory* InstanceNodeMemory = CastInstanceNodeMemory<FApplyGameplayEffectMemory>(NodeMemory);
	ASC->RemoveActiveGameplayEffect(InstanceNodeMemory->GameplayEffectHandle, 1);
	return EBTNodeResult::Aborted;
}

uint16 UBTTask_ApplyGameplayEffect::GetInstanceMemorySize() const
{
	return sizeof(FApplyGameplayEffectMemory);
}

FString UBTTask_ApplyGameplayEffect::GetStaticDescription() const
{
	FString GEName;
	if (!bUseGameplayEffectKey && GameplayEffect.GetDefaultObject())
	{
		GEName = GameplayEffect.GetDefaultObject()->GetName();
	} else
	{
		GEName = GameplayEffectKey.SelectedKeyName.ToString();
	}
	return FString::Printf(TEXT("Apply GE: %s"), *GEName);
}

UGameplayEffect* UBTTask_ApplyGameplayEffect::GetGameplayEffect(UBehaviorTreeComponent& OwnerComp) const
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if (bUseGameplayEffectKey && BlackboardComponent)
	{
		return Cast<UGameplayEffect>(BlackboardComponent->GetValueAsObject(GameplayEffectKey.SelectedKeyName));
	}
	return GameplayEffect.GetDefaultObject();
}
