﻿#include "Tasks/GameplayAbility/BTTask_ActivateInstantAbility.h"


#include "FunctionLibraries/PMTAIFunctionLibrary.h"
#include "GameplayAbilities/Public/AbilitySystemComponent.h"

UBTTask_ActivateInstantAbility::UBTTask_ActivateInstantAbility()
{
	NodeName = TEXT("Activate Instant Ability");
}

EBTNodeResult::Type UBTTask_ActivateInstantAbility::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	UAbilitySystemComponent* ASC = UPMTAIFunctionLibrary::GetAbilitySystemComponent(&OwnerComp);
	if (!ASC)
	{
		return EBTNodeResult::Failed;
	}
	const bool Activated = ASC->TryActivateAbilityByClass(Ability);
	return Activated ? EBTNodeResult::Succeeded : EBTNodeResult::Failed;
}

FString UBTTask_ActivateInstantAbility::GetStaticDescription() const
{
	UGameplayAbility* GameplayAbility = Ability.GetDefaultObject();
	if (GameplayAbility)
	{
		return FString::Printf(TEXT("%s: %s"),
		                       *Super::GetStaticDescription(),
		                       *GameplayAbility->GetName());
	}
	return Super::GetStaticDescription();
}
