﻿#include "Tasks/GameplayAbility/BTTask_ActivateAbility.h"

// UE Includes
#include "AbilitySystemComponent.h"

// PMTAI Includes
#include "Components/PMTAIAbilityMessageSender.h"
#include "FunctionLibraries/PMTAIFunctionLibrary.h"

UBTTask_ActivateAbility::UBTTask_ActivateAbility()
{
}

EBTNodeResult::Type UBTTask_ActivateAbility::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	UAbilitySystemComponent* ASC = UPMTAIFunctionLibrary::GetAbilitySystemComponent(&OwnerComp);
	UPMTAIAbilityMessageSender* MessageSender = UPMTAIFunctionLibrary::GetAbilityMessageSenderComponent(&OwnerComp);
	if (!ASC || !MessageSender)
	{
		return EBTNodeResult::Failed;
	}

	FActivateAbilityMemory* ActivateAbilityMemory = CastInstanceNodeMemory<FActivateAbilityMemory>(NodeMemory);
	WaitForMessage(OwnerComp, UPMTAIAbilityMessageSender::AIMessage_AbilityEnded);
	WaitForMessage(OwnerComp, UPMTAIAbilityMessageSender::AIMessage_AbilityFailed);
	ActivateAbilityMemory->EndedAbilityHandle = MessageSender->GetAbilityEndedMessageSender().SubscribeToMessages();
	ActivateAbilityMemory->FailedAbilityHandle = MessageSender->GetAbilityFailedMessageSender().SubscribeToMessages();

	const bool bSuccess = ASC->TryActivateAbilitiesByTag(AbilityTags);
	if (!bSuccess)
	{
		UnSubscribe(OwnerComp, NodeMemory, MessageSender);
		return EBTNodeResult::Failed;
	}

	return EBTNodeResult::InProgress;
}

void UBTTask_ActivateAbility::OnMessage(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, FName Message, int32 RequestID, bool bSuccess)
{
	UPMTAIAbilityMessageSender* MessageSender = UPMTAIFunctionLibrary::GetAbilityMessageSenderComponent(&OwnerComp);
	if (!MessageSender)
	{
		return;
	}

	if (Message == UPMTAIAbilityMessageSender::AIMessage_AbilityFailed)
	{
		const UGameplayAbility* Ability = MessageSender->GetAbilityFailedMessageSender().GetObjectByRequestId<UGameplayAbility>(RequestID);
		if (Ability && Ability->AbilityTags.HasAll(AbilityTags))
		{
			FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
		}
	} else
	{
		const UGameplayAbility* Ability = MessageSender->GetAbilityEndedMessageSender().GetObjectByRequestId<UGameplayAbility>(RequestID);
		if (Ability && Ability->AbilityTags.HasAll(AbilityTags))
		{
			FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
		}
	}
	UnSubscribe(OwnerComp, NodeMemory, MessageSender);
}

uint16 UBTTask_ActivateAbility::GetInstanceMemorySize() const
{
	return sizeof(FActivateAbilityMemory);
}

EBTNodeResult::Type UBTTask_ActivateAbility::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UAbilitySystemComponent* ASC = UPMTAIFunctionLibrary::GetAbilitySystemComponent(&OwnerComp);
	if (ASC)
	{
		ASC->CancelAbilities(&AbilityTags);
	}
	UPMTAIAbilityMessageSender* MessageSender = UPMTAIFunctionLibrary::GetAbilityMessageSenderComponent(&OwnerComp);
	UnSubscribe(OwnerComp, NodeMemory, MessageSender);
	return EBTNodeResult::Aborted;
}

FString UBTTask_ActivateAbility::GetStaticDescription() const
{
	TArray<FGameplayTag> Tags;
	AbilityTags.GetGameplayTagArray(Tags);
	TArray<FString> TagNames;
	TagNames.Reserve(Tags.Num());
	for (const FGameplayTag& Tag : Tags)
	{
		TagNames.Add(Tag.GetTagName().ToString());
	}

	return FString::Printf(TEXT("Abilities activated:\n%s"), *FString::Join(TagNames, TEXT("\n")));
}

void UBTTask_ActivateAbility::UnSubscribe(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, UPMTAIAbilityMessageSender* MessageSender) const
{
	if (!MessageSender)
	{
		return;
	}

	FActivateAbilityMemory* ActivateAbilityMemory = CastInstanceNodeMemory<FActivateAbilityMemory>(NodeMemory);
	StopWaitingForMessages(OwnerComp);
	MessageSender->GetAbilityEndedMessageSender().UnSubscribeToMessages(ActivateAbilityMemory->EndedAbilityHandle);
	MessageSender->GetAbilityFailedMessageSender().UnSubscribeToMessages(ActivateAbilityMemory->FailedAbilityHandle);
}



