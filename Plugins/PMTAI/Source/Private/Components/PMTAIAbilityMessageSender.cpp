﻿#include "Components/PMTAIAbilityMessageSender.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "AIController.h"
#include "BrainComponent.h"

namespace
{
	UAbilitySystemComponent* GetAbilitySystemComponent(const AAIController* Controller)
	{
		IAbilitySystemInterface* ASInterface = Controller ? Controller->GetPawn<IAbilitySystemInterface>() : nullptr;
		return ASInterface ? ASInterface->GetAbilitySystemComponent() : nullptr;
	}
}

//
// ~ Begin FPMTAIMessageSenderHelper
//

int32 FPMTAIMessageSenderHelper::RequestID = 0;

void FPMTAIMessageSenderHelper::SendMessage(const UObject* ObjectToSend)
{
	if (Handles.Num() == 0)
	{
		return;
	}
	const FAIRequestID RequestId = FAIRequestID(++RequestID);
	ObjectsByRequestId.Add(RequestId, ObjectToSend);
	FAIMessage::Send(BrainComponent, FAIMessage(Message, Sender, RequestId, FAIMessage::Success));
}

const FPMTAIMessageSenderHandle& FPMTAIMessageSenderHelper::SubscribeToMessages()
{
	const int32 NewHandleIndex = Handles.Emplace(FPMTAIMessageSenderHandle::GenerateNewHandle());
	return Handles[NewHandleIndex];
}

bool FPMTAIMessageSenderHelper::UnSubscribeToMessages(const FPMTAIMessageSenderHandle& Handle)
{
	const int32 Removes = Handles.Remove(Handle);
	if (Handles.Num() == 0)
	{
		ObjectsByRequestId.Empty();
	}
	return Removes > 0;
}

//
// ~ End FPMTAIMessageSenderHelper
//

//
// ~ Start UPMTAIAbilityMessageSender
//
const FName UPMTAIAbilityMessageSender::AIMessage_AbilityActivated = TEXT("AbilityActivated");
const FName UPMTAIAbilityMessageSender::AIMessage_AbilityEnded= TEXT("AbilityEnded");
const FName UPMTAIAbilityMessageSender::AIMessage_AbilityFailed= TEXT("AbilityFailed");

UPMTAIAbilityMessageSender::UPMTAIAbilityMessageSender()
{
}

void UPMTAIAbilityMessageSender::BeginPlay()
{
	Super::BeginPlay();

	AAIController* Controller = Cast<AAIController>(GetOwner());
	if (Controller)
	{
		BrainComponent = Controller->GetBrainComponent();
		AbilityActivatedMessageSender = FPMTAIMessageSenderHelper(this, BrainComponent, AIMessage_AbilityActivated);
		AbilityEndedMessageSender = FPMTAIMessageSenderHelper(this, BrainComponent, AIMessage_AbilityEnded);
		AbilityFailedMessageSender = FPMTAIMessageSenderHelper(this, BrainComponent, AIMessage_AbilityFailed);
	}

	UAbilitySystemComponent* ASC = GetAbilitySystemComponent(Controller);
	if (ASC)
	{
		ASC->AbilityActivatedCallbacks.AddUObject(this, &UPMTAIAbilityMessageSender::AbilityActivated);
		ASC->OnAbilityEnded.AddUObject(this, &UPMTAIAbilityMessageSender::AbilityEnded);
		ASC->AbilityFailedCallbacks.AddUObject(this, &UPMTAIAbilityMessageSender::AbilityFailed);
	}
}

void UPMTAIAbilityMessageSender::AbilityActivated(UGameplayAbility* AbilityActivated)
{
	AbilityActivatedMessageSender.SendMessage(AbilityActivated);
}

void UPMTAIAbilityMessageSender::AbilityEnded(const FAbilityEndedData& AbilityEndedData)
{
	AbilityEndedMessageSender.SendMessage(AbilityEndedData.AbilityThatEnded);
}

void UPMTAIAbilityMessageSender::AbilityFailed(const UGameplayAbility* FailedAbility, const FGameplayTagContainer& FailedReason)
{
	AbilityFailedMessageSender.SendMessage(FailedAbility);
}

//
// ~ End UPMTAIAbilityMessageSender
//
