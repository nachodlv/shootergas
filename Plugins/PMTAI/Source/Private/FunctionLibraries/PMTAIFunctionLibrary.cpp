﻿#include "FunctionLibraries/PMTAIFunctionLibrary.h"

#include "AbilitySystemInterface.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"

#include "Interfaces/PMTAIHaveAbilityMessageSender.h"

UAbilitySystemComponent* UPMTAIFunctionLibrary::GetAbilitySystemComponent(UBehaviorTreeComponent* OwnerComp)
{
	AAIController* AIController = OwnerComp ? OwnerComp->GetAIOwner() : nullptr;
	IAbilitySystemInterface* AbilitySystemInterface =
		Cast<IAbilitySystemInterface>(AIController ? AIController->GetPawn() : nullptr);
	return AbilitySystemInterface ? AbilitySystemInterface->GetAbilitySystemComponent() : nullptr;
}

UPMTAIAbilityMessageSender* UPMTAIFunctionLibrary::GetAbilityMessageSenderComponent(UBehaviorTreeComponent* OwnerComp)
{
	IPMTAIHaveAbilityMessageSender* AbilityMessageSenderInterface = OwnerComp
		                                                                ? Cast<IPMTAIHaveAbilityMessageSender>(OwnerComp->GetAIOwner())
		                                                                : nullptr;
	return AbilityMessageSenderInterface ? AbilityMessageSenderInterface->GetMessageSender() : nullptr;
}
