﻿#include "Services/BTService_SetActorLocation.h"

#include "AIModule/Classes/BehaviorTree/BlackboardComponent.h"

UBTService_SetActorLocation::UBTService_SetActorLocation()
{
	NodeName = "Set Actor Location";

	Actor.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_SetActorLocation, Actor), AActor::StaticClass());
	Location.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_SetActorLocation, Location));
}

void UBTService_SetActorLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	SetActorLocation(OwnerComp);
}

void UBTService_SetActorLocation::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	SetActorLocation(OwnerComp);
}

void UBTService_SetActorLocation::SetActorLocation(UBehaviorTreeComponent& OwnerComp) const
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if (!BlackboardComponent)
	{
		return;
	}

	AActor* ActorSelected = Cast<AActor>(BlackboardComponent->GetValueAsObject(Actor.SelectedKeyName));
	if (ActorSelected)
	{
		BlackboardComponent->SetValueAsVector(Location.SelectedKeyName, ActorSelected->GetActorLocation());
	}
}

FString UBTService_SetActorLocation::GetStaticDescription() const
{
	return FString::Printf(TEXT("Actor: %s\nLocation: %s"),
		*Actor.SelectedKeyName.ToString(),
		*Location.SelectedKeyName.ToString());
}
