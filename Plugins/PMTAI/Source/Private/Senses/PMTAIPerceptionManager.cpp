﻿#include "Senses/PMTAIPerceptionManager.h"

// UE Includes
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"

// PMTAI Includes
#include "Senses/Interfaces/PMTAIPerception.h"
#include "Senses/Interfaces/PMTAIPerceptionToBlackboardKey.h"

//
// ~ Begin FPMTAIBlackboardKey
//
IPMTAIPerceptionToBlackboardKey* FPMTAIBlackboardKey::GetPerceptionToBlackboardKey() const
{
	return Cast<IPMTAIPerceptionToBlackboardKey>(PerceptionToBlackboardKey.GetDefaultObject());
}

//
// ~ End FPMTAIBlackboardKey
//

//
// ~ Begin UPMTAIPerceptionManager
//
UPMTAIPerceptionManager::UPMTAIPerceptionManager()
{
}

void UPMTAIPerceptionManager::BeginPlay()
{
	Super::BeginPlay();

	AIController = Cast<AAIController>(GetOwner());

	if (AIController)
	{
		Pawn = AIController->GetPawn();
	} else
	{
		Pawn = Cast<APawn>(GetOwner());
		if (Pawn)
		{
			AIController = Pawn->GetController<AAIController>();
		}
	}

	check(AIController); // The component should be used with an AIController as owner or pawn possessed by an AIController

	IPMTAIPerception* PerceptionComponent = Cast<IPMTAIPerception>(GetPerceptionComponent());
	check(PerceptionComponent); // The ai controller should have a perception component that implements IPMTAIPerception

	PerceptionComponent->GetOnPerceptionUpdatedDelegate().AddDynamic(this, &UPMTAIPerceptionManager::ActorsPerceptionUpdated);
	PerceptionComponent->GetOnStimulusExpiredDelegate().AddUObject(this, &UPMTAIPerceptionManager::StimulusExpired);
}

void UPMTAIPerceptionManager::ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors)
{
	UBlackboardComponent* BlackboardComponent = GetBlackboardComponent();
	UAIPerceptionComponent* PerceptionComponent = GetPerceptionComponent();
	if (!BlackboardComponent || !Pawn || !PerceptionComponent)
	{
		return;
	}
	for (AActor* UpdatedActor : UpdatedActors)
	{
		if (UpdatedActor == Pawn)
		{
			continue;
		}

		const FActorPerceptionInfo* ActorPerceptionInfo = PerceptionComponent->GetActorInfo(*UpdatedActor);
		if (!ActorPerceptionInfo)
		{
			return;
		}
		for (int32 i = 0; i < SensesByPriority.Num(); i++)
		{
			const FPMTAISenseWithBlackboardKeys& SenseWithBlackboardKey = SensesByPriority[i];

			if (ActorPerceptionInfo->HasKnownStimulusOfSense(UAISense::GetSenseID(SenseWithBlackboardKey.Sense)))
			{
				for (const FPMTAIBlackboardKey& BlackboardKey : SenseWithBlackboardKey.BlackboardKeys)
				{
					if (!CanOverrideBlackboardKey(BlackboardKey.BlackboardKeyName, i))
					{
						continue;
					}
					const IPMTAIPerceptionToBlackboardKey* PerceptionToBlackboardKey = BlackboardKey.GetPerceptionToBlackboardKey();
					if (!PerceptionToBlackboardKey)
					{
						continue;
					}

					// The sense is currently being perceived
					PerceptionToBlackboardKey->SetToBlackboard(BlackboardKey.BlackboardKeyName, ActorPerceptionInfo, BlackboardComponent);
					KeysSetWithPriorities.Add(BlackboardKey.BlackboardKeyName, i);
				}

			}
		}
	}
}

void UPMTAIPerceptionManager::StimulusExpired(FAIStimulus& Stimulus)
{
	for (int32 i = 0; i < SensesByPriority.Num(); ++i)
	{
		const FPMTAISenseWithBlackboardKeys& SenseWithBlackboardKey = SensesByPriority[i];
		if (UAISense::GetSenseID(SensesByPriority[i].Sense) == Stimulus.Type)
		{
			for (const FPMTAIBlackboardKey& BlackboardKey : SenseWithBlackboardKey.BlackboardKeys)
			{
				if (CanOverrideBlackboardKey(BlackboardKey.BlackboardKeyName, i))
				{
					RemoveSenseFromBlackboard(BlackboardKey.BlackboardKeyName);
				}
			}

		}
	}
}

void UPMTAIPerceptionManager::RemoveSenseFromBlackboard(const FName& BlackboardKeyName)
{
	UBlackboardComponent* BlackboardComponent = GetBlackboardComponent();
	if (!BlackboardComponent || !KeysSetWithPriorities.Find(BlackboardKeyName))
	{
		return;
	}

	// We don't have a stimulus of this sense so we clear it
	BlackboardComponent->ClearValue(BlackboardKeyName);
	KeysSetWithPriorities.Remove(BlackboardKeyName);
}

bool UPMTAIPerceptionManager::CanOverrideBlackboardKey(const FName& BlackboardKey, int32 SensePriority)
{
	// Checks if the blackboard key is currently being used by a sense with higher priority
	const int32* Priority = KeysSetWithPriorities.Find(BlackboardKey);
	return !Priority || *Priority <= SensePriority;
}

UBlackboardComponent* UPMTAIPerceptionManager::GetBlackboardComponent() const
{
	return AIController ? AIController->GetBlackboardComponent() : nullptr;
}

UAIPerceptionComponent* UPMTAIPerceptionManager::GetPerceptionComponent() const
{
	return AIController ? AIController->GetPerceptionComponent() : nullptr;
}

//
// ~ End UPMTAIPerceptionManager
//
