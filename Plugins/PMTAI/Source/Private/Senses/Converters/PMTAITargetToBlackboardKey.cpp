﻿#include "Senses/Converters/PMTAITargetToBlackboardKey.h"

// UE Includes
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"

void UPMTAITargetToBlackboardKey::SetToBlackboard(const FName& BlackboardKey, const FActorPerceptionInfo* PerceptionInfo, UBlackboardComponent* BlackboardComponent) const
{
	if (!BlackboardComponent || !PerceptionInfo)
	{
		return;
	}

	BlackboardComponent->SetValueAsObject(BlackboardKey, PerceptionInfo->Target.Get());
}
