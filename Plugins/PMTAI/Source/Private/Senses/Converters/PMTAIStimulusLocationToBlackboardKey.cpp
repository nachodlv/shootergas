﻿#include "Senses/Converters/PMTAIStimulusLocationToBlackboardKey.h"

// UE Includes
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"

void UPMTAIStimulusLocationToBlackboardKey::SetToBlackboard(const FName& BlackboardKey, const FActorPerceptionInfo* PerceptionInfo, UBlackboardComponent* BlackboardComponent) const
{
	if (!BlackboardComponent || !PerceptionInfo)
	{
		return;
	}

	BlackboardComponent->SetValueAsVector(BlackboardKey, PerceptionInfo->GetLastStimulusLocation());
}
