﻿#include "Decorators/BTDecorator_CompareFloats.h"

// UE Includes
#include "BehaviorTree/BlackboardComponent.h"

UBTDecorator_CompareFloats::UBTDecorator_CompareFloats()
{
	KeyLeftFloat.AddFloatFilter(this, GET_MEMBER_NAME_CHECKED(UBTDecorator_CompareFloats, KeyLeftFloat));
	KeyRightFloat.AddFloatFilter(this, GET_MEMBER_NAME_CHECKED(UBTDecorator_CompareFloats, KeyRightFloat));
}

void UBTDecorator_CompareFloats::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	if (BlackboardComp)
	{
		if (bLeftValueAsKey)
		{
			const FBlackboard::FKey LeftKey = KeyLeftFloat.GetSelectedKeyID();
			BlackboardComp->RegisterObserver(
                        LeftKey,
                        this,
                        FOnBlackboardChangeNotification::CreateUObject(this, &UBTDecorator_CompareFloats::OnKeyValueChanged)
                    );
		}

		if (bRightValueAsKey)
		{
			const FBlackboard::FKey Right = KeyRightFloat.GetSelectedKeyID();
			BlackboardComp->RegisterObserver(
            Right,
            this,
            FOnBlackboardChangeNotification::CreateUObject(this, &UBTDecorator_CompareFloats::OnKeyValueChanged)
        );
		}

	}
}

bool UBTDecorator_CompareFloats::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if (!BlackboardComponent)
	{
		return false;
	}

	const float Left = bLeftValueAsKey ? BlackboardComponent->GetValueAsFloat(KeyLeftFloat.SelectedKeyName) : LeftFloat;
	const float Right = bRightValueAsKey ? BlackboardComponent->GetValueAsFloat(KeyRightFloat.SelectedKeyName) : RightFloat;

	switch (ComparisonType)
	{
	case EFloatComparisonType::Equal:
		return FMath::Abs(Left - Right) < ComparisonPrecision;
	case EFloatComparisonType::GreaterThan:
		return Left > Right;
	default:
		return false;
	}

	return false;
}

EBlackboardNotificationResult UBTDecorator_CompareFloats::OnKeyValueChanged(const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID)
{
	UBehaviorTreeComponent* BehaviorComp = static_cast<UBehaviorTreeComponent*>(Blackboard.GetBrainComponent());
	if (!BehaviorComp)
	{
		return EBlackboardNotificationResult::RemoveObserver;
	}

	BehaviorComp->RequestExecution(this);
	return EBlackboardNotificationResult::ContinueObserving;
}


