﻿#include "NavMesh/PMTAINavLinkPooler.h"

// UE Includes
#include "Kismet/GameplayStatics.h"

// PMTAI Includes
#include "NavMesh/Interfaces/PMTAINavLinkProxyProvider.h"

// PMTCommon Includes
#include "Utilities/PMTCObjectPooler.h"


// ~ Begin FNavLinkWrapper

FNavLinkWrapper::FNavLinkWrapper(const TScriptInterface<IPMTAINavLinkProxyProvider> InNavLinkProvider) : NavLinkProvider(InNavLinkProvider)
{
	if (NavLinkProvider)
	{
		OnNavLinkReachedDelegate = NavLinkProvider->GetOnNavLinkProxyReached().AddRaw(this, &FNavLinkWrapper::NavLinkReached);
		LastTimeUsed = UGameplayStatics::GetRealTimeSeconds(NavLinkProvider.GetObject());
	}
}

FNavLinkWrapper::~FNavLinkWrapper()
{
	if (NavLinkProvider)
	{
		NavLinkProvider->GetOnNavLinkProxyReached().Remove(OnNavLinkReachedDelegate);
	}
}

void FNavLinkWrapper::NavLinkReached(AActor* Actor, const FVector& Destination)
{
	LastTimeUsed = UGameplayStatics::GetRealTimeSeconds(Actor);
	OnNavLinkUsed.Broadcast(*this);
}

// ~ End FNavLinkWrapper


// ~ Begin UPMTAINavLinkPooler

UPMTAINavLinkPooler::UPMTAINavLinkPooler()
{
	NavLinksPooler = CreateDefaultSubobject<UPMTCObjectPooler>(TEXT("NavLinksPooler"));
}

void UPMTAINavLinkPooler::Initialize(UWorld* World)
{
	NavLinksPooler->InstantiateInitialObjects(MaxLinks, [this, World]
    {
        return Cast<ANavLinkProxy>(World->SpawnActor(LinkProxyClass));
    });
}

const TScriptInterface<IPMTAINavLinkProxyProvider>& UPMTAINavLinkPooler::GetNavLink()
{
	return InstantiateWrapper().GetNavLinkProvider();
}

void UPMTAINavLinkPooler::BeginDestroy()
{
	NavLinkWrappers.Empty(); // Necessary for unbinding the NavLinkProvider on the destructor
	Super::BeginDestroy();
}

FNavLinkWrapper& UPMTAINavLinkPooler::InstantiateWrapper()
{
	// If we surpass the MaxLinks then the link which was used the last is deactivated
	if (NavLinkWrappers.Num() >= MaxLinks)
	{
		int32 OldestLink = 0;
		for (int32 i = 1; i < NavLinkWrappers.Num(); ++i)
		{
			if (NavLinkWrappers[i].GetLastTimeUsed() < NavLinkWrappers[OldestLink].GetLastTimeUsed())
			{
				OldestLink = i;
			}
		}
		TScriptInterface<IPMTAINavLinkProxyProvider> OldestNavLink = NavLinkWrappers[OldestLink].GetNavLinkProvider();
		if (OldestNavLink)
		{
			OldestNavLink->DeActivate();
		}
		NavLinkWrappers.RemoveAt(OldestLink, 1, false);
	}

	// Gets the new link from the pooler
	TScriptInterface<IPMTAINavLinkProxyProvider> Link;
	if (NavLinksPooler)
	{
		Link = TScriptInterface<IPMTAINavLinkProxyProvider>(NavLinksPooler->GetObject<ANavLinkProxy>());
	}

	check(Link); // The Link should always be created

	// Adds the new link to the array of active links
	const int32 NewLink = NavLinkWrappers.Emplace(FNavLinkWrapper(Link));

	return NavLinkWrappers[NewLink];
}

// ~ End UPMTAINavLinkPooler
