﻿#include "NavMesh/PMTAIBreadcrumbActorListener.h"

// UE Includes
#include "GameFramework/Character.h"

// PMTAI Includes
#include "NavMesh/PMTAIBreadcrumbsGenerator.h"
#include "NavMesh/Interfaces/PMTAICanJump.h"

void APMTAIBreadcrumbActorListener::Initialize(APMTAIBreadcrumbsGenerator* NewBreadcrumbsGenerator)
{
	BreadcrumbsGenerator = NewBreadcrumbsGenerator;
}

void APMTAIBreadcrumbActorListener::ListenToActor(const TScriptInterface<IPMTAICanJump>& Actor)
{
	if (!Actor)
	{
		return;
	}

	ActorListened = Actor;
	JumpHandle = Actor->GetOnJumpDelegate().AddUObject(this, &APMTAIBreadcrumbActorListener::ActorJumped);
	WalkOffLedgeHandle = Actor->GetOnWalkOffLedgeDelegate().AddUObject(this, &APMTAIBreadcrumbActorListener::ActorWalkedOffLedge);
	LandHandle = Actor->GetOnLandDelegate().AddUObject(this, &APMTAIBreadcrumbActorListener::ActorLanded);
}

void APMTAIBreadcrumbActorListener::BeginDestroy()
{
	if (ActorListened)
	{
		ActorListened->GetOnJumpDelegate().Remove(JumpHandle);
		ActorListened->GetOnWalkOffLedgeDelegate().Remove(WalkOffLedgeHandle);
		ActorListened->GetOnLandDelegate().Remove(LandHandle);
	}
	Super::BeginDestroy();
}

void APMTAIBreadcrumbActorListener::DeActivate()
{
	bActive = false;
	if (ActorListened)
	{
		ActorListened->GetOnJumpDelegate().Remove(JumpHandle);
		ActorListened->GetOnWalkOffLedgeDelegate().Remove(WalkOffLedgeHandle);
		ActorListened->GetOnLandDelegate().Remove(LandHandle);
	}
	OnDeActivate.Broadcast(TScriptInterface<IPMTCPooleable>(this));
}

void APMTAIBreadcrumbActorListener::ActorJumped(const FVector& Location)
{
	From = Location;
}

void APMTAIBreadcrumbActorListener::ActorLanded(const FVector& Location)
{
	if (BreadcrumbsGenerator)
	{
		BreadcrumbsGenerator->PlaceBreadcrumb(ActorListened->GetAgentProperties(), From, Location);
	}
}

void APMTAIBreadcrumbActorListener::ActorWalkedOffLedge(const FVector& PreviousPosition)
{
	From = PreviousPosition;
}

