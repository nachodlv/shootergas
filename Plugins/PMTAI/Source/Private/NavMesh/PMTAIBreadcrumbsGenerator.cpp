﻿#include "NavMesh/PMTAIBreadcrumbsGenerator.h"

// UE Includes
#include "GameFramework/Character.h"
#include "NavigationSystem.h"
#include "Navigation/NavLinkProxy.h"
#include "NavLinkCustomComponent.h"

// PMTAI Includes
#include "NavMesh/PMTAIBreadcrumbActorListener.h"

// PMTCommon Includes
#include "NavMesh/PMTAINavLinkPooler.h"
#include "NavMesh/Interfaces/PMTAINavLinkProxyProvider.h"
#include "Utilities/PMTCObjectPooler.h"

APMTAIBreadcrumbsGenerator::APMTAIBreadcrumbsGenerator()
{
	ActorListenersPooler = CreateDefaultSubobject<UPMTCObjectPooler>(TEXT("ActorListenersPooler"));
}

void APMTAIBreadcrumbsGenerator::BeginPlay()
{
	Super::BeginPlay();

	FindPathAsync.BindUObject(this, &APMTAIBreadcrumbsGenerator::PathResult);
	UWorld* World = GetWorld();

	// No idea why the pooler is not instantiating
	if (!ActorListenersPooler)
	{
		ActorListenersPooler = NewObject<UPMTCObjectPooler>();
	}

	ActorListenersPooler->InstantiateInitialObjects(1, [World]()
	{
		FActorSpawnParameters SpawnParameters = FActorSpawnParameters();
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		return World->SpawnActor<APMTAIBreadcrumbActorListener>(SpawnParameters);
	});

	NavLinkPooler = NewObject<UPMTAINavLinkPooler>(this, NavLinkPoolerClass);
	NavLinkPooler->Initialize(World);
}

FPMTAIActorRegisteredHandle APMTAIBreadcrumbsGenerator::RegisterActor(const TScriptInterface<IPMTAICanJump>& Actor)
{
	APMTAIBreadcrumbActorListener* Listener = ActorListenersPooler->GetObject<APMTAIBreadcrumbActorListener>();
	if (Listener)
	{
		Listener->Initialize(this);
		Listener->ListenToActor(Actor);
		FPMTAIActorRegisteredHandle Handle = FPMTAIActorRegisteredHandle::GenerateNewHandle();
		ActorListeners.Add(Handle, Listener);
		return Handle;
	}
	return FPMTAIActorRegisteredHandle();
}

bool APMTAIBreadcrumbsGenerator::UnRegisterActor(const FPMTAIActorRegisteredHandle& ListenerId)
{
	APMTAIBreadcrumbActorListener** Listener = ActorListeners.Find(ListenerId);
	if (Listener)
	{
		check(*Listener); // we should never have a null in the array
		ActorListeners.Remove(ListenerId);
		(*Listener)->DeActivate();
		return true;
	}

	return false;
}

void APMTAIBreadcrumbsGenerator::BeginDestroy()
{
	for (auto Listener : ActorListeners)
	{
		if (IsValid(Listener.Value))
		{
			Listener.Value->DeActivate();
		}
	}
	Super::BeginDestroy();
}


bool APMTAIBreadcrumbsGenerator::PlaceBreadcrumb(const FNavAgentProperties& AgentProperties, const FVector& From, const FVector& To)
{
	UNavigationSystemV1* NavigationSystem = UNavigationSystemV1::GetNavigationSystem(GetWorld());
	FNavLocation NavFrom;
	FNavLocation NavTo;
	const bool FromProjected = NavigationSystem->ProjectPointToNavigation(From, NavFrom, NavMeshProjectionExtent);
	const bool ToProjected = NavigationSystem->ProjectPointToNavigation(To, NavTo, NavMeshProjectionExtent);
	if (!FromProjected || !ToProjected)
	{
		return false;
	}

	FPathFindingQuery Query;
	Query.CostLimit = MinimumCostForNewLink;
	Query.bAllowPartialPaths = false;
	Query.StartLocation = NavFrom;
	Query.EndLocation = NavTo;
	const uint32 QueryId = NavigationSystem->FindPathAsync(AgentProperties, Query, FindPathAsync);
	Queries.Add(QueryId, FBreadcrumbToBePlaced(NavFrom, NavTo));
	return true;
}

void APMTAIBreadcrumbsGenerator::CreateNavLink(const FVector& From, const FVector& To) const
{
	if (!NavLinkPooler)
	{
		return;
	}

	const TScriptInterface<IPMTAINavLinkProxyProvider>& Link = NavLinkPooler->GetNavLink();
	check(Link); // We should always be able to instantiate a link
	UNavLinkCustomComponent* SmartLink = Link->GetSmartLink();
	SmartLink->SetLinkData(From, To, ENavLinkDirection::LeftToRight);
}

void APMTAIBreadcrumbsGenerator::PathResult(uint32 QueryId, ENavigationQueryResult::Type Result, FNavPathSharedPtr Path)
{
	FBreadcrumbToBePlaced* Breadcrumb = Queries.Find(QueryId);
	const bool bSuccess = Result == ENavigationQueryResult::Success && Path.IsValid() && Path->GetPathPoints().Num() > 1;
	if (Breadcrumb && !bSuccess) // If there is no path then we want to create a link for it
	{
		CreateNavLink(Breadcrumb->From, Breadcrumb->To);
	}

	Queries.Remove(QueryId);
}






