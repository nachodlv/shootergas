﻿#include "NavMesh/PMTNavLinkProxy.h"

// UE Includes
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

APMTNavLinkProxy::APMTNavLinkProxy()
{
	SetSmartLinkEnabled(true);
	bSmartLinkIsRelevant = true;
}

void APMTNavLinkProxy::BeginPlay()
{
	Super::BeginPlay();
	OnSmartLinkReached.AddDynamic(this, &APMTNavLinkProxy::NotifySmartLinkReached);
}

void APMTNavLinkProxy::NotifySmartLinkReached(AActor* PathingAgent, const FVector& DestPoint)
{
	OnNavLinkProxyReached.Broadcast(PathingAgent, DestPoint);

	ACharacter* Character = Cast<ACharacter>(PathingAgent);
	ensureMsgf(Character, TEXT("APMTNavLinkProxy::NotifySmartLinkReached: the agent should implement ACharacter"));
	if (Character)
	{
		Character->LaunchCharacter(CalculateLaunchVelocity(Character, DestPoint), true, true);
	}
}

void APMTNavLinkProxy::Activate()
{
	bActive = true;
	SetSmartLinkEnabled(true);
}

void APMTNavLinkProxy::DeActivate()
{
	SetSmartLinkEnabled(false);
	OnDeActivate.Broadcast(TScriptInterface<IPMTCPooleable>(this));
	bActive = false;
}

FVector APMTNavLinkProxy::CalculateLaunchVelocity(const AActor* LaunchedActor, const FVector& Destination) const
{
	// Launch from the "feet" of the Character
	auto Start = LaunchedActor->GetActorLocation();
	Start.Z -= LaunchedActor->GetSimpleCollisionHalfHeight();

	const FVector& End = GetActorTransform().TransformPosition(Destination);

	FVector Result;
	UGameplayStatics::SuggestProjectileVelocity_CustomArc(this, Result, Start, End);

	return Result;
}


